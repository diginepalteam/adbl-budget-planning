<!-- EDIT MODAL FOR DEPO -->
<div id="updateDepoFormModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <form class="form-horizontal form-bordered" id="updateDepoForm">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">EDIT DEPO</h4>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="code"> Code</label>
                                <input type="text" class="form-control" id="code" name="code">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="name">
                                    Name</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="address">
                                    Address
                                </label>
                                <input type="text" class="form-control" id="address" name="address">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="phone">
                                    Phone Number
                                </label>
                                <input type="text" class="form-control" id="phone" name="phone">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="panNo">
                                    Pan Number
                                </label>
                                <input type="text" class="form-control" id="panNo" name="panNo">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <a type="button" id="deletebtn" onclick="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                <button type="button" id="updateDepoBtn" class="btn btn-primary">Update</button>
                                <button type="button" class="btn btn-info"
                                        data-dismiss="modal">Close
                                </button>

                            </div>
                        </div>
                    </footer>
                </div>
            </form>
        </div>

    </div>
</div>