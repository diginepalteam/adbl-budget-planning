import {CrudUtils} from "./c-crud-utils.js";
import * as Constants from "./c-constants.js";

$(document).ready(function () {
    getRq().then(r => console.log("budget date parameter loaded..."));
    ;
});

let sendSaveRq = () => {
    new CrudUtils().sendPostRequest(Constants.VERSION_1 + Constants.MD_BUDGET_DATE_PARAMETER, getFormData());
    setTimeout(function () {
        getRq().then(r => console.log("budget date parameter loaded..."));
        ;
    }, 200);
};

let getFormData = () => {
    let currentYearActual = $("[name='currentYearActual']").val();
    let currentYearGrowth = $("[name='currentYearGrowth']").val();
    let currentYearProjection = $("[name='currentYearProjection']").val();
    let nextYearProjection = $("[name='nextYearProjection']").val();
    let nextYearEnd = $("[name='nextYearEnd']").val();
    return JSON.stringify({
            currentYearActual,
            currentYearGrowth,
            currentYearProjection,
            nextYearProjection,
            nextYearEnd
        }
    );
}

let getRq = async () => {
    let resource = await CrudUtils.fetchResource(Constants.VERSION_1 + Constants.MD_BUDGET_DATE_PARAMETER);
    if (resource.status === 200) {
        let data = resource.data;
        $("#spanCurrentYearActual").html(data.currentYearActual);
        $("#spanCurrentYearGrowth").html(data.currentYearGrowth);
        $("#spanCurrentYearProjection").html(data.currentYearProjection);
        $("#spanNextYearProjection").html(data.nextYearProjection);
        $("#spanNextYearEnd").html(data.nextYearEnd);
    }
}

window.sendSaveRq = sendSaveRq;
window.getRq = getRq;