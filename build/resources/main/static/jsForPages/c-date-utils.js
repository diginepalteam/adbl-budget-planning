import {CrudUtils} from "./c-crud-utils.js";
import {API_DATE_CONVERT_N_TO_E} from "./c-constants.js";
import {API_DATE_CONVERT_E_TO_N} from "./c-constants.js";


export class DateUtils {
    async convertEnglishToNepaliDate(englishDate, successId) {
        let url = API_DATE_CONVERT_E_TO_N.replace("{englishDate}", englishDate);
        let result = await CrudUtils.fetchResource(url);
        if (result.status === 200) {
            $(successId).val(result.data);
        }
    }

    async convertNepaliToEnglishDate(nepaliDate, successId){
      let url = API_DATE_CONVERT_N_TO_E.replace("{nepaliDate}", nepaliDate);
        let resource =  await CrudUtils.fetchResource(url);
        if (resource.status === 200) {
            $(successId).val(resource.data);
        }
    }

    englishToNepali = (inputId, successId) =>{
        new DateUtils().convertEnglishToNepaliDate($(inputId).val(), successId).then(r => console.log("convertEnglishToNepaliDate"));
    };

    nepaliToEnglish = (inputId, successId) =>{
        new DateUtils().convertNepaliToEnglishDate($(inputId).val(), successId).then(r => console.log("convertNepaliToEnglishDate"));
    };
}