export const TOKEN_KEY = 'ADBLBUDGETPLANSECRETKEY';
export const BASE_URL = "http://localhost:8088";
//Resource APIs
export const API_OAUTH_GET = `${BASE_URL}/api/v1/oauth/token`;
export const API_LOGOUT = `${BASE_URL}/api/v1/logout`;

export let API_USER_PROFILE_GET = `${BASE_URL}/api/v1/profiles`;

export const API_OFFICE_GET_ALL = `${BASE_URL}/impl/api/v1/offices`;

export const API_DOCUMENTS_BY_TYPE_AND_ID_GET_ALL = `${BASE_URL}/impl/api/v1/uploads/documents/{type}/{refId}`;
export const API_DOCUMENTS_SAVE = `${BASE_URL}/impl/api/v1/uploads`;


/*
* DATE CONVERSION
 */
export let API_DATE_CONVERT_N_TO_E = `${BASE_URL}/impl/api/v1/conversions/ntoe/{nepaliDate}`;
export let API_DATE_CONVERT_E_TO_N = `${BASE_URL}/impl/api/v1/conversions/eton/{englishDate}`;
export let API_DATE_TODAY_NEPALI = `${BASE_URL}/impl/api/v1/dates/todays/nepali`;

//USER
export const API_USER_SAVE = `${BASE_URL}/api/v1/users`;
export const API_USER_UPDATE = `${BASE_URL}/api/v1/users/{username}`;
export const API_USER_GET = `${BASE_URL}/api/v1/users`;
export const API_USER_GET_ONE = `${BASE_URL}/api/v1/users/`;
export const API_USER_ACTIVATE = `${BASE_URL}/api/v1/users/{username}/activate`;
export const API_USER_DEACTIVATE = `${BASE_URL}/api/v1/users/{username}/deactivate`;
export const API_USER_CHANGE_PASSWORD = `${BASE_URL}/impl/api/v1/passwords`;
//ROLES
export const API_ROLES_GET = `${BASE_URL}/api/v1/users/roles`;
//STAFFS
export const API_STAFFS_GET_ALL = `${BASE_URL}/impl/api/v1/staffs`;
export const API_STAFFS_SAVE = `${BASE_URL}/impl/api/v1/staffs`;
export const API_STAFFS_GET_ONE = `${BASE_URL}/impl/api/v1/staffs/{id}`;
export const API_STAFFS_UPDATE = `${BASE_URL}/impl/api/v1/staffs/{id}`;

// --------------------------------------JSP PAGES---------------------------------------------------------------
export const UI_LOGIN_PAGE = `${BASE_URL}/v1/login-page`;

export const UI_USER_PROFILE_VIEW_PAGE = `${BASE_URL}/v1/user-profile-page`;

export const UI_DASHBOARD_PAGE = `${BASE_URL}/v1/dashboard-page`;

export const UI_USER_CREATE_PAGE = `${BASE_URL}/v1/user-create-page`;

export const UI_STAFF_CREATE_PAGE = `${BASE_URL}/v1/staff-create-page`;