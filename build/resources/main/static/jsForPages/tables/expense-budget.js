import {CToastNotification} from "../c-toastNotification.js";
import {CrudUtils} from "../c-crud-utils.js";
import * as Constants from "../c-constants.js";

$(document).ready(function () {
  loadDataTable().then(r => console.log(""));
});
let dataTable;
let loadDataTable = async () => {
  let count = 0;
  let resource = await CrudUtils.fetchResource(Constants.TXN_EXPENSE_BUDGET_GET);
  dataTable = $('#datatable-editable').DataTable({
    paging: false,
    scrollX: true,
    scrollY: 600,
    ordering: false,
    select: {
      style: 'os'
    },
    dom: 'Bfrtip',
    buttons: [
      {
        text: '<i class="fa fa-edit"></i> Edit',
        className: 'btn btn-info',
        action: function () {
          if (dataTable.rows({selected: true}).count() === 0) {
            new CToastNotification().getFailureToastNotification("No records selected!");
          } else if (dataTable.rows({selected: true}).count() > 1) {
            new CToastNotification().getFailureToastNotification("Please select 1 record!");
          } else {
            let selectedRowData = dataTable.rows({selected: true}).data()[0];
            loadDataTableModal(selectedRowData);
          }
        }
      }
    ],
    "data": resource.data,
    "columns": [
      {
        "data": "SNo",
        "render": function (data, type, row, meta) {
          count = count + 1;
          return count;
        }
      },
      {data: "expenseMain.head", defaultContent: ''},
      {
        "data": "expenseCode.code", defaultContent: ''
      },
      {
        "data": "expenseCode.head", defaultContent: ''
      },
      {data: "actualexpenselastyear", defaultContent: ''},
      {data: "currentyearbudget", defaultContent: ''},
      {data: "currentyearadditionalbudget", defaultContent: ''},
      {data: "totalcurrentyearbudget", defaultContent: ''},
      {data: "actualexpensethisyear", defaultContent: ''},
      {data: "currentyearexpenseprojected", defaultContent: ''},
      {data: "currentyearexpenseprojectedannual", defaultContent: ''},
      {data: "nextyearexpensebudgetprojection", defaultContent: ''},
      {data: "remarks", defaultContent: ''}
    ],
    "destroy": true
  });

  $('#datatable-editable tfoot tr').append(
    '<td>Total</td>' +
    '<td></td>' +
    '<td></td>' +
    '<td></td>' +
    '<td>' + dataTable.column(4).data().sum() + '</td>' +
    '<td>' + dataTable.column(5).data().sum() + '</td>' +
    '<td>' + dataTable.column(6).data().sum() + '</td>' +
    '<td>' + dataTable.column(7).data().sum() + '</td>' +
    '<td>' + dataTable.column(8).data().sum() + '</td>' +
    '<td>' + dataTable.column(9).data().sum() + '</td>' +
    '<td>' + dataTable.column(10).data().sum() + '</td>' +
    '<td>' + dataTable.column(11).data().sum() + '</td>' +
    '<td></td>'
  );
}
$('#datatable-editable tbody').on('dblclick', 'tr', function (e) {
  let data = dataTable.rows(this).data()[0];
  loadDataTableModal(data).then(r => console.log(""));
})

let loadDataTableModal = async (data) => {
  if (null != data.id && data.id > 0) {
    $("#updateDataModal #title").html("");
    $("#updateDataModal .panel-body :input").each(function (e) {
      this.value = data[this.id];
    });

    $("#updateDataBtn").attr("onclick", "updateData(" + data.id + ")");
    $("#closeModalBtn").attr("onclick", "closeModal()");
    $("#updateDataModal").modal('show');
  } else {
    new CToastNotification().getFailureToastNotification("ID not found!");
  }
}

function updateData(id) {
  let json = {};
  $("#updateDataModal .panel-body :input").each(function (e) {
    json[this.id] = this.value;
  });

  new CrudUtils().sendPostRequest(Constants.TXN_EXPENSE_BUDGET_UPDATE_ONE.replace("{id}", id), JSON.stringify(json));

  setTimeout(function () {
    loadDataTable().then(r => console.log(""));
  }, 1000);
}

let closeModal = async () => {
  $("#updateDataModal").modal('hide');
}

window.loadDataTableModal = loadDataTableModal;
window.updateData = updateData;
window.closeModal = closeModal;
