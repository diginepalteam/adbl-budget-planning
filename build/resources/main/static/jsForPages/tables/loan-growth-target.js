import {CrudUtils} from "../c-crud-utils.js";
import * as Constants from "../c-constants.js";

$(document).ready(function () {
    loadDataTable().then(r => console.log(""));
});
let loadDataTable = async () => {
    let count = 0;
    let resource = await CrudUtils.fetchResource(Constants.TXN_LOAN_GROWTH_TARGET_GET);
    let dataTable = $('#datatable-editable').DataTable({
        paging: false,
        dom: 'frtip',
        ordering:false,
        select: {
            style: 'os'
        },
        "data": resource.data,
        "columns": [
            {
                "data": "SNo",
                "render": function (data, type, row, meta) {
                    count = count + 1;
                    return count;
                }
            },
            {
                "data": "LoanType",
                "render": function (data, type, row, meta) {
                    return row.loanType.loanProductCode + "~" + row.loanType.name;
                }
            },
            {data: "firstqtrshort", defaultContent: ''},
            {data: "firstqtrterm", defaultContent: ''},
            {data: "firstqtrtotal", defaultContent: ''},
            {data: "secondqtrshort", defaultContent: ''},
            {data: "secondqtrterm", defaultContent: ''},
            {data: "secondqtrtotal", defaultContent: ''},
            {data: "thirdqtrshort", defaultContent: ''},
            {data: "thirdqtrterm", defaultContent: ''},
            {data: "thirdqtrtotal", defaultContent: ''},
            {data: "fourthqtrshort", defaultContent: ''},
            {data: "fourthqtrterm", defaultContent: ''},
            {data: "fourthqtrtotal", defaultContent: ''},
            {data: "sumqtrshort", defaultContent: ''},
            {data: "sumqtrterm", defaultContent: ''},
            {data: "sumqtrtotal", defaultContent: ''},
            {data: "loantotalcurestimated", defaultContent: ''},
            {data: "totalloanoutestimated", defaultContent: ''}
        ],
        "destroy": true
    });

    $('#datatable-editable tfoot tr').append(
        '<td>Total</td>'+
        '<td></td>'+
        '<td>'+dataTable.column(2).data().sum()+'</td>'+
        '<td>'+dataTable.column(3).data().sum()+'</td>'+
        '<td>'+dataTable.column(4).data().sum()+'</td>'+

        '<td>'+dataTable.column(5).data().sum()+'</td>'+
        '<td>'+dataTable.column(6).data().sum()+'</td>'+
        '<td>'+dataTable.column(7).data().sum()+'</td>'+

        '<td>'+dataTable.column(8).data().sum()+'</td>'+
        '<td>'+dataTable.column(9).data().sum()+'</td>'+
        '<td>'+dataTable.column(10).data().sum()+'</td>'+

        '<td>'+dataTable.column(11).data().sum()+'</td>'+
        '<td>'+dataTable.column(12).data().sum()+'</td>'+
        '<td>'+dataTable.column(13).data().sum()+'</td>'+

        '<td>'+dataTable.column(14).data().sum()+'</td>'+
        '<td>'+dataTable.column(15).data().sum()+'</td>'+
        '<td>'+dataTable.column(16).data().sum()+'</td>'+

        '<td>'+dataTable.column(17).data().sum()+'</td>'+
        '<td>'+dataTable.column(18).data().sum()+'</td>'
    );
}