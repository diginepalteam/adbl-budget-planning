import {CrudUtils} from "../c-crud-utils.js";
import * as Constants from "../c-constants.js";
import {CToastNotification} from "../c-toastNotification.js";

$(document).ready(function () {
    loadDataTable().then(r => console.log(""));
});
let dataTable;
let loadDataTable = async () => {
    let count = 0;
    let resource = await CrudUtils.fetchResource(Constants.TXN_LOAN_CLASSIFICATION_GET);
     dataTable = $('#datatable-editable').DataTable({
        paging: false,
        scrollX: true,
        scrollY: 400,
        ordering:false,
        dom: 'Bfrtip',
        select: {
            style: 'os'
        },
        buttons: [
            {
                text: '<i class="fa fa-edit"></i> Edit',
                className: 'btn btn-info',
                action: function () {
                    if (dataTable.rows({selected: true}).count() === 0) {
                        new CToastNotification().getFailureToastNotification("No records selected!");
                    } else if (dataTable.rows({selected: true}).count() > 1) {
                        new CToastNotification().getFailureToastNotification("Please select 1 record!");
                    } else {
                        let selectedRowData = dataTable.rows({selected: true}).data()[0];
                        loadDataTableModal(selectedRowData);
                    }
                }
            }
        ],
        "data": resource.data,
        "columns": [
            {
                "data": "SNo",
                "render": function (data, type, row, meta) {
                    count = count + 1;
                    return count;
                }
            },
            {
                "data": "LoanType",
                "render": function (data, type, row, meta) {
                    return row.loanType.loanProductCode + "~" + row.loanType.name;
                }
            },
            {data: "passcur", defaultContent: ''},
            {data: "watchlistcur", defaultContent: ''},
            {data: "substandardcur", defaultContent: ''},
            {data: "doubtfulcur", defaultContent: ''},
            {data: "losscur", defaultContent: ''},
            {data: "totalcur", defaultContent: ''},
            {data: "passthisprojected", defaultContent: ''},
            {data: "watchlistthisprojected", defaultContent: ''},
            {data: "substandardthisprojected", defaultContent: ''},
            {data: "doubtfulthisprojected", defaultContent: ''},
            {data: "lossthisprojected", defaultContent: ''},
            {data: "totalthisprojected", defaultContent: ''},
            {data: "passnextprojected", defaultContent: ''},
            {data: "watchlistnextprojected", defaultContent: ''},
            {data: "substandardnextprojected", defaultContent: ''},
            {data: "doubtfulnextprojected", defaultContent: ''},
            {data: "lossnextprojected", defaultContent: ''},
            {data: "totalnextprojected", defaultContent: ''},
        ],
        "destroy": true
    });

    $('#datatable-editable tfoot tr').append(
        '<td>Total</td>'+
        '<td></td>'+
        '<td>'+dataTable.column(2).data().sum()+'</td>'+
        '<td>'+dataTable.column(3).data().sum()+'</td>'+
        '<td>'+dataTable.column(4).data().sum()+'</td>'+
        '<td>'+dataTable.column(5).data().sum()+'</td>'+
        '<td>'+dataTable.column(6).data().sum()+'</td>'+
        '<td>'+dataTable.column(7).data().sum()+'</td>'+

        '<td>'+dataTable.column(8).data().sum()+'</td>'+
        '<td>'+dataTable.column(9).data().sum()+'</td>'+
        '<td>'+dataTable.column(10).data().sum()+'</td>'+
        '<td>'+dataTable.column(11).data().sum()+'</td>'+
        '<td>'+dataTable.column(12).data().sum()+'</td>'+
        '<td>'+dataTable.column(13).data().sum()+'</td>'+

        '<td>'+dataTable.column(14).data().sum()+'</td>'+
        '<td>'+dataTable.column(15).data().sum()+'</td>'+
        '<td>'+dataTable.column(16).data().sum()+'</td>'+
        '<td>'+dataTable.column(17).data().sum()+'</td>'+
        '<td>'+dataTable.column(18).data().sum()+'</td>'+
        '<td>'+dataTable.column(19).data().sum()+'</td>'
    );
}

$('#datatable-editable tbody').on('dblclick', 'tr', function (e) {
    let data = dataTable.rows(this).data()[0];
    loadDataTableModal(data).then(r => console.log(""));
})

let loadDataTableModal = async (data) => {
    if (null != data.id && data.id > 0) {
        $("#updateDataModal #title").html(data.loanType.name);
        $("#updateDataModal .panel-body :input").each(function (e) {
            this.value = data[this.id];
        });

        $("#updateDataBtn").attr("onclick", "updateData(" + data.id + ")");
        $("#closeModalBtn").attr("onclick", "closeModal()");
        $("#updateDataModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("ID not found!");
    }
}

function updateData(id) {
    let json = {};
    $("#updateDataModal .panel-body :input").each(function (e) {
        json[this.id] = this.value;
    });

    new CrudUtils().sendPostRequest(Constants.TXN_LOAN_CLASSIFICATION_UPDATE_ONE.replace("{id}", id), JSON.stringify(json));

    setTimeout(function () {
        loadDataTable().then(r => console.log(""));
    }, 1000);
}

let closeModal = async () => {
    $("#updateDataModal").modal('hide');
}

window.loadDataTableModal = loadDataTableModal;
window.updateData = updateData;
window.closeModal = closeModal;