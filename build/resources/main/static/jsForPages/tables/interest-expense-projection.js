import {CrudUtils} from "../c-crud-utils.js";
import * as Constants from "../c-constants.js";

$(document).ready(function () {
    loadDataTable().then(r => console.log(""));
});
let dataTable;
let loadDataTable = async () => {
    let count = 0;
    let resource = await CrudUtils.fetchResource(Constants.TXN_INTEREST_EXPENSE_PROJECTION_GET);
    dataTable = $('#datatable-editable').DataTable({
        paging: false,
        scrollX: true,
        scrollY: 400,
        ordering:false,
        dom: 'rtip',
        select: {
            style: 'os'
        },
        "data": resource.data,
        "columns": [
            {
                "data": "SNo",
                "render": function (data, type, row, meta) {
                    count = count + 1;
                    return count;
                }
            },
            {data: "mainType", defaultContent: ''},
            {data: "depositType", defaultContent: ''},
            {data: "depositoutcur", defaultContent: ''},
            {data: "depositoutcurtotalestimated", defaultContent: ''},
            {data: "firstqtrnextprojected", defaultContent: ''},
            {data: "secondqtrnextprojected", defaultContent: ''},
            {data: "thirdqtrnextprojected", defaultContent: ''},
            {data: "fourthqtrnextprojected", defaultContent: ''},
            {data: "firstqtrnextaverageoutstanding", defaultContent: ''},
            {data: "secondqtrnextaverageoutstanding", defaultContent: ''},
            {data: "thirdqtrnextaverageoutstanding", defaultContent: ''},
            {data: "fourthqtrnextaverageoutstanding", defaultContent: ''},
            {data: "interestrate", defaultContent: ''},
            {data: "firstqtrnextinterestexp", defaultContent: ''},
            {data: "secondqtrnextinterestexp", defaultContent: ''},
            {data: "thirdqtrnextinterestexp", defaultContent: ''},
            {data: "fourthqtrnextinterestexp", defaultContent: ''},
            {data: "totalnextinterestexp", defaultContent: ''}
        ],
        "destroy": true
    });
}