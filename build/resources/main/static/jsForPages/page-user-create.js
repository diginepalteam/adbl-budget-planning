import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";
import {CToastNotification} from "./c-toastNotification.js";

document.addEventListener('DOMContentLoaded', () => {
    setStaffsInChoices().then(r => console.log("Staffs loaded successfully."));
    setRolesChoices().then(r => console.log("Roles choices filled."));
    loadUsers().then(r => console.log("Users loaded successfully."));
});

//---------------------------------------SAVE USER ---------------------------------------------------
let sendUserSaveReq = () => {
    let crudUtils = new CrudUtils();
    let password = $("[name='password']").val();
    let rePass = $("[name='repass']").val();
    if (password !== rePass) {
        new CToastNotification()
            .getFailureToastNotification("Password Mismatch!");
    } else if ($("[name='staffId']").val() === '0' && $("[name='driverId']").val() === '0') {
        new CToastNotification().getFailureToastNotification("Please select staff or driver");
    }else if ($("[name='staffId']").val() !== '0' && $("[name='driverId']").val() !== '0') {
        new CToastNotification().getFailureToastNotification("Staff and driver both cannot be selected");
    } else {

        crudUtils.sendPostRequest(Constants.API_USER_SAVE, getUserFormData());
        setTimeout(function () {
            loadUsers().then(r => console.log("users loaded."));
        }, 200);

    }
};
let getUserFormData = () => {
    getNavigationJson();
    //get multiple values from roles select option
    let roles_list = $("[name='roles']").val();
    let roles = [];
    //convert roles selected values to json array object
    $(roles_list).each(function (index, element) {
        let data = {"id": element};
        roles.push(data);
    });

    let data ={
        "username": $("[name='username']").val(),
        "email": $("[name='email']").val(),
        "password": $("[name='password']").val(),
        "navigationJson": getNavigationJson(),
        ['roles']: roles
    };

    if($("[name='driverId']").val()!=='0'){
        data['driver'] = {
            "id": $("[name='driverId']").val()
        };
    }else if($("[name='staffId']").val()!=='0'){
        data['staffs'] = {
            "id": $("[name='staffId']").val()
        };
    }

    return JSON.stringify(data);
}

let getAdminNavigation = (type) => {
    let roles_name_list;
    if (type === 'edit') {
        roles_name_list = $("#editBlockRoles :selected").map(function () {
            return $(this).text();
        }).get();
    } else {
        roles_name_list = $("#roles :selected").map(function () {
            return $(this).text();
        }).get();
    }

    if ($.inArray('ROLE_SUPER_ADMIN', roles_name_list) > -1) {
        console.log("contains super admin");
        return [
            {
                "id": "user-settings-page",
                "value": "User Settings"
            },
            {
                "id": "office-settings-page",
                "value": "Office Settings"
            },
            {
                "id": "driver-settings-page",
                "value": "Driver Settings"
            },
            {
                "id": "staff-settings-page",
                "value": "Staff Settings"
            },
            {
                "id": "vehicle-settings-page",
                "value": "Vehicle Settings"
            },
            {
                "id": "vendor-settings-page",
                "value": "Vendor Settings"
            },
            {
                "id": "location-settings-page",
                "value": "Location Settings"
            },
            {
                "id": "rates-settings-page",
                "value": "Rates Settings"
            },
            {
                "id": "logo-settings-page",
                "value": "Logo Settings"
            },
            {
                "id": "packages-settings-page",
                "value": "Packages Settings"
            }
        ];
    }
    return [];
}

let getNavigationJson = () => {
    let navigation = {};

    let adminNavigation = getAdminNavigation();
    console.log(adminNavigation);


    $("#accessControl option:selected").each(function () {
        let $this = $(this);
        if ($this.length) {
            let text = $this.text();
            let value = $this.val();

            let data = {
                "id": value,
                "show": true,
                "value": text,
            };
            adminNavigation.push(data);
        }
    });

    navigation['menu'] = adminNavigation;
    return JSON.stringify(navigation);
}

//---------------------------USER CREATE FORM SELECT INITIALIZATION------------------------------------------------
let setStaffsInChoices = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_STAFFS_GET_ALL);
    let staffs = resource.data;
    staffs.forEach(function (staff, index) {
        const {firstName, lastName, id} = staff;
        let option = document.createElement("option");

        option.text = firstName + " " + lastName;
        option.value = id;

        let select = document.getElementById("staffId");
        select.appendChild(option);
    });
}

let setRolesChoices = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_ROLES_GET);
    let roles = resource.data;
    $("#roles").empty();
    roles.forEach(function (role, index) {
        const {name, id} = role;
        let option1 = document.createElement("option");
        option1.text = name;
        option1.value = id;

        let option2 = document.createElement("option");
        option2.text = name;
        option2.value = id;

        let editBlockRole = document.getElementById("editBlockRoles");
        let select = document.getElementById("roles");

        editBlockRole.appendChild(option1);
        select.appendChild(option2);
    })
}

//------------------------------USER DATATABLE LOAD------------------------------------------------------------
let loadUsers = async () => {
    let resource = await CrudUtils.fetchResource(Constants.API_USER_GET);
    $('#userTable').DataTable({
        "data": resource.data,
        "select": {
            "style": 'os'
        },
        "columns": [
            {data: "username", defaultContent: ""},
            {
                "data": "UserType",
                "orderable": false,
                "searchable": false,
                "render": function (data, type, row, meta) {
                    if(null!=row.staffs){
                        return "STAFF";
                    }else if(null!=row.driver){
                        return "DRIVER";
                    }else{
                        return "";
                    }
                },
            },
            {
                "data": "BranchCode",
                "orderable": false,
                "searchable": false,
                "render": function (data, type, row, meta) {
                    if(null!=row.staffs){
                        return row.staffs.branchCode;
                    }else{
                        return "";
                    }
                },
            },
            {
                "data": "Name",
                "orderable": false,
                "searchable": false,
                "render": function (data, type, row, meta) {
                    if(null!=row.staffs){
                        return row.staffs.firstName + " " + row.staffs.middleName + " " + row.staffs.lastName;
                    }else if(null!=row.driver){
                        return row.driver.firstName + " "+ row.driver.middleName + " "+ row.driver.lastName;
                    }else{
                        return "";
                    }
                },
            },
            {data: "status", defaultContent: ""},
            {
                "data": "Action",
                "orderable": false,
                "searchable": false,
                "render": function (data, type, row, meta) {
                    let username = "'" + row.username + "'";
                    let a = '<a type="button" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="EDIT ' + username + '" onclick="getEditUserValues(' + username + ')"><i class="fa fa-edit" aria-hidden="true"></i></a>' +
                        '&nbsp;&nbsp;&nbsp;<a type="button" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="DEACTIVATE ' + username + '" onclick="deactivateUser(' + username + ')"><i class="fa fa-times" aria-hidden="true"></i></a>' +
                        '&nbsp;&nbsp;&nbsp;<a type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="ACTIVATE ' + username + '" onclick="activateUser(' + username + ')"><i class="fa fa-check" aria-hidden="true"></i></a>';
                    return a;
                },

            }
        ],
        "destroy": true
    });
}

//--------------------------------------ACTIVATE/DEACTIVATE USER----------------------------------------------------
let deactivateUser = (username) => {
    new CrudUtils().sendPutRequest(Constants.API_USER_DEACTIVATE.replace("{username}", username));
    setTimeout(() => {
        loadUsers().then(r => console.log("Users loaded successfully."));
    }, 1000);

}

let activateUser = (username) => {
    new CrudUtils().sendPutRequest(Constants.API_USER_ACTIVATE.replace("{username}", username));
    setTimeout(() => {
        loadUsers().then(r => console.log("Users loaded successfully."));
    }, 1000);
}

// ------------------------------------UPDATE USER ACTIONS--------------------------------------------------
/**
 * On edit button click on datatable, set role, navigation values in edit user form
 * @param username
 * @returns {Promise<void>}
 */
let getEditUserValues = async (username) => {
    let url = Constants.API_USER_GET_ONE + username;
    $("[name='editBlockUsername']").val(username);
    const result = await CrudUtils.fetchResource(url);
    if (result.status === 200) {
        const {roles, navigationJson} = result.data;
        if (null != roles) {
            let roleIds = [];
            roles.forEach(function (role, index) {
                roleIds.push(role.id);
            });
            $("#editBlockRoles").val(roleIds).attr("selected", "selected").change();
        } else {
            $("#editBlockRoles").val("").attr("selected", "selected").change();
        }

        if (null != navigationJson && navigationJson !== '') {
            let accessControlNav = JSON.parse(navigationJson);
            let accessIds = [];
            $.each(accessControlNav.menu, function () {
                accessIds.push(this.id);
            });

            $("#editAccessControl").val(accessIds).attr("selected", "selected").change();
        } else {
            $("#editAccessControl").val("").attr("selected", "selected").change();
        }

        let user = "'" + username + "'";
        $("#updateUserBtn").attr("onClick", "updateUser(" + user + ")");
        $("#editUserModal").modal('show');
    } else {
        new CToastNotification().getFailureToastNotification("User details not found.");
    }

}

/**
 * Update user roles, navigation for provided username
 */
let updateUser = (username) => {
    let navigationJson = getEditNavigationJson();
    let roles = getEditRolesFormData();
    let data = JSON.stringify({
        navigationJson: navigationJson,
        roles: roles
    });
    new CrudUtils().sendPutRequest(Constants.API_USER_UPDATE.replace("{username}", username), data);
};

/**
 * Get roles array from edit user form for update
 * @returns {[]}
 */
let getEditRolesFormData = () => {
    //get multiple values from roles select option
    let roles_list = $("[name='editBlockRoles']").val();
    let array = [];

    //convert roles selected values to json array object
    $(roles_list).each(function (index, element) {
        let data = {"id": element};
        array.push(data);
    });

    return array;
}

/**
 * Get navigation JSON from edit user form for update
 * @returns {string}
 */
let getEditNavigationJson = () => {
    let navigation = {};

    let adminNavigation = getAdminNavigation('edit');

    $("#editAccessControl option:selected").each(function () {
        let $this = $(this);
        if ($this.length) {
            let text = $this.text();
            let value = $this.val();

            let data = {
                "id": value,
                "value": text
            };
            adminNavigation.push(data);
        }
    });
    navigation['menu'] = adminNavigation;
    return JSON.stringify(navigation);
}

window.getEditUserValues = getEditUserValues;
window.sendUserSaveReq = sendUserSaveReq;
window.activateUser = activateUser;
window.deactivateUser = deactivateUser;
window.updateUser = updateUser;


