import {CrudUtils} from "../c-crud-utils.js";
import * as Constants from "../c-constants.js";

$(document).ready(function () {
    loadDataTable().then(r => console.log(""));
});
let loadDataTable = async () => {
    let count = 0;
    let resource = await CrudUtils.fetchResource(Constants.TXN_INTEREST_COLLECTION_TARGET_GET);
    let dataTable = $('#datatable-editable').DataTable({
        paging: false,
        scrollX: true,
        scrollY: 400,
        ordering:false,
        dom: 'rtip',
        select: {
            style: 'os'
        },
        "data": resource.data,
        "columns": [
            {data: "head", defaultContent: ''},
            {data: "shrawan", defaultContent: ''},
            {data: "bhadra", defaultContent: ''},
            {data: "ahwin", defaultContent: ''},
            {data: "kartik", defaultContent: ''},
            {data: "mangsir", defaultContent: ''},
            {data: "paush", defaultContent: ''},
            {data: "magh", defaultContent: ''},
            {data: "falgun", defaultContent: ''},
            {data: "chaitra", defaultContent: ''},
            {data: "baisekh", defaultContent: ''},
            {data: "jesth", defaultContent: ''},
            {data: "ashadh", defaultContent: ''},
            {data: "total", defaultContent: ''}
        ],
        "destroy": true
    });
}
