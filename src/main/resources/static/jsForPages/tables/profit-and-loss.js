import {CrudUtils} from "../c-crud-utils.js";
import * as Constants from "../c-constants.js";

$(document).ready(function () {
  loadDataTable().then(r => console.log(""));
});
let loadDataTable = async () => {
  let count = 0;
  let resource = await CrudUtils.fetchResource(Constants.TXN_PROFIT_AND_LOSS_GET);
  let dataTable = $('#datatable-editable').DataTable({
    paging: false,
    scrollY: 400,
    ordering: true,
    order: [[1, 'asc']],
    dom: 'frtip',
    select: {
      style: 'os'
    },
    "data": resource.data,
    "columns": [
      {
        "data": "SNo",
        "render": function (data, type, row, meta) {
          count = count + 1;
          return count;
        }
      },
      {data: "description", defaultContent: ''},
      {data: "lastyearactual", defaultContent: ''},
      {data: "thisyearactual", defaultContent: ''},
      {data: "thisyearprojectedremained", defaultContent: ''},
      {data: "thisyeartotal", defaultContent: ''},
      {data: "nextyearprojected", defaultContent: ''},
      {data: "nextyearprojected", defaultContent: ''},
      {data: "nextyearprojected", defaultContent: ''},
      {data: "nextyearprojected", defaultContent: ''},
      {data: "nextyearprojected", defaultContent: ''}
    ],
    "destroy": true
  });

  $('#datatable-editable tfoot tr').append(
    '<td>Total</td>' +
    '<td></td>' +
    '<td>' + dataTable.column(2).data().sum() + '</td>' +
    '<td>' + dataTable.column(3).data().sum() + '</td>' +
    '<td>' + dataTable.column(4).data().sum() + '</td>' +

    '<td>' + dataTable.column(5).data().sum() + '</td>' +
    '<td>' + dataTable.column(6).data().sum() + '</td>' +
    '<td>' + dataTable.column(7).data().sum() + '</td>' +

    '<td>' + dataTable.column(8).data().sum() + '</td>' +
    '<td>' + dataTable.column(9).data().sum() + '</td>' +
    '<td>' + dataTable.column(10).data().sum() + '</td>'
  );
}
