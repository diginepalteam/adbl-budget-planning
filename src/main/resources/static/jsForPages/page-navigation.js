import * as Constants from './c-constants.js';
import * as Util from './c-utils.js';
import {CAuthentication} from "./c-authentication.js";
import {CrudUtils} from "./c-crud-utils.js";

/**
 * Token validation
 */
$(document).ready(function () {
  checkCurrentAuthToken().then(r => console.log("token verified."));

  $('#navbarId').on('click','.toggleChildNav',function () {
    $(this).siblings("ul").toggle('slow',function () {});
  });
});

//todo validate token if not then refresh
let checkCurrentAuthToken = async () => {
  if (null == new CAuthentication().getCurrentToken()) {
    loadLoginPage();
  } else {
    document.getElementsByTagName("html")[0].style.visibility = "visible";

    await loadBudgetDate();
    await loadProfileDetailsInTopBar();
  }
}
let loadBudgetDate = async () => {
  let resource = await CrudUtils.fetchResource(Constants.VERSION_1 + Constants.MD_BUDGET_DATE_PARAMETER);
  console.log(resource.data);
  if (resource.status === 200) {
    let data = resource.data;
    const {currentYearActual, currentYearGrowth, currentYearProjection, nextYearProjection, nextYearEnd} = data;
    $(".currentYearActual").html(currentYearActual);
    $(".currentYearGrowth").html(currentYearGrowth);
    $(".currentYearProjection").html(currentYearProjection);
    $(".nextYearProjection").html(nextYearProjection);
    $(".nextYearEnd").html(nextYearEnd);
  }
}
/**
 * profile details and navigation
 * @returns {Promise<void>}
 */
let loadProfileDetailsInTopBar = async () => {
  let resource = await CrudUtils.fetchResource(Constants.API_USER_PROFILE_GET);
  console.log(resource.data);
  if (resource.status === 200) {
    if (null != resource.data.staffs) {
      let {firstName, middleName, lastName, phoneNumber, email, branch} = resource.data.staffs;

      let fullName;
      if (null != firstName && null != middleName && null != lastName) {
        fullName = firstName.concat(" ").concat(middleName).concat(" ").concat(lastName);
      } else if (null != firstName && null != lastName) {
        fullName = firstName.concat(" ").concat(lastName);
      } else {
        fullName = firstName;
      }
      $("#profileName").html(fullName);
      $("#loginBranchCode").html(branch.branchCode);
    }

    const {username, roles, navigationJson} = resource.data;
    $("#profileUsername").html(username);

    setDynamicNavBar(
      JSON.parse(navigationJson)
    );
  }

  function setDynamicNavBar(navJson) {
    if (null !== navJson) {
      navJson.menu.unshift({
        id: 'dashboard-page',
        value: 'Dashboard'
      });

      let getIconClass = (id) => {
        let $icon;
        if (id.includes('dashboard')) {
          $icon = 'fa fa-home';
        } else if (id.includes('user')) {
          $icon = 'fa fa-users';
        } else if (id.includes('office')) {
          $icon = 'fa fa-briefcase';
        } else if (id.includes('staff')) {
          $icon = 'fa fa-users';
        } else if (id.includes('logo')) {
          $icon = 'fa fa-photo';
        } else {
          $icon = 'fa fa-file-text-o';
        }
        return $icon;
      }
      $.each(navJson.menu, function (i, item) {
        let $navList = '';

        $.each(item.children, function (i, item) {
          let $pageUrl = Constants.BASE_URL + item.pageUrl;
          let $id = item.id;
          let $value = item.value;

          let $icon = getIconClass($id);

          let $singularNav = '<li><a id="' + $id + '" href="' + $pageUrl + '"> <i class="' + $icon + '" aria-hidden="true"></i> <span>' + $value + '</span> </a></li>';

          $navList = $navList + $singularNav;
        });

        if(item.parent) {
          let $parentLabel = item.parentLabel;
          let $parentNav = '<li class="nav-parent nav-active"> <a href="#" class="toggleChildNav"> <i class="fa fa-list-alt" aria-hidden="true"></i> <span>' + $parentLabel + '</span> </a> <ul class="nav nav-children"> ' + $navList + '</ul> </li>';

          $("#navbarId").append($parentNav);
        }else{
          $("#navbarId").append($navList);
        }
      });
    }
  }
}

let loadNavigationPage = (pageUrl) => {
  Util.loadPage(Constants.BASE_URL + pageUrl);
};

/**
 * Profile and session out page
 */
document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('userProfileViewPageBtn').addEventListener('click', loadUserProfilePage);
});

let loadLoginPage = () => Util.loadPage(Constants.UI_LOGIN_PAGE);
let loadUserProfilePage = () => Util.loadPage(Constants.UI_USER_PROFILE_VIEW_PAGE);


window.loadNavigationPage = loadNavigationPage;
