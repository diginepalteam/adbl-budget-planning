import {CrudUtils} from "./c-crud-utils.js";
import * as Constants from "./c-constants.js";
import {CToastNotification} from "./c-toastNotification.js";

$(document).ready(function () {
  loadDataTable().then(r => console.log("data loaded..."));
});

let sendSaveRq = () => {
  new CrudUtils().sendPostRequest(Constants.TXN_COMMITS_POST, getFormData());
  setTimeout(function () {
    loadDataTable().then(r => console.log("data loaded..."));
  }, 200);
};

let getFormData = () => {
  let currentoutstanding = $("[name='currentoutstanding']").val();
  let disbursementwithrenewal = $("[name='disbursementwithrenewal']").val();
  let collectiontwithrenewaloverdue = $("[name='collectiontwithrenewaloverdue']").val();
  let collectiontwithrenewaldue = $("[name='collectiontwithrenewaldue']").val();
  let outstandinginashadh = $("[name='outstandinginashadh']").val();
  return JSON.stringify({
      currentoutstanding,
      disbursementwithrenewal,
      collectiontwithrenewaloverdue,
      collectiontwithrenewaldue,
      outstandinginashadh
    }
  );
}

let loadDataTable = async () => {
  let count = 0;
  let resource = await CrudUtils.fetchResource(Constants.TXN_COMMITS_GET);
  let dataTable = $('#datatable-editable').DataTable({
    paging: false,
    dom: 'Brtip',
    ordering: false,
    select: {
      style: 'os'
    },
    buttons: [
      {
        text: '<i class="fa fa-edit"></i> Edit',
        className: 'btn btn-info',
        action: function () {
          if (dataTable.rows({selected: true}).count() === 0) {
            new CToastNotification().getFailureToastNotification("No records selected!");
          } else if (dataTable.rows({selected: true}).count() > 1) {
            new CToastNotification().getFailureToastNotification("Please select 1 record!");
          } else {
            let selectedRowData = dataTable.rows({selected: true}).data()[0];
            loadDataTableModal(selectedRowData);
          }
        }
      }
    ],
    "data": resource.data,
    "columns": [
      {data: "currentoutstanding", defaultContent: ''},
      {data: "disbursementwithrenewal", defaultContent: ''},
      {data: "collectiontwithrenewaloverdue", defaultContent: ''},
      {data: "collectiontwithrenewaldue", defaultContent: ''},
      {data: "outstandinginashadh", defaultContent: ''},
    ],
    "destroy": true
  });
}

let loadDataTableModal = async (data) => {
  if (null != data.id && data.id > 0) {
    $("#currentYearRemainingCommitForm :input").each(function (e) {
      this.value = data[this.id];
    });
  } else {
    new CToastNotification().getFailureToastNotification("ID not found!");
  }
}

window.sendSaveRq = sendSaveRq;
window.getFormData = getFormData;
window.loadDataTableModal=loadDataTableModal;
