import {CrudUtils} from "./c-crud-utils.js";
import * as Constants from "./c-constants.js";
import {CToastNotification} from "./c-toastNotification.js";

$(document).ready(function () {

});

let triggerMisBudgetExpenseSync = async () => {
  CrudUtils.showLoader();
  let resource = await CrudUtils.fetchResource(Constants.MIS_SYNC_EXPENSE_BUDGET_GET);
  if(null!=resource && resource.status ===200){
    new CToastNotification().getSuccessToastNotification("Expense budget sync completed.");
  }else{
    new CToastNotification().getFailureToastNotification("Expense budget sync failure.");
  }
  CrudUtils.hideLoader();
}

let triggerInterestLoanCollectionTarget = async () => {
  CrudUtils.showLoader();
  let resource = await CrudUtils.fetchResource(Constants.MIS_SYNC_INTEREST_LOAN_COLLECTION_GET);
  if(null!=resource && resource.status ===200){
    new CToastNotification().getSuccessToastNotification("Interest loan collection sync completed.");
  }else{
    new CToastNotification().getFailureToastNotification("Interest loan collection sync failure.");
  }
  CrudUtils.hideLoader();
}

let triggerPostLoanEvent = async () => {
  CrudUtils.showLoader();
  let resource = await CrudUtils.fetchResource(Constants.MIS_SYNC_POST_LOAN_EVENT_GET);
  if(null!=resource && resource.status ===200){
    new CToastNotification().getSuccessToastNotification("Post loan collection sync completed.");
  }else{
    new CToastNotification().getFailureToastNotification("Post loan collection sync failure.");
  }
  CrudUtils.hideLoader();
}

let triggerProfitAndLossSyncEvent = async () => {
  CrudUtils.showLoader();
  let resource = await CrudUtils.fetchResource(Constants.MIS_SYNC_PROFIT_AND_LOSS_EVENT_GET);
  if(null!=resource && resource.status ===200){
    new CToastNotification().getSuccessToastNotification("Profit and loss sync completed.");
  }else{
    new CToastNotification().getFailureToastNotification("Profit and loss sync failure.");
  }
  CrudUtils.hideLoader();
}
let triggerBalanceSheetSyncEvent = async () => {
  CrudUtils.showLoader();
  let resource = await CrudUtils.fetchResource(Constants.MIS_SYNC_BALANCE_SHEET_EVENT_GET);
  if(null!=resource && resource.status ===200){
    new CToastNotification().getSuccessToastNotification("Balance sheet sync completed.");
  }else{
    new CToastNotification().getFailureToastNotification("Balance sheet sync failure.");
  }
  CrudUtils.hideLoader();
}

document.triggerMisBudgetExpenseSync = triggerMisBudgetExpenseSync;
document.triggerInterestLoanCollectionTarget = triggerInterestLoanCollectionTarget;
document.triggerPostLoanEvent = triggerPostLoanEvent;
document.triggerProfitAndLossSyncEvent = triggerProfitAndLossSyncEvent;
document.triggerBalanceSheetSyncEvent = triggerBalanceSheetSyncEvent;
