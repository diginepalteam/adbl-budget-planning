import * as Constants from "./c-constants.js";
import {CAuthentication} from "./c-authentication.js";

$( "#loginForm" ).submit(function( event ) {
    event.preventDefault();
    let myform = document.getElementById("loginForm");
    let fd = new FormData(myform );

    new CAuthentication().sendAuthenticationRequest(
        Constants.API_OAUTH_GET,
        fd
    )
});