import * as Constants from "./c-constants.js";
import {CrudUtils} from "./c-crud-utils.js";

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('logoutBtn').addEventListener('click', sendLogoutReq);
});

export let sendLogoutReq = async () => {
    await CrudUtils.fetchResource(Constants.API_LOGOUT).then(r => console.log("Logout request completed."));
    sessionStorage.clear();
    window.location = Constants.BASE_URL;
}