export let isEmptyJson = obj => Object.keys(obj).length === 0;

export let triggerOnError = (xhr) => {
    console.log(xhr);
    let data = JSON.parse(xhr.responseText);

    const {statusCode, message, errors} = data;

    $('#errorResponse #statusCode').html(statusCode);
    $('#errorResponse #msg').html(message);
    $('#errorResponse #errors').empty()
    $(errors).each(
        function (index, value) {
            $("#errorResponse #errors").append(
                $(`<span>${value}</span><br>`)
            );
        });
    $('#errorResponse').modal('show');
}

export let loadPage = url => window.location = url;