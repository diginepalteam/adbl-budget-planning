import {CAuthentication} from "./c-authentication.js";
import {CToastNotification} from "./c-toastNotification.js";

export class CrudUtils {
    sendPostRequest(url, data) {
        this.sendRequest(url, data, 'POST');
    }

    sendPutRequest(url, data) {
        this.sendRequest(url, data, 'PUT');
    }

    sendPatchRequest(url, data) {
        this.sendRequest(url, data, 'PATCH');
    }

    sendDeleteRequest(url) {
        this.sendRequest(url, null, 'DELETE');
    }

    sendDeleteRequestWithPayload(url, data) {
        this.sendRequest(url, data, 'DELETE');
    }

    sendMultiPartPostRequest(url, data) {
        return this.sendMultiPartRequest(url, data, 'POST');
    }

    sendRequest(url, data, methodType) {
        let authentication = new CAuthentication();
        if (confirm('Are you sure?')) {
            let req = new Request(url, {
                method: methodType,
                mode: 'cors',
                headers: authentication.getHeadersWithToken(),
                body: data
            });
            fetch(req)
                .then(resp => resp.json())
                .then(responseData => {
                    const {message: msg, status} = responseData;
                    if (status === 200) {
                        new CToastNotification()
                            .getSuccessToastNotification(msg);
                    } else if (status === 500) {
                        new CToastNotification().getFailureToastNotification("Something went wrong.");
                    } else {
                        new CToastNotification().getFailureToastNotification(msg);
                    }
                    return responseData;
                })
                .catch(err => {
                    console.error(err.message);
                    new CToastNotification().getFailureToastNotification(err.message);
                });
        }
    }

    static async fetchResource(url) {
        let req = new Request(url, {
            method: 'GET',
            mode: 'cors',
            headers: new CAuthentication().getHeadersWithToken()
        });

        return await fetch(req)
            .then(response => {
                if (response.ok) return response.json()
            })
            .then(json => {
                return json;
            })
            .catch(err => {
                console.error(`Error: ${err.message}`);
            });
    }

    static async fetchExternalResource(url) {
        let req = new Request(url, {
            method: 'GET',
            mode: 'cors'
        });

        return await fetch(req)
            .then(response => {
                if (response.ok) return response.json()
            })
            .then(json => {
                return json;
            })
            .catch(err => {
                console.error(`Error: ${err.message}`);
            });
    }

    sendMultiPartRequest(url, data, methodType) {
        let authentication = new CAuthentication();
        if (confirm('Are you sure?')) {
            let req = new Request(url, {
                method: methodType,
                mode: 'cors',
                enctype: "multipart/form-data",
                headers: authentication.getMultiPartHeadersWithToken(),
                body: data,
                contentType: false
            });
            return fetch(req)
                .then(resp => {
                    if (resp.ok) return resp.json();
                })
                .then(json => {
                    const {status, message} = json;
                    if (status === 200) {
                        new CToastNotification().getSuccessToastNotification(message);
                    } else {
                        new CToastNotification().getFailureToastNotification(message);
                    }
                    return json;
                })
                .catch(err => {
                    console.error(`Error: ${err.message}`);
                });
        }
    }

    static downloadExcelFile(url, fileNameWithExtension) {
        this.downloadFile(url, fileNameWithExtension,"application/vnd.ms-excel");
    }

    static downloadPdfFile(url, fileNameWithExtension) {
        this.downloadFile(url, fileNameWithExtension,"application/pdf");
    }

    static downloadFile(url, nameWithExtension, type) {
        let req = new Request(url, {
            method: 'GET',
            mode: 'cors',
            headers: new CAuthentication().getHeadersWithToken()
        });
        fetch(url, req)
            .then(response => response.blob())
            .then(blob => {
                download(blob, nameWithExtension, type);
            });
    }

  static showLoader(){
    $('#loadingModal').modal({backdrop:'static', keyboard: false});
    $('#loadingModal').modal('show');
  }
  static hideLoader(){
    $('#loadingModal').modal('hide');
  }
}
