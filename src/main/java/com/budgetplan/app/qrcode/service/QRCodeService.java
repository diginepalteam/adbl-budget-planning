
package com.budgetplan.app.qrcode.service;

public interface QRCodeService {

	byte[] getQRCode(String text, int width, int height);

}
