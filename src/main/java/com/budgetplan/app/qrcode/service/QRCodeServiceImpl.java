
package com.budgetplan.app.qrcode.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;

/**
 * <p>
 * This class is used for QRCode generation
 * </p>
 */
@Slf4j
@Service
public class QRCodeServiceImpl implements QRCodeService {

	@Override
	public byte[] getQRCode(String text, int width, int height) {
		try {
			QRCodeWriter qrCodeWriter = new QRCodeWriter();
			BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);
			return byteArrayOutputStream.toByteArray();
		}
		catch (Exception e) {
			log.error("Exception generating qr code for text {}, width {}, height {}", text, width, height, e);
			return null;
		}
	}

}
