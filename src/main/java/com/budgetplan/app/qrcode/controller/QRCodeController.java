
package com.budgetplan.app.qrcode.controller;

import com.budgetplan.app.qrcode.service.QRCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Shishir Karki
 */
@RestController
@RequestMapping("impl/api/v1")
public class QRCodeController {

	private QRCodeService qrCodeService;

	@Autowired
	public void setQrCodeService(QRCodeService qrCodeService) {
		this.qrCodeService = qrCodeService;
	}

	@GetMapping(value = "/qrcodes/{text}", produces = "image/png")
	public ResponseEntity<?> generateQrCodeForText(@PathVariable String text) {
		return new ResponseEntity<>(qrCodeService.getQRCode(text, 200, 200), HttpStatus.OK);
	}

}
