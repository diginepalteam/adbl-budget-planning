
package com.budgetplan.app.apiresponse;

import lombok.Getter;

@Getter
public enum EApiResponseMessages {
    POD_STATUS_UPDATED("POD status updated."),
    POD_UPDATE_FAILED("POD update failed.");
    private final String message;

    EApiResponseMessages(String message) {
        this.message = message;
    }
}
