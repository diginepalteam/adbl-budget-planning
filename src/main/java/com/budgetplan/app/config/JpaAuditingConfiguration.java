
package com.budgetplan.app.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Slf4j
@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class JpaAuditingConfiguration {
    @Bean
    public AuditorAware<String> auditorProvider() {

        /*
          if you are using spring security, you can get the currently logged username with following code segment.
          SecurityContextHolder.getContext().getAuthentication().getName()
         */
        return new AuditorAware<String>() {
            @Override
            public Optional<String> getCurrentAuditor() {
                try {
                    String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
                    if (StringUtils.isEmpty(currentUser)) {
                        return Optional.of(currentUser);
                    }
                } catch (Exception e) {
                    log.warn("JpaAuditingConfiguration.getCurrentAuditor authentication exception {}", e.getLocalizedMessage());
                }
                return Optional.of("system");
            }
        };
    }

}
