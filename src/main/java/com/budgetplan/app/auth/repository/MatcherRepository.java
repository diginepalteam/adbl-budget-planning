
package com.budgetplan.app.auth.repository;

import com.budgetplan.app.auth.entity.SecurityMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatcherRepository extends JpaRepository<SecurityMapping, Integer> {
}
