
package com.budgetplan.app.auth.repository;

import com.budgetplan.app.auth.entity.TokenBlackList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenBlackListRepository extends JpaRepository<TokenBlackList, Integer> {
    Optional<TokenBlackList> findByToken(String token);

}
