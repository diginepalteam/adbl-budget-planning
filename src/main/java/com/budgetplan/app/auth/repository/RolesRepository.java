
package com.budgetplan.app.auth.repository;

import com.budgetplan.app.auth.entity.Roles;
import com.budgetplan.app.auth.enums.ERole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RolesRepository extends JpaRepository<Roles, Long> {
    Optional<Roles> findByName(ERole eRole);

}
