
package com.budgetplan.app.auth.repository;

import com.budgetplan.app.auth.entity.User;
import com.budgetplan.app.auth.enums.EUserStatus;
import com.budgetplan.app.staffs.Staffs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsernameAndStatusNot(String username, EUserStatus status);

    List<User> findByStatusNot(EUserStatus status);

    Optional<User> findByUsernameAndStatusIn(String username, List<EUserStatus> validUserStatus);

    List<User> findAllByStaffsIn(List<Staffs> staffsList);
}
