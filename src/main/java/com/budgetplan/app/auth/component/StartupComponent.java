
package com.budgetplan.app.auth.component;

import com.budgetplan.app.auth.constants.ApiConstants;
import com.budgetplan.app.auth.entity.Roles;
import com.budgetplan.app.auth.entity.SecurityMapping;
import com.budgetplan.app.auth.entity.User;
import com.budgetplan.app.auth.enums.ERole;
import com.budgetplan.app.auth.enums.EUserStatus;
import com.budgetplan.app.auth.enums.MatcherStatus;
import com.budgetplan.app.auth.repository.RolesRepository;
import com.budgetplan.app.auth.repository.UserRepository;
import com.budgetplan.app.auth.service.MatcherService;
import com.budgetplan.app.constants.JspPageAPIConstants;
import com.budgetplan.app.md.entity.*;
import com.budgetplan.app.md.repository.*;
import com.budgetplan.app.staffs.Staffs;
import com.budgetplan.app.staffs.StaffsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.ManagedList;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class StartupComponent {
  @Value("${initial.user.name}")
  private String username;
  @Value("${initial.user.password}")
  private String password;

  private UserRepository usersRepo;
  private RolesRepository rolesRepo;
  private MatcherService matcherService;
  private MdBranchRepository mdBranchRepository;
  private MdLoanTypeRepository mdLoanTypeRepository;
  private MdExpenseMainRepository mdExpenseMainRepository;
  private MdExpenseCodeRepository mdExpenseCodeRepository;
  private MdIncomeFromLoanRepository mdIncomeFromLoanRepository;
  private StaffsService staffsService;


  @Autowired
  public void setUsersRepo(UserRepository usersRepo) {
    this.usersRepo = usersRepo;
  }


  @Autowired
  public void setRolesRepo(RolesRepository rolesRepo) {
    this.rolesRepo = rolesRepo;
  }

  @Autowired
  public void setMatcherService(MatcherService matcherService) {
    this.matcherService = matcherService;
  }


  @Autowired
  public void setStaffsService(StaffsService staffsService) {
    this.staffsService = staffsService;
  }

  @Autowired
  public void setMdBranchRepository(MdBranchRepository mdBranchRepository) {
    this.mdBranchRepository = mdBranchRepository;
  }

  @Autowired
  public void setMdLoanTypeRepository(MdLoanTypeRepository mdLoanTypeRepository) {
    this.mdLoanTypeRepository = mdLoanTypeRepository;
  }

  @Autowired
  public void setMdExpenseMainRepository(MdExpenseMainRepository mdExpenseMainRepository) {
    this.mdExpenseMainRepository = mdExpenseMainRepository;
  }

  @Autowired
  public void setMdExpenseCodeRepository(MdExpenseCodeRepository mdExpenseCodeRepository) {
    this.mdExpenseCodeRepository = mdExpenseCodeRepository;
  }

  @Autowired
  public void setMdIncomeFromLoanRepository(MdIncomeFromLoanRepository mdIncomeFromLoanRepository) {
    this.mdIncomeFromLoanRepository = mdIncomeFromLoanRepository;
  }

  private void saveRoles() {
    if (rolesRepo.findAll().isEmpty()) {
      List<Roles> rolesToSave = Lists.newArrayList();
      ERole[] roleEnums = ERole.values();
      for (ERole roleEnum : roleEnums) {
        Roles role = new Roles();
        role.setName(roleEnum);
        rolesToSave.add(role);
      }
      rolesRepo.saveAll(rolesToSave);
    }
  }

  @EventListener(ApplicationReadyEvent.class)
  public void doSomethingAfterStartup() {
    this.runScript();
    this.setUpData();
    this.saveInitialMatcher();
    log.info("Initial security_mapping setup completed.");
    this.saveRoles();
    log.info("Initial roles setup completed.");
    this.createUser();
    log.info("Initial user setup completed.");

  }

  private void runScript() {
    List<User> users = usersRepo.findAll();
    users.forEach(user -> {
      user.setNavigationJson("{ \"menu\": [ { \"parent\": true, \"parentLabel\": \"Navigation 1\", \"children\": [ { \"id\": \"deposit-detail-and-projection\", \"value\": \"Deposit detail and projection\", \"pageUrl\": \"/v1/pages/navs/DEPOSIT_DETAIL_AND_PROJECTION\" }, { \"id\": \"loan-detail-and-projection\", \"value\": \"Loan detail and projection\", \"pageUrl\": \"/v1/pages/navs/LOAN_DETAIL_AND_PROJECTION\" }, { \"id\": \"loan-disbursement-target\", \"value\": \"Loan Disbursement Target\", \"pageUrl\": \"/v1/pages/navs/LOAN_DISBURSEMENT_TARGET\" }, { \"id\": \"due-loan-collection-target\", \"value\": \"Due Loan Collection Target\", \"pageUrl\": \"/v1/pages/navs/DUE_LOAN_COLLECTION_TARGET\" }, { \"id\": \"overdue-loan-collection-target\", \"value\": \"Overdue Loan Collection Target\", \"pageUrl\": \"/v1/pages/navs/OVERDUE_LOAN_COLLECTION_TARGET\" }, { \"id\": \"loan-growth-detail\", \"value\": \"Loan Growth Detail\", \"pageUrl\": \"/v1/pages/navs/LOAN_GROWTH_DETAIL\" }, { \"id\": \"loan-classification\", \"value\": \"Loan Classification\", \"pageUrl\": \"/v1/pages/navs/LOAN_CLASSIFICATION\" }, { \"id\": \"interest-collection-target\", \"value\": \"Interest Collection Target\", \"pageUrl\": \"/v1/pages/navs/INTEREST_COLLECTION_TARGET\" }, { \"id\": \"current-year-remaining-commits\", \"value\": \"Commits\", \"pageUrl\": \"/v1/pages/navs/COMMITS\" }, { \"id\": \"interest-expense-projection\", \"value\": \"Interest Expense Projection\", \"pageUrl\": \"/v1/pages/navs/INTEREST_EXPENSE_PROJECTION\" }, { \"id\": \"karmachari-darbandi\", \"value\": \"Karmachari darbandi\", \"pageUrl\": \"/v1/pages/navs/KARMACHARI_DARBANDI\" }, { \"id\": \"expense-budget\", \"value\": \"Expense budget\", \"pageUrl\": \"/v1/pages/navs/EXPENSE_BUDGET\" }, { \"id\": \"income-from-fee-and-commission-target\", \"value\": \"Income from fee and commission target\", \"pageUrl\": \"/v1/pages/navs/INCOME_FROM_FEE_AND_COMMISSION_TARGET\" }, { \"id\": \"projection-profit-and-loss\", \"value\": \"Projection Profit and loss\", \"pageUrl\": \"/v1/pages/navs/PROJECTION_PROFIT_AND_LOSS\" }, { \"id\": \"projection-balance-sheet\", \"value\": \"Projection Balance sheet\", \"pageUrl\": \"/v1/pages/navs/PROJECTION_BALANCE_SHEET\" }, { \"id\": \"loan-write-off-sheet\", \"value\": \"Loan write-off detail\", \"pageUrl\": \"/v1/pages/navs/LOAN_WRITE_OFF_DETAIL\" }, { \"id\": \"non-banking-asset-detail\", \"value\": \"Non-banking asset detail\", \"pageUrl\": \"/v1/pages/navs/NON_BANKING_ASSET_DETAIL\" }, { \"id\": \"performance-planning\", \"value\": \"Performance planning\", \"pageUrl\": \"/v1/pages/navs/PERFORMANCE_PLANNING\" }, { \"id\": \"house-rent-detail\", \"value\": \"House rent detail\", \"pageUrl\": \"/v1/pages/navs/HOUSE_RENT_DETAIL\" }, { \"id\": \"physical-assets-details\", \"value\": \"Physical assets details\", \"pageUrl\": \"/v1/pages/navs/PHYSICAL_ASSETS_DETAILS\" } ] }, { \"parent\": true, \"parentLabel\": \"Navigation 2\", \"children\": [ { \"id\": \"budget-date-parameter\", \"value\": \"Budget Date Parameter\", \"pageUrl\": \"/v1/pages/navs/BUDGET_DATE_PARAMETER\" }, { \"id\": \"profit-and-loss\", \"value\": \"Profit And Loss\", \"pageUrl\": \"/v1/pages/navs/PROFIT_AND_LOSS\" } ] }, { \"parent\": false, \"children\": [ { \"id\": \"reports\", \"value\": \"Reports\", \"pageUrl\": \"/v1/pages/navs/REPORTS\" }, { \"id\": \"mis-triggers\", \"value\": \"MIS Sync\", \"pageUrl\": \"/v1/pages/navs/MIS_TRIGGERS\" } ] } ] }");
    });
    usersRepo.saveAll(users);
  }

  protected void setUpData() {
    if (mdBranchRepository.count() == 0) {
      List<MdBranch> branches = new ArrayList<>();
      try {
        ObjectMapper mapper = new ObjectMapper();
        branches.addAll(mapper.readValue(new ClassPathResource("md_branch.json").getFile(), mapper.getTypeFactory().constructCollectionType(List.class, MdBranch.class)));
        mdBranchRepository.saveAll(branches);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    if (mdLoanTypeRepository.count() == 0) {
      List<MdLoanType> loanTypes = new ArrayList<>();
      try {
        ObjectMapper mapper = new ObjectMapper();
        loanTypes.addAll(mapper.readValue(new ClassPathResource("md_loantype.json").getFile(), mapper.getTypeFactory().constructCollectionType(List.class, MdLoanType.class)));
        mdLoanTypeRepository.saveAll(loanTypes);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    if (mdExpenseMainRepository.count() == 0) {
      List<MdExpenseMain> expenseMains = new ArrayList<>();
      try {
        ObjectMapper mapper = new ObjectMapper();
        expenseMains.addAll(mapper.readValue(new ClassPathResource("md_expensemain.json").getFile(), mapper.getTypeFactory().constructCollectionType(List.class, MdExpenseMain.class)));
        mdExpenseMainRepository.saveAll(expenseMains);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    if (mdExpenseCodeRepository.count() == 0) {
      List<MdExpenseCode> expenseCodes = new ArrayList<>();
      try {
        ObjectMapper mapper = new ObjectMapper();
        expenseCodes.addAll(mapper.readValue(new ClassPathResource("md_expensecode.json").getFile(), mapper.getTypeFactory().constructCollectionType(List.class, MdExpenseCode.class)));
        mdExpenseCodeRepository.saveAll(expenseCodes);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    if (mdIncomeFromLoanRepository.count() == 0) {
      List<MdIncomeFromLoan> incomeFromLoans = new ArrayList<>();
      try {
        ObjectMapper mapper = new ObjectMapper();
        incomeFromLoans.addAll(mapper.readValue(new ClassPathResource("md_incomefromloan.json").getFile(), mapper.getTypeFactory().constructCollectionType(List.class, MdIncomeFromLoan.class)));
        mdIncomeFromLoanRepository.saveAll(incomeFromLoans);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private void createUser() {
    if (usersRepo.findAll().isEmpty()) {
      List<User> users = new ArrayList<>();

      mdBranchRepository.findAll().forEach(mdBranch -> {
        Staffs staffs = new Staffs();
        staffs.setBranch(mdBranch);
        staffs.setFirstName(mdBranch.getBranchCode());

        Optional<Staffs> staffsOptional = staffsService.save(staffs);

        staffsOptional.ifPresent(staff -> {
          BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

          User user = new User();
          Optional<Roles> roleOptional = rolesRepo.findByName(ERole.ROLE_SUPER_ADMIN);
          roleOptional.ifPresent(role -> user.setRoles(Collections.singleton(role)));
          user.setUsername(mdBranch.getBranchCode());
          user.setPassword(encoder.encode(mdBranch.getBranchCode()));
          user.setStatus(EUserStatus.ACTIVE);
          user.setStaffs(staff);
          user.setNavigationJson("{ \"menu\": [ { \"id\": \"deposit-detail-and-projection\", \"value\": \"Deposit detail and projection\", \"pageUrl\": \"/v1/pages/navs/DEPOSIT_DETAIL_AND_PROJECTION\" }, { \"id\": \"loan-detail-and-projection\", \"value\": \"Loan detail and projection\", \"pageUrl\": \"/v1/pages/navs/LOAN_DETAIL_AND_PROJECTION\" }, { \"id\": \"loan-disbursement-target\", \"value\": \"Loan Disbursement Target\", \"pageUrl\": \"/v1/pages/navs/LOAN_DISBURSEMENT_TARGET\" }, { \"id\": \"due-loan-collection-target\", \"value\": \"Due Loan Collection Target\", \"pageUrl\": \"/v1/pages/navs/DUE_LOAN_COLLECTION_TARGET\" }, { \"id\": \"overdue-loan-collection-target\", \"value\": \"Overdue Loan Collection Target\", \"pageUrl\": \"/v1/pages/navs/OVERDUE_LOAN_COLLECTION_TARGET\" }, { \"id\": \"loan-growth-detail\", \"value\": \"Loan Growth Detail\", \"pageUrl\": \"/v1/pages/navs/LOAN_GROWTH_DETAIL\" }, { \"id\": \"loan-classification\", \"value\": \"Loan Classification\", \"pageUrl\": \"/v1/pages/navs/LOAN_CLASSIFICATION\" }, { \"id\": \"interest-collection-target\", \"value\": \"Interest Collection Target\", \"pageUrl\": \"/v1/pages/navs/INTEREST_COLLECTION_TARGET\" }, { \"id\": \"current-year-remaining-commits\", \"value\": \"Commits\", \"pageUrl\": \"/v1/pages/navs/COMMITS\" }, { \"id\": \"interest-expense-projection\", \"value\": \"Interest Expense Projection\", \"pageUrl\": \"/v1/pages/navs/INTEREST_EXPENSE_PROJECTION\" }, { \"id\": \"karmachari-darbandi\", \"value\": \"Karmachari darbandi\", \"pageUrl\": \"/v1/pages/navs/KARMACHARI_DARBANDI\" }, { \"id\": \"expense-budget\", \"value\": \"Expense budget\", \"pageUrl\": \"/v1/pages/navs/EXPENSE_BUDGET\" }, { \"id\": \"income-from-fee-and-commission-target\", \"value\": \"Income from fee and commission target\", \"pageUrl\": \"/v1/pages/navs/INCOME_FROM_FEE_AND_COMMISSION_TARGET\" }, { \"id\": \"projection-profit-and-loss\", \"value\": \"Projection Profit and loss\", \"pageUrl\": \"/v1/pages/navs/PROJECTION_PROFIT_AND_LOSS\" }, { \"id\": \"projection-balance-sheet\", \"value\": \"Projection Balance sheet\", \"pageUrl\": \"/v1/pages/navs/PROJECTION_BALANCE_SHEET\" }, { \"id\": \"loan-write-off-sheet\", \"value\": \"Loan write-off detail\", \"pageUrl\": \"/v1/pages/navs/LOAN_WRITE_OFF_DETAIL\" }, { \"id\": \"non-banking-asset-detail\", \"value\": \"Non-banking asset detail\", \"pageUrl\": \"/v1/pages/navs/NON_BANKING_ASSET_DETAIL\" }, { \"id\": \"performance-planning\", \"value\": \"Performance planning\", \"pageUrl\": \"/v1/pages/navs/PERFORMANCE_PLANNING\" }, { \"id\": \"house-rent-detail\", \"value\": \"House rent detail\", \"pageUrl\": \"/v1/pages/navs/HOUSE_RENT_DETAIL\" }, { \"id\": \"physical-assets-details\", \"value\": \"Physical assets details\", \"pageUrl\": \"/v1/pages/navs/PHYSICAL_ASSETS_DETAILS\" }, { \"id\": \"budget-date-parameter\", \"value\": \"Budget Date Parameter\", \"pageUrl\": \"/v1/pages/navs/BUDGET_DATE_PARAMETER\" } ]}");

          users.add(user);
        });
      });

      if (!users.isEmpty()) {
        usersRepo.saveAll(users);
      }
    }
  }

  public void saveInitialMatcher() {
    if (matcherService.getAll().isEmpty()) {
      List<SecurityMapping> securityMappings = new ManagedList<>();
      //OTHERS AUTHENTICATION
      securityMappings.add(new SecurityMapping(1, "/swagger-resources/**", HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(2, "/swagger-ui.html", HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(3, "/v2/api-docs", HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(4, "/webjars/**", HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(5, "/octopus/**", HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      //API AUTHENTICATION
      securityMappings.add(new SecurityMapping(6, ApiConstants.VERSION_1.concat(ApiConstants.OAUTH), HttpMethod.POST.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(7, JspPageAPIConstants.V1_LOGIN, HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
      securityMappings.add(new SecurityMapping(8, ApiConstants.VERSION_1.concat(ApiConstants.USERS), HttpMethod.GET.name(), MatcherStatus.AUTHORIZED.getValue(), ERole.ROLE_SUPER_ADMIN.getValue()));
      securityMappings.add(new SecurityMapping(9, ApiConstants.VERSION_1.concat(ApiConstants.USERS), HttpMethod.POST.name(), MatcherStatus.AUTHORIZED.getValue(), ERole.ROLE_SUPER_ADMIN.getValue()));
      //JSP PAGES API AUTHENTICATION
//            securityMappings.add(new SecurityMapping(9, JspPageAPIConstants.V1_USER_CREATE, HttpMethod.GET.name(), MatcherStatus.AUTHORIZED.getValue(), ERole.ROLE_SUPER_ADMIN.getValue()));

      final String[] WHITE_LIST_EXTENSIONS = {".css", ".js", ".png", ".woff", ".gif"};
      int index = 10;
      for (String white_list_extension : WHITE_LIST_EXTENSIONS) {
        securityMappings.add(new SecurityMapping(index, "**" + white_list_extension, HttpMethod.GET.name(), MatcherStatus.PERMIT_ALL.getValue(), null));
        index++;
      }

      matcherService.saveAll(securityMappings);
    }
  }
}
