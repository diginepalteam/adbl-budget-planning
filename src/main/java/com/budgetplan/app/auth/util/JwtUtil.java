
package com.budgetplan.app.auth.util;

import com.budgetplan.app.auth.dto.JwtTokenDetails;
import com.budgetplan.app.auth.entity.TokenBlackList;
import com.budgetplan.app.auth.service.CustomUserDetailsService;
import com.budgetplan.app.auth.service.TokenBlackListService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class JwtUtil {
    private static final String AUTHORITIES_KEY = "authorities";
    @Value("${project.config.token.expiry.hour:10}")
    private int tokenExpiryHour;

    @Value("${project.config.client.secret:ADBLBUDGETPLANSECRETKEY}")
    private String secret;

    private CustomUserDetailsService customUserDetailsService;
    private TokenBlackListService tokenBlackListService;

    @Autowired
    public void setCustomUserDetailsService(CustomUserDetailsService customUserDetailsService) {
        this.customUserDetailsService = customUserDetailsService;
    }

    @Autowired
    public void setTokenBlackListService(TokenBlackListService tokenBlackListService) {
        this.tokenBlackListService = tokenBlackListService;
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public JwtTokenDetails generateToken(String username) {
        Map<String, Object> claims = new HashMap<>();

        UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);

        final String authorities = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));
        claims.put(AUTHORITIES_KEY, authorities);
        claims.put("username", userDetails.getUsername());

        return createToken(claims, username);
    }

    private JwtTokenDetails createToken(Map<String, Object> claims, String subject) {
        Date expiration = new Date(System.currentTimeMillis() + 1000 * 60 * 60 * tokenExpiryHour);
        Date issuedAt = new Date(System.currentTimeMillis());

        String token = Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(issuedAt)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS256, secret).compact();

        return new JwtTokenDetails((String) claims.get("username"), token, issuedAt.toString(), expiration.toString(), (String) claims.get(AUTHORITIES_KEY));
    }

    public Optional<TokenBlackList> blackListToken(String token) {
        if (null!=token && !token.isEmpty()) {
            return tokenBlackListService.saveTokenAsBlackListed(token);
        }
        return Optional.empty();
    }

    public Optional<String> getToken(HttpServletRequest request) {
        try {
            String authorizationHeader = request.getHeader("Authorization");
            if (!StringUtils.isEmpty(authorizationHeader) && authorizationHeader.startsWith("Bearer ")) {
                return Optional.of(authorizationHeader.substring(7));
            }
        } catch (Exception e) {
            log.error("getToken(request) exception with message {}", e.getLocalizedMessage());
        }
        return Optional.empty();
    }


    public boolean validate(String token) {
        try {
            String username = this.extractUsername(token);
            UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);
            return !isTokenExpired(token)
                    && !tokenBlackListService.isTokenBlackListed(token)
                    && username.equals(userDetails.getUsername());
        }catch (Exception e){
            return false;
        }
    }

    public Optional<String> getUsername(String token) {
        if (!StringUtils.isEmpty(token)) {
            String userName = this.extractUsername(token);
            if (null != userName) {
                return Optional.of(userName);
            }
        }
        return Optional.empty();
    }

    public Optional<String> getCurrentUserNameFromToken() {
        try {
            HttpServletRequest request =
                    ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                            .getRequest();
            Optional<String> token = this.getToken(request);
            if (token.isPresent()) {
                return this.getUsername(token.get());
            }
        } catch (Exception e) {
            log.warn("getUserNameFromToken() : null or exception getting current username from token");
        }
        return Optional.empty();
    }
}
