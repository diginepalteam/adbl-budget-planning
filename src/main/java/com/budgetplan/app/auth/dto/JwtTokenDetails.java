
package com.budgetplan.app.auth.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({
        "username",
        "token",
        "iat",
        "exp",
        "roles"
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JwtTokenDetails {
    @JsonProperty("status")
    private int status;
    @JsonProperty("message")
    private String message;
    @JsonProperty("username")
    private String username;
    @JsonProperty("token")
    private String authToken;
    @JsonProperty("iat")
    private String issuedAt;
    @JsonProperty("exp")
    private String expirationAt;
    @JsonProperty("roles")
    private String roles;

    public JwtTokenDetails(String username, String authToken, String issuedAt, String expirationAt, String roles) {
        this.username = username;
        this.authToken = authToken;
        this.issuedAt = issuedAt;
        this.expirationAt = expirationAt;
        this.roles = roles;
    }
}
