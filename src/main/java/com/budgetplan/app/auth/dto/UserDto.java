
package com.budgetplan.app.auth.dto;

import com.budgetplan.app.auth.entity.Roles;
import com.budgetplan.app.auth.enums.EUserStatus;
import com.budgetplan.app.staffs.Staffs;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class UserDto {
    private String username;
    private String password;
    private EUserStatus status = EUserStatus.ACTIVE;
    private Set<Roles> roles;
    private Staffs staffs;
    private String navigationJson;
}
