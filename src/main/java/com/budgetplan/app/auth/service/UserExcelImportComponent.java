package com.budgetplan.app.auth.service;

import com.budgetplan.app.auth.entity.User;
import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.staffs.Staffs;
import com.budgetplan.app.staffs.StaffsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class UserExcelImportComponent {
    private UserService userService;
    private StaffsService staffsService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setStaffsService(StaffsService staffsService) {
        this.staffsService = staffsService;
    }

    @Autowired
    public void setbCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public boolean importUser(MultipartFile file) {
        if (null != file) {
            try {
                XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
                XSSFSheet worksheet = workbook.getSheetAt(0);
                int rowsCount = worksheet.getPhysicalNumberOfRows();

                List<User> userList = new ArrayList<>();

                for (int i = 1; i < rowsCount; i++) {
                    XSSFRow row = worksheet.getRow(i);

                    String username = row.getCell(0).getStringCellValue();
                    String password = row.getCell(1).getStringCellValue();
                    String branchCode = row.getCell(2).getStringCellValue();
                    String staffFirstName = row.getCell(3).getStringCellValue();
                    String staffMiddleName = row.getCell(4).getStringCellValue();
                    String staffLastName = row.getCell(5).getStringCellValue();

                    Staffs staffs = new Staffs();
                    staffs.setFirstName(staffFirstName);
                    staffs.setMiddleName(staffMiddleName);
                    staffs.setLastName(staffLastName);

                    MdBranch branch = new MdBranch();
                    branch.setBranchCode(branchCode);
                    staffs.setBranch(branch);

                    Optional<Staffs> staffsOptional = staffsService.save(staffs);
                    if (staffsOptional.isPresent()) {
                        User user = new User();
                        user.setStaffs(staffsOptional.get());
                        user.setUsername(username);
                        user.setPassword(bCryptPasswordEncoder.encode(password));

                        userList.add(user);
                    }
                }
                userService.saveUsersIfEmpty(userList);
                return true;
            } catch (IOException ex) {
                log.warn("User import Excel file exception ", ex);
            }
        }
        return false;
    }
}
