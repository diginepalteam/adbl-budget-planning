
package com.budgetplan.app.auth.service;

import com.budgetplan.app.auth.entity.Roles;
import com.budgetplan.app.auth.entity.User;
import com.budgetplan.app.auth.enums.EUserStatus;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getUsers();

    Optional<User> getUser(String username);

    Optional<User> saveUser(User user);

    List<User> saveUsersIfEmpty(List<User> users);

    Optional<User> updateUser(String username, User user);

    Optional<User> updateUserStatus(String username, EUserStatus status);

    Optional<User> activateUser(String username);

    Optional<User> deActivateUser(String username);

    Optional<User> deleteUser(String username);

    List<Roles> getRoles();

    List<String> getCurrentUserRoles();

    Optional<User> getCurrentUserFromToken();

    String getTokenUserStaffName();

    List<User> getCurrentUserRelatedBranchUsers();

    Optional<String> getTokenUserBranchCode();

    boolean validateUserPassword(String username, String password);

    boolean validateCurrentUserPassword(String password);

    boolean updateCurrentUserPassword(String password);
}
