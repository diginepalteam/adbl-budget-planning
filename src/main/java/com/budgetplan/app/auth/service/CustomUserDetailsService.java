
package com.budgetplan.app.auth.service;

import com.budgetplan.app.auth.entity.User;
import com.budgetplan.app.auth.enums.EUserStatus;
import com.budgetplan.app.auth.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CustomUserDetailsService implements UserDetailsService {
    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        List<EUserStatus> validUserStatus = new ArrayList<>();
        validUserStatus.add(EUserStatus.ACTIVE);
        validUserStatus.add(EUserStatus.VALIDATED);

        Optional<User> userOptional = Optional.ofNullable(userRepository.findByUsernameAndStatusIn(username, validUserStatus)
                .orElseThrow(() -> new UsernameNotFoundException("Username " + username + "not found")));

        if (userOptional.isPresent()) {
            User user = userOptional.get();
            List<GrantedAuthority> authorities = user.getRoles().stream()
                    .map(role -> new SimpleGrantedAuthority(role.getName().name())).collect(Collectors.toList());

            return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
        } else {
            throw new UsernameNotFoundException("Username " + username + "not found");
        }
    }
}
