
package com.budgetplan.app.auth.service;

import com.budgetplan.app.auth.entity.TokenBlackList;

import java.util.Optional;

public interface TokenBlackListService {
    Optional<TokenBlackList> saveTokenAsBlackListed(String token);

    Optional<TokenBlackList> getBlackListToken(String token);

    boolean isTokenBlackListed(String token);

}
