
package com.budgetplan.app.auth.service;

import com.budgetplan.app.auth.entity.SecurityMapping;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import java.util.List;

public interface MatcherService {
    List<SecurityMapping> getAll();
    void setMatcherToHttpSecurity(HttpSecurity http);
    List<SecurityMapping> saveAll(List<SecurityMapping> securityMappings);
}
