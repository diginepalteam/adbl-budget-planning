
package com.budgetplan.app.auth.service;

import com.budgetplan.app.auth.entity.TokenBlackList;
import com.budgetplan.app.auth.repository.TokenBlackListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TokenBlackListServiceImpl implements TokenBlackListService {
    private TokenBlackListRepository tokenBlackListRepository;

    @Autowired
    public void setTokenBlackListRepository(TokenBlackListRepository tokenBlackListRepository) {
        this.tokenBlackListRepository = tokenBlackListRepository;
    }

    @Override
    public Optional<TokenBlackList> saveTokenAsBlackListed(String token) {
        TokenBlackList tokenBlackList = new TokenBlackList(token);
        return Optional.of(tokenBlackListRepository.save(tokenBlackList));
    }

    @Override
    public Optional<TokenBlackList> getBlackListToken(String token) {
        return tokenBlackListRepository.findByToken(token);
    }

    @Override
    public boolean isTokenBlackListed(String token) {
        return getBlackListToken(token).isPresent();
    }

}
