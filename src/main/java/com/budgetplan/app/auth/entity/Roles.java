
package com.budgetplan.app.auth.entity;

import com.budgetplan.app.auth.enums.ERole;
import com.budgetplan.app.commons.Auditable;
import lombok.*;

import javax.persistence.*;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "auth_roles")
public class Roles extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ERole name;
}
