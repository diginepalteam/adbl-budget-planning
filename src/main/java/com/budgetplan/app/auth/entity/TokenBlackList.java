
package com.budgetplan.app.auth.entity;


import com.budgetplan.app.commons.Auditable;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "token_black_list")
public class TokenBlackList extends Auditable<String> {
    @Id
    private String token;
}
