
package com.budgetplan.app.auth.entity;

import com.budgetplan.app.auth.enums.EUserStatus;
import com.budgetplan.app.commons.Auditable;
import com.budgetplan.app.staffs.Staffs;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "auth_user",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "userName")
        })
public class User extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    @Enumerated(EnumType.STRING)
    private EUserStatus status = EUserStatus.ACTIVE;

    @OneToOne(optional = true)
    private Staffs staffs;

    @Column(columnDefinition = "TEXT")
    private String navigationJson;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "auth_user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Roles> roles = new HashSet<>();

    public User(String userName, String password, Set<Roles> rolesSet) {
        this.username = userName;
        this.password = password;
        this.roles = rolesSet;
    }
}
