
package com.budgetplan.app.auth.entity;

import com.budgetplan.app.auth.enums.MatcherStatus;
import com.budgetplan.app.commons.Auditable;
import lombok.*;

import javax.persistence.*;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "security_mapping")
public class SecurityMapping extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String apiPath;
    /**
     * {@link org.springframework.http.HttpMethod}
     */
    private String method;
    /**
     * {@link MatcherStatus}
     */
    private String authorizationStatus;
    /**
     * {@link Roles}
     */
    private String roleValue;
}
