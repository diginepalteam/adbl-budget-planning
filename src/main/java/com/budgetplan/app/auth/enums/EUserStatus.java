
package com.budgetplan.app.auth.enums;

import lombok.Getter;

@Getter
public enum EUserStatus {
    ACTIVE,
    PENDING,
    VALIDATED,
    RESTRICTED,
    VALIDATED_AS_RESTRICTED,
    SUSPENDED,
    DELETED;
}
