
package com.budgetplan.app.auth.enums;

import lombok.Getter;

@Getter
public enum MatcherStatus {
    AUTHORIZED("AUTHORIZED"),
    PERMIT_ALL("PERMIT_ALL");
    private final String value;

    MatcherStatus(String value) {
        this.value = value;
    }
}
