
package com.budgetplan.app.auth.enums;

import lombok.Getter;

@Getter
public enum ERole {
    ROLE_SUPER_ADMIN("SUPER_ADMIN"),
    ROLE_BRANCH_ADMIN("BRANCH_ADMIN");
    private final String value;

    ERole(String value) {
        this.value = value;
    }
}
