package com.budgetplan.app.integration;

import com.budgetplan.app.integration.responses.*;

import java.util.List;

public interface MisIntegrationService {

  List<ModelProductWiseLoan> getLoanDetails();

  List<ModelProductWiseDeposit> getDepositDetails();

  List<ModelProductWiseLoanClassification> getLoanClassifications();

  List<ModelIncomeAndExpenses> getExpenseBudgets();

  List<MisLoanInterestTarget> getLoanInterestTargets();

  List<MisProfitAndLoss> getProfitAndLoss();

  List<MisBalanceSheet> getBalanceSheets();
}
