package com.budgetplan.app.integration;

import com.budgetplan.app.integration.responses.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.budgetplan.app.integration.MisAPI.*;

@Slf4j
@Service
public class MisIntegrationServiceImpl implements MisIntegrationService {
  protected static final String MIS_API_CALL_INITIATED = "MIS API {} call initiated";
  protected static final String MIS_API_CALL_COMPLETED = "MIS API {} call completed";
  protected static final String MIS_EXCEPTION_FOUND = "Exception found";

  @Autowired
  private WebClient webClient;

  @Override
  public List<ModelProductWiseLoan> getLoanDetails() {
    log.info(MIS_API_CALL_INITIATED, MIS_LOAN_DETAILS);
    try {
      ModelProductWiseLoan[] modelProductWiseLoans = this.webClient.get().uri(MIS_LOAN_DETAILS)
        .retrieve().bodyToMono(ModelProductWiseLoan[].class).block();
      log.info(MIS_API_CALL_COMPLETED, MIS_LOAN_DETAILS);
      assert modelProductWiseLoans != null;
      return Arrays.asList(modelProductWiseLoans);
    } catch (Exception e) {
      log.error(MIS_EXCEPTION_FOUND, e);
    }
    return new ArrayList<>();
  }

  @Override
  public List<ModelProductWiseDeposit> getDepositDetails() {
    log.info(MIS_API_CALL_INITIATED, MIS_DEPOSIT_DETAILS);
    try {
      ModelProductWiseDeposit[] modelProductWiseDeposit = this.webClient.get().uri(MIS_DEPOSIT_DETAILS)
        .retrieve().bodyToMono(ModelProductWiseDeposit[].class).block();
      log.info(MIS_API_CALL_COMPLETED, MIS_DEPOSIT_DETAILS);

      assert modelProductWiseDeposit != null;
      return Arrays.asList(modelProductWiseDeposit);
    } catch (Exception e) {
      log.error(MIS_EXCEPTION_FOUND, e);

    }
    return new ArrayList<>();
  }

  @Override
  public List<ModelProductWiseLoanClassification> getLoanClassifications() {
    log.info(MIS_API_CALL_INITIATED, MIS_LOAN_CLASSIFICATION);
    try {
      ModelProductWiseLoanClassification[] modelProductWiseLoanClassifications = this.webClient.get().uri(MIS_LOAN_CLASSIFICATION)
        .retrieve().bodyToMono(ModelProductWiseLoanClassification[].class).block();

      log.info(MIS_API_CALL_COMPLETED, MIS_LOAN_CLASSIFICATION);

      assert modelProductWiseLoanClassifications != null;
      return Arrays.asList(modelProductWiseLoanClassifications);
    } catch (Exception e) {
      log.error(MIS_EXCEPTION_FOUND, e);
    }
    return new ArrayList<>();
  }

  @Override
  public List<ModelIncomeAndExpenses> getExpenseBudgets() {
    log.info(MIS_API_CALL_INITIATED, MIS_EXPENSE_HEAD);
    try {
      ModelIncomeAndExpenses[] modelIncomeAndExpenses = this.webClient.get().uri(MIS_EXPENSE_HEAD)
        .retrieve().bodyToMono(ModelIncomeAndExpenses[].class).block();

      log.info(MIS_API_CALL_COMPLETED, MIS_EXPENSE_HEAD);

      assert modelIncomeAndExpenses != null;
      return Arrays.asList(modelIncomeAndExpenses);
    } catch (Exception e) {
      log.error(MIS_EXCEPTION_FOUND, e);
    }
    return new ArrayList<>();
  }

  @Override
  public List<MisLoanInterestTarget> getLoanInterestTargets() {
    log.info(MIS_API_CALL_INITIATED, MIS_LOAN_INTEREST_INCOME);
    try {
      MisLoanInterestTarget[] misLoanInterestTargets = this.webClient.get().uri(MIS_LOAN_INTEREST_INCOME)
        .retrieve().bodyToMono(MisLoanInterestTarget[].class).block();
      log.info(MIS_API_CALL_COMPLETED, MIS_LOAN_INTEREST_INCOME);
      assert misLoanInterestTargets != null;
      return Arrays.asList(misLoanInterestTargets);
    } catch (Exception e) {
      log.error(MIS_EXCEPTION_FOUND, e);
    }
    return new ArrayList<>();
  }

  @Override
  public List<MisProfitAndLoss> getProfitAndLoss() {
    log.info(MIS_API_CALL_INITIATED, MIS_PROFIT_AND_LOSS);
    try {
      MisProfitAndLoss[] misProfitAndLosses = this.webClient.get().uri(MIS_PROFIT_AND_LOSS)
        .retrieve().bodyToMono(MisProfitAndLoss[].class).block();
      log.info(MIS_API_CALL_COMPLETED, MIS_PROFIT_AND_LOSS);
      assert misProfitAndLosses != null;
      return Arrays.asList(misProfitAndLosses);
    } catch (Exception e) {
      log.error(MIS_EXCEPTION_FOUND, e);
    }
    return new ArrayList<>();
  }

  @Override
  public List<MisBalanceSheet> getBalanceSheets() {
    log.info(MIS_API_CALL_INITIATED, MIS_BALANCE_SHEET);
    try {
      MisBalanceSheet[] misBalanceSheets = this.webClient.get().uri(MIS_BALANCE_SHEET)
        .retrieve().bodyToMono(MisBalanceSheet[].class).block();
      log.info(MIS_API_CALL_COMPLETED, MIS_BALANCE_SHEET);
      assert misBalanceSheets != null;
      return Arrays.asList(misBalanceSheets);
    } catch (Exception e) {
      log.error(MIS_EXCEPTION_FOUND, e);
    }
    return new ArrayList<>();
  }
}
