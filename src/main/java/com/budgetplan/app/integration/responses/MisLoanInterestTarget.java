package com.budgetplan.app.integration.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MisLoanInterestTarget {
  private String branch_branchcode;
  private int incomeid;
  private String incomehead;
}
