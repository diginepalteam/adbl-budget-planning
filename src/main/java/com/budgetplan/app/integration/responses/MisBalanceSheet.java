package com.budgetplan.app.integration.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MisBalanceSheet {
  private String branchcode;
  private int headid;
  private String balancehead;
  private int balanceamount;
  private int balanceamountlastyr;
}
