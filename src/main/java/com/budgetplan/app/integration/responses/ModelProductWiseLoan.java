package com.budgetplan.app.integration.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModelProductWiseLoan {
    private String branch_branchcode;
    private String loan_type_loanproductcode;
    private String workingcapitaloutstandingcurrent;
    private String termloanoutcur;
    private String totalloanoutcur;
    private String pastdueloanoutcur;
    private String nonperformingloanoutcur;
    private String nonperforminginterestoutcur;
}
