package com.budgetplan.app.integration.responses;

import lombok.Data;

@Data
public class ModelIncomeAndExpenses {
  private int maincode;
  private String branch_branchcode;
  private String lineno;
  private int amount;
  private int amountLastYr;
}
