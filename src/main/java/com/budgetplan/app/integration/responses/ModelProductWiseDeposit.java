package com.budgetplan.app.integration.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModelProductWiseDeposit {
    private String branch_branchcode;
    private String deposit_maintype;
    private String deposit_code;
    private int deposit_amount;
    private double interestrate;
    private String category;
}
