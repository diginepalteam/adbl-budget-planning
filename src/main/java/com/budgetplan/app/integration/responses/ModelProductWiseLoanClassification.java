package com.budgetplan.app.integration.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModelProductWiseLoanClassification {
    private String branch_branchcode;
    private String loan_type_loanproductcode;
    private int passloan;
    private int watchlist;
    private int substandard;
    private int doubtful;
    private int loss;
}
