package com.budgetplan.app.integration;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.txn.entity.ProfitAndLoss;
import com.budgetplan.app.txn.entity.TxnBalanceSheet;
import com.budgetplan.app.txn.service.ProfitAndLossService;
import com.budgetplan.app.txn.service.TxnBalanceSheetService;
import com.budgetplan.app.txn.service.TxnLoanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("core/api/v1")
public class MisIntegrationController {
  private TxnLoanService txnLoanService;
  private ProfitAndLossService profitAndLossService;
  private TxnBalanceSheetService txnBalanceSheetService;

  @Autowired
  public void setTxnLoanService(TxnLoanService txnLoanService) {
    this.txnLoanService = txnLoanService;
  }

  @Autowired
  public void setProfitAndLossService(ProfitAndLossService profitAndLossService) {
    this.profitAndLossService = profitAndLossService;
  }

  @Autowired
  public void setTxnBalanceSheetService(TxnBalanceSheetService txnBalanceSheetService) {
    this.txnBalanceSheetService = txnBalanceSheetService;
  }

  /**
   * TxnLoanOverDueCollectionTarget, TxnLoanDueCollectionTarget, TxnLoanGrowthTarget
   */
  @GetMapping("post/loan/sync")
  public ResponseEntity<?> createPostLoanEvent() {
    log.info("Post loan event triggered for TxnLoanOverDueCollectionTarget, TxnLoanDueCollectionTarget, TxnLoanGrowthTarget");
    txnLoanService.postLoanEvent();
    return new ApiResponse().getSuccessResponse(HttpStatus.OK.getReasonPhrase(), null);
  }

  @GetMapping("mis/profitandloss/sync")
  public ResponseEntity<?> syncProfitAndLossFromMis() {
    List<ProfitAndLoss> profitAndLossList = profitAndLossService.saveProfitAndLossFromMis();
    if (!profitAndLossList.isEmpty()) {
      return new ApiResponse().getSuccessResponse(HttpStatus.OK.getReasonPhrase(), profitAndLossList);
    } else {
      return new ApiResponse().getFailureResponse(HttpStatus.BAD_REQUEST.getReasonPhrase());
    }
  }

  @GetMapping("mis/balancesheet/sync")
  public ResponseEntity<?> syncBalanceSheetFromMis() {
    List<TxnBalanceSheet> balanceSheets = txnBalanceSheetService.syncWithMis();
    if (!balanceSheets.isEmpty()) {
      return new ApiResponse().getSuccessResponse(HttpStatus.OK.getReasonPhrase(), balanceSheets);
    } else {
      return new ApiResponse().getFailureResponse(HttpStatus.BAD_REQUEST.getReasonPhrase());
    }
  }


}
