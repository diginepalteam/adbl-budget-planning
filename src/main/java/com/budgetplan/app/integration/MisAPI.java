package com.budgetplan.app.integration;

public class MisAPI {
  protected MisAPI() {
  }

  protected static final String MIS_LOAN_DETAILS = "/mis/plannings/loanbal";
  protected static final String MIS_DEPOSIT_DETAILS = "/mis/plannings/depositbal";
  protected static final String MIS_LOAN_CLASSIFICATION = "/mis/plannings/loanclassification";
  protected static final String MIS_EXPENSE_HEAD = "/mis/plannings/expensehead";
  protected static final String MIS_LOAN_INTEREST_INCOME = "/mis/plannings/getloaninterestincome";
  protected static final String MIS_PROFIT_AND_LOSS = "/mis/plannings/getprofitandloss";
  protected static final String MIS_BALANCE_SHEET = "/mis/plannings/getbalancesheet";

}
