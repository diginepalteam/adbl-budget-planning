
package com.budgetplan.app.passwords;

import com.budgetplan.app.apiresponse.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("impl/api/v1")
public class PasswordController {
    private PasswordService passwordService;

    @Autowired
    public void setPasswordService(PasswordService passwordService) {
        this.passwordService = passwordService;
    }

    @PutMapping("passwords")
    public ResponseEntity<?> changeCurrentUserPassword(@ModelAttribute ChangePasswordDto changePasswordDto) {
        if (!passwordService.matchPasswords(changePasswordDto.getNewPassword(), changePasswordDto.getNewPasswordRepeat())) {
            return new ApiResponse().getFailureResponse("Password mismatch.");
        } else if (!passwordService.verifyCurrentUserOldPassword(changePasswordDto.getOldPassword())) {
            return new ApiResponse().getFailureResponse("Invalid old password.");
        }
        boolean updateStatus = passwordService.updateCurrentUserPassword(changePasswordDto.getNewPassword());
        if (updateStatus) {
            return new ApiResponse().getSuccessResponse("Password updated.", null);
        } else {
            return new ApiResponse().getFailureResponse("Password update failure.");
        }
    }
}
