
package com.budgetplan.app.passwords;

public interface PasswordService {
    boolean matchPasswords(String password, String confirmPassword);

    boolean verifyCurrentUserOldPassword(String oldPassword);

    boolean updateCurrentUserPassword(String password);
}
