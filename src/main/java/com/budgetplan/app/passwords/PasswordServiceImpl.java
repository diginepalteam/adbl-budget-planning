
package com.budgetplan.app.passwords;

import com.budgetplan.app.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PasswordServiceImpl implements PasswordService {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean matchPasswords(String newPassword, String newPasswordRepeat) {
        return newPassword.equals(newPasswordRepeat);
    }

    @Override
    public boolean verifyCurrentUserOldPassword(String oldPassword) {
        return userService.validateCurrentUserPassword(oldPassword);
    }

    @Override
    public boolean updateCurrentUserPassword(String newPassword) {
        return userService.updateCurrentUserPassword(newPassword);
    }
}
