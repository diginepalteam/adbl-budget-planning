
package com.budgetplan.app.utils;

import org.springframework.util.StringUtils;

public class NameUtility {
    public static String getName(String firstName, String middleName, String lastName){
        if(!StringUtils.isEmpty(firstName)
                && !StringUtils.isEmpty(middleName)
                && !StringUtils.isEmpty(lastName)){
            return firstName + " " + middleName + " " + lastName;
        }else if(!StringUtils.isEmpty(firstName)
                && !StringUtils.isEmpty(lastName)){
            return firstName + " " + lastName;
        }else{
            return firstName;
        }
    }
}
