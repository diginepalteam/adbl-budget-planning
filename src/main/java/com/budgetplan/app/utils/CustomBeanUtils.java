
package com.budgetplan.app.utils;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class CustomBeanUtils {
    public CustomBeanUtils() {
    }

    public static void copyProperties(Object source, Object target) {
        BeanUtils.copyProperties(source, target);
    }

    public static void copyNonNullProperties(Object source, Object target) {
        BeanUtils.copyProperties(source, target, getNullPropertyNames(source));
    }

    public static void copyNonNullAndNonZeroProperties(Object source, Object target) {
        BeanUtils.copyProperties(source, target, getNullAndZeroValuePropertyNames(source));
    }

    public static String[] getNullPropertyNames(Object source) {
        BeanWrapper src = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();
        Set<String> emptyNames = new HashSet();
        PropertyDescriptor[] var4 = pds;
        int var5 = pds.length;

        for (int var6 = 0; var6 < var5; ++var6) {
            PropertyDescriptor pd = var4[var6];
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            } else if (srcValue instanceof Collection) {
                Collection<?> list = (Collection) srcValue;
                if (list.isEmpty()) {
                    emptyNames.add(pd.getName());
                }
            }
        }

        return (String[]) emptyNames.toArray(new String[emptyNames.size()]);
    }

    public static String[] getNullAndZeroValuePropertyNames(Object source) {
        BeanWrapper src = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();
        Set<String> emptyNames = new HashSet();
        PropertyDescriptor[] var4 = pds;
        int var5 = pds.length;

        for (int var6 = 0; var6 < var5; ++var6) {
            PropertyDescriptor pd = var4[var6];
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            } else if (isNumber(srcValue) && 0 == (int) srcValue) {
                emptyNames.add(pd.getName());
            }else if(isDouble(srcValue) && 0 == (double) srcValue){
                emptyNames.add(pd.getName());
            } else if (srcValue instanceof Collection) {
                Collection<?> list = (Collection) srcValue;
                if (list.isEmpty()) {
                    emptyNames.add(pd.getName());
                }
            }
        }

        return (String[]) emptyNames.toArray(new String[emptyNames.size()]);
    }

    private static boolean isNumber(Object src) {
        try {
            int value = (int) src;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean isDouble(Object src) {
        try {
            double value = (double) src;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
