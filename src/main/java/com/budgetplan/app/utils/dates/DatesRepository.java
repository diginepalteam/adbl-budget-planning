
package com.budgetplan.app.utils.dates;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DatesRepository extends JpaRepository<Dates, Integer> {
    Dates findByEnglishdate(String date);

    Dates findByNepalidate(String date);

}
