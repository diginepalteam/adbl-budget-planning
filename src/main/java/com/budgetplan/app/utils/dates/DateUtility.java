
package com.budgetplan.app.utils.dates;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Component
public class DateUtility extends DateUtils {
    private DatesRepository datesRepository;

    @Autowired
    public void setDatesRepository(DatesRepository datesRepository) {
        this.datesRepository = datesRepository;
    }

    public String todayEnglishDate() {
        return dateToString(new Date());
    }

    public String todayNepaliDate() {
        try {
            String currentDate = this.dateToString(new Date());
            return datesRepository.findByEnglishdate(currentDate).getNepalidate();
        }catch (Exception e){
            log.warn("Today's nepali date conversion exception", e);
        }
        return StringUtils.EMPTY;
    }

    public String nepaliToEnglish(String nepaliDate) {
        Dates dates = datesRepository.findByNepalidate(nepaliDate);
        if (null != dates) {
            return dates.getEnglishdate();
        } else {
            return StringUtils.EMPTY;
        }
    }

    public boolean validateDateFormat(String date, String dateFormatFor) {
        date = date.trim();
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date parsedDate = simpleDateFormat.parse(date);
            if (date.equals(simpleDateFormat.format(parsedDate))) {
                return true;
            }
        } catch (Exception e) {
            if(dateFormatFor.isEmpty()){
                throw new RuntimeException("Invalid Date Format");
            }
            throw new RuntimeException("Invalid Date Format");
        }
        return false;
    }

    public boolean compareStartDateLessThanEndDate(String startDate, String endDate) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date start = simpleDateFormat.parse(startDate);
            Date end = simpleDateFormat.parse(endDate);
            if (start.compareTo(end) <= 0) {
                return true;
            }
        } catch (Exception e) {
            log.error("Invalid date format startDate {} endDate {}", startDate, endDate, e);
        }
        return false;
    }

    public String dateToString(Date date) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return simpleDateFormat.format(date);
        } catch (Exception e) {
            log.error("Invalid date format date {}", date, e);
        }
        return "0000-00-00";
    }

    public String getYearFromDate(String date) {
        if (StringUtils.isNotBlank(date) && date.contains("-")) {
            String[] split = date.split("-");
            if (split.length == 3) {
                return split[0];
            }
        }
        return StringUtils.EMPTY;
    }

    public static String getCurrentDate(){
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        Date dateObj = new Date();
        return df.format(dateObj);
    }
}
