
package com.budgetplan.app.utils.dates;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "md_dates")
@Getter
@Setter
public class Dates {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String englishdate;
    private String nepalidate;
}
