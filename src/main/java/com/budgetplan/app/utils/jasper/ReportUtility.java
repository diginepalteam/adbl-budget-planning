
package com.budgetplan.app.utils.jasper;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class ReportUtility {
    private DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private JasperPrint fillReport(JasperRequestDto request) {
        try {
            File file = ResourceUtils.getFile(request.getJrxmlFileLocation());
            JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
            if (null != request.getJrDataSource()) {
                return JasperFillManager.fillReport(jasperReport, request.getParameters(), request.getJrDataSource());
            } else if (null != request.getConnection()) {
                return JasperFillManager.fillReport(jasperReport, request.getParameters(), request.getConnection());
            } else {
                log.error("fillReport() : No connection found");
                JasperFillManager.fillReport(jasperReport, null, new JREmptyDataSource());
            }
        } catch (Exception e) {
            log.error("Class ReportUtility, method exportPdf, message export to pdf exception {}", e.getLocalizedMessage());
        }
        return null;
    }

  public void exportDownloadableReport(HttpServletResponse response, JasperRequestDto request) throws IOException, JRException {
    response.setContentType("application/x-download");
    response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + request.getOutputFileNameWithExtension() + "\""));

    OutputStream out = response.getOutputStream();

    JasperPrint jasperPrint = this.fillReport(request);

    switch (request.getOutputFileType()) {
      case PDF:
        JasperExportManager.exportReportToPdfStream(jasperPrint, out);
        break;
      case EXCEL:
        JasperExportManager.exportReportToXmlStream(jasperPrint, out);
        break;
      default:
    }
  }

    public void exportBulkDownloadableReport(HttpServletResponse response, List<JasperRequestDto> requests, String outputFileName, EFileType outputFileType) throws JRException, IOException {
        response.setContentType("application/x-download");
        response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + outputFileName.concat(outputFileType.getExtension()) + "\""));

        List<JasperPrint> jpList = new ArrayList<>();
        requests.forEach(requestDto -> {
            jpList.add(fillReport(requestDto));
        });

        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput(SimpleExporterInput.getInstance(jpList));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));

        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        configuration.setCreatingBatchModeBookmarks(true); //add this so your bookmarks work, you may set other parameters
        exporter.setConfiguration(configuration);

        exporter.exportReport();

    }

    public JasperRequestDto generateJasperRequest(List<Object> dsDataList, Object request, String outputFileName, String jrxmlLocation, Map<String, Object> params) {
        JasperRequestDto jasperRequest = new JasperRequestDto();

        if (null != dsDataList && null != request) {
            JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(dsDataList);
            jasperRequest.setJrDataSource(ds);

            params.put("request", request);
            params.put("ds", ds);
        } else {
            try {
                jasperRequest.setConnection(dataSource.getConnection());
            } catch (SQLException sqlException) {
                log.error("generateJasperRequest() : datasource connection not found", sqlException);
            }
        }

        if (null != params) {
            jasperRequest.setParameters(params);
        }

        jasperRequest.setOutputFileName(outputFileName);
        jasperRequest.setJrxmlFileLocation(jrxmlLocation);

        return jasperRequest;
    }
}
