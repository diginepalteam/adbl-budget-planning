
package com.budgetplan.app.utils.jasper;

import lombok.Getter;

@Getter
public enum EFileType {
    PDF(".pdf"),
    EXCEL(".xls"),
    CSV("csv"),
    HTML(".html");
    public final String extension;

    EFileType(String extension) {
        this.extension = extension;
    }
}
