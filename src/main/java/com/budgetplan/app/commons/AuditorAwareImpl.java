
package com.budgetplan.app.commons;

import com.budgetplan.app.auth.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class AuditorAwareImpl implements AuditorAware<String> {
    private JwtUtil jwtUtil;

    @Autowired
    public void setJwtUtil(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Override
    public Optional<String> getCurrentAuditor() {
        try {
            Optional<String> userOptional = jwtUtil.getCurrentUserNameFromToken();
            if (userOptional.isPresent()) {
                return userOptional;
            } else {
                return Optional.of("system");
            }
        }catch (Exception e){
            return Optional.of("system");
        }
    }


}
