
package com.budgetplan.app.commons;

import com.budgetplan.app.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Getter
@Setter
@MappedSuperclass
public class PersonalInformation extends Auditable<String> implements Serializable {
    private static final long serialVersionUID = 1L;

    private String firstName;

    private String middleName;

    private String lastName;

    private String address;

    private String phone;

    private String email;

    @Enumerated(EnumType.STRING)
    private Status status =  Status.ACTIVE;
}
