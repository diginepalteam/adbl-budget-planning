package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnLoanDueCollectionTarget;

import java.util.List;

public interface TxnDueLoanCollectionTargetService {
    List<TxnLoanDueCollectionTarget> getByBranchCode(String branchCode);
}
