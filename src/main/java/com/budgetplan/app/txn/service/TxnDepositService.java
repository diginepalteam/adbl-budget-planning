package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnDeposit;

import java.util.List;

public interface TxnDepositService {
    List<TxnDeposit> getByBranchCode(String branchCode);
}
