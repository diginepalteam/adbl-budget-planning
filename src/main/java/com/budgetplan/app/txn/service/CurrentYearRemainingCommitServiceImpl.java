package com.budgetplan.app.txn.service;

import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.CurrentYearRemainingCommit;
import com.budgetplan.app.txn.repository.CurrentYearRemainingCommitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CurrentYearRemainingCommitServiceImpl implements CurrentYearRemainingCommitService, GenericCrudService<CurrentYearRemainingCommit, Integer> {
  private CurrentYearRemainingCommitRepository currentYearRemainingCommitRepository;

  @Autowired
  public void setCurrentYearRemainingCommitRepository(CurrentYearRemainingCommitRepository currentYearRemainingCommitRepository) {
    this.currentYearRemainingCommitRepository = currentYearRemainingCommitRepository;
  }

  @Override
  public Optional<CurrentYearRemainingCommit> save(CurrentYearRemainingCommit entity) {
    return Optional.of(currentYearRemainingCommitRepository.save(entity));
  }

  @Override
  public List<CurrentYearRemainingCommit> saveAll(List<CurrentYearRemainingCommit> entities) {
    return currentYearRemainingCommitRepository.saveAll(entities);
  }

  //todo
  @Override
  public Optional<CurrentYearRemainingCommit> update(CurrentYearRemainingCommit entity, Integer id) {
    return Optional.empty();
  }

  @Override
  public List<CurrentYearRemainingCommit> getAll() {
    return currentYearRemainingCommitRepository.findAll();
  }

  @Override
  public Optional<CurrentYearRemainingCommit> getOne(Integer id) {
    return Optional.of(currentYearRemainingCommitRepository.getOne(id));
  }

  @Override
  public List<CurrentYearRemainingCommit> getByBranchCode(String branchCode) {
    return currentYearRemainingCommitRepository.findAllByBranchBranchCode(branchCode);
  }
}
