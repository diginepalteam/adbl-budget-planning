package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnLoanDisbursementTarget;

import java.util.List;

public interface TxnLoanDisbursementTargetService {
    List<TxnLoanDisbursementTarget> getByBranchCode(String branchCode);
}
