package com.budgetplan.app.txn.service;

import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnLoanClassification;
import com.budgetplan.app.txn.repository.TxnLoanClassificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TxnLoanClassificationServiceImpl implements GenericCrudService<TxnLoanClassification, Integer>, TxnLoanClassificationService {
    private TxnLoanClassificationRepository txnLoanClassificationRepository;

    @Autowired
    public void setTxnLoanClassificationRepository(TxnLoanClassificationRepository txnLoanClassificationRepository) {
        this.txnLoanClassificationRepository = txnLoanClassificationRepository;
    }


    @Override
    public List<TxnLoanClassification> getByBranchCode(String branchCode) {
        return txnLoanClassificationRepository.findAllByBranchBranchCode(branchCode);
    }

    @Override
    public Optional<TxnLoanClassification> save(TxnLoanClassification entity) {
        return Optional.of(txnLoanClassificationRepository.save(entity));
    }

    @Override
    public List<TxnLoanClassification> saveAll(List<TxnLoanClassification> entities) {
        return txnLoanClassificationRepository.saveAll(entities);
    }

    /**
     * <p>
     *     This method only updates watchlistthisprojected, watchlistnextprojected for {@link TxnLoanClassification}
     * </p>
     * @param entity
     * @param id
     * @return
     */
    @Override
    public Optional<TxnLoanClassification> update(TxnLoanClassification entity, Integer id) {
        Optional<TxnLoanClassification> txnLoanClassificationOptional = this.getOne(id);
        if(txnLoanClassificationOptional.isPresent()){
            TxnLoanClassification existing = txnLoanClassificationOptional.get();
            existing.setWatchlistthisprojected(entity.getWatchlistthisprojected());
            existing.setWatchlistnextprojected(entity.getWatchlistnextprojected());
            return this.save(existing);
        }
        return Optional.empty();
    }

    @Override
    public List<TxnLoanClassification> getAll() {
        return txnLoanClassificationRepository.findAll();
    }

    @Override
    public Optional<TxnLoanClassification> getOne(Integer id) {
        return txnLoanClassificationRepository.findById(id);
    }
}
