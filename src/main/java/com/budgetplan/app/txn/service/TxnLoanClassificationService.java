package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnLoanClassification;

import java.util.List;

public interface TxnLoanClassificationService {
    List<TxnLoanClassification> getByBranchCode(String branchCode);
}
