package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnLoanDisbursementTarget;
import com.budgetplan.app.txn.repository.TxnLoanDisbursementTargetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TxnLoanDisbursementTargetServiceImpl implements TxnLoanDisbursementTargetService{
    private TxnLoanDisbursementTargetRepository txnLoanDisbursementTargetRepository;

    @Autowired
    public void setTxnLoanDisbursementTargetRepository(TxnLoanDisbursementTargetRepository txnLoanDisbursementTargetRepository) {
        this.txnLoanDisbursementTargetRepository = txnLoanDisbursementTargetRepository;
    }

    @Override
    public List<TxnLoanDisbursementTarget> getByBranchCode(String branchCode) {
        return txnLoanDisbursementTargetRepository.findAllByBranchBranchCode(branchCode);
    }
}
