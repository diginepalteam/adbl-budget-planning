package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnExpenseBudget;

import java.util.List;

public interface TxnExpenseBudgetService {
    List<TxnExpenseBudget> getByBranchCode(String branchCode);

    List<TxnExpenseBudget> setInitialEntry(String branchCode);
}
