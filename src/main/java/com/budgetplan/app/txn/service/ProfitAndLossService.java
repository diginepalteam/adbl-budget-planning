package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.ProfitAndLoss;

import java.util.List;

public interface ProfitAndLossService {
  List<ProfitAndLoss> saveProfitAndLossFromMis();
  List<ProfitAndLoss> getByBranchCode(String branchCode);
}
