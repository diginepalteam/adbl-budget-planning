package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnLoanDueCollectionTarget;
import com.budgetplan.app.txn.repository.TxnDueLoanCollectionTargetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TxnDueLoanCollectionTargetServiceImpl implements TxnDueLoanCollectionTargetService {
    private TxnDueLoanCollectionTargetRepository txnDueLoanCollectionTargetRepository;

    @Autowired
    public void setTxnDueLoanCollectionTargetRepository(TxnDueLoanCollectionTargetRepository txnDueLoanCollectionTargetRepository) {
        this.txnDueLoanCollectionTargetRepository = txnDueLoanCollectionTargetRepository;
    }

    @Override
    public List<TxnLoanDueCollectionTarget> getByBranchCode(String branchCode) {
        return txnDueLoanCollectionTargetRepository.findAllByBranchBranchCode(branchCode);
    }
}
