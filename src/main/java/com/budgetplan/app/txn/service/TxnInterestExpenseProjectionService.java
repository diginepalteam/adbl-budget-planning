package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnInterestExpenseProjection;

import java.util.List;

public interface TxnInterestExpenseProjectionService {
    List<TxnInterestExpenseProjection> findByBranchCode(String branchCode);
}
