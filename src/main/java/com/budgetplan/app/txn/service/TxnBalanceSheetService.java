package com.budgetplan.app.txn.service;

import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnBalanceSheet;

import java.util.List;

public interface TxnBalanceSheetService extends GenericCrudService<TxnBalanceSheet, Integer> {
  List<TxnBalanceSheet> syncWithMis();

  List<TxnBalanceSheet> getByBranchCode(String branchCode);

}
