package com.budgetplan.app.txn.service;

import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.*;
import com.budgetplan.app.txn.repository.*;
import com.budgetplan.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;

@Service
public class TxnLoanServiceImpl implements GenericCrudService<TxnLoan, Integer>, TxnLoanService {
  private static final DecimalFormat df = new DecimalFormat("0.00");
  private TxnLoanRepository txnLoanRepository;
  private TxnLoanOverDueCollectionTargetRepository txnLoanOverDueCollectionTargetRepository;
  private TxnDueLoanCollectionTargetRepository txnDueLoanCollectionTargetRepository;
  private TxnLoanGrowthTargetRepository txnLoanGrowthTargetRepository;
  private TxnLoanDisbursementTargetRepository txnLoanDisbursementTargetRepository;


  @Autowired
  public void setTxnLoanRepository(TxnLoanRepository txnLoanRepository) {
    this.txnLoanRepository = txnLoanRepository;
  }

  @Autowired
  public void setTxnLoanOverDueCollectionTargetRepository(TxnLoanOverDueCollectionTargetRepository txnLoanOverDueCollectionTargetRepository) {
    this.txnLoanOverDueCollectionTargetRepository = txnLoanOverDueCollectionTargetRepository;
  }

  @Autowired
  public void setTxnDueLoanCollectionTargetRepository(TxnDueLoanCollectionTargetRepository txnDueLoanCollectionTargetRepository) {
    this.txnDueLoanCollectionTargetRepository = txnDueLoanCollectionTargetRepository;
  }

  @Autowired
  public void setTxnLoanGrowthTargetRepository(TxnLoanGrowthTargetRepository txnLoanGrowthTargetRepository) {
    this.txnLoanGrowthTargetRepository = txnLoanGrowthTargetRepository;
  }

  @Autowired
  public void setTxnLoanDisbursementTargetRepository(TxnLoanDisbursementTargetRepository txnLoanDisbursementTargetRepository) {
    this.txnLoanDisbursementTargetRepository = txnLoanDisbursementTargetRepository;
  }

  @Override
  public Optional<TxnLoan> save(TxnLoan entity) {
    return Optional.of(txnLoanRepository.save(entity));
  }

  @Override
  public List<TxnLoan> saveAll(List<TxnLoan> entities) {
    return txnLoanRepository.saveAll(entities);
  }

  @Override
  public Optional<TxnLoan> update(TxnLoan entity, Integer id) {
    Optional<TxnLoan> txnLoanOptional = this.getOne(id);
    if (txnLoanOptional.isPresent()) {
      TxnLoan existingTxnLoan = txnLoanOptional.get();
      CustomBeanUtils.copyNonNullAndNonZeroProperties(entity, existingTxnLoan);
      return Optional.of(txnLoanRepository.save(existingTxnLoan));
    }
    return Optional.empty();
  }

  @Override
  public List<TxnLoan> getAll() {
    return txnLoanRepository.findAll();
  }

  @Override
  public Optional<TxnLoan> getOne(Integer id) {
    return txnLoanRepository.findById(id);
  }

  @Override
  public List<TxnLoan> getByBranchCode(String branchCode) {
    List<TxnLoan> txnLoans = txnLoanRepository.findAllByBranchBranchCode(branchCode);

    int total = txnLoans.stream().mapToInt(TxnLoan::getTotalLoanOutEstimated).sum();
    txnLoans.forEach(txnLoan ->
      txnLoan.setOutStandingWeight(
        df.format((double) (txnLoan.getTotalLoanOutEstimated() * 100) / total)
      )
    );

    return txnLoans;
  }

  /**
   * TxnLoanOverDueCollectionTarget, TxnLoanDueCollectionTarget, TxnLoanGrowthTarget
   */
  @Override
  public void postLoanEvent() {
    txnLoanRepository.findAll().forEach(txnLoan -> {
      this.loadLoanDependentTables(txnLoan);
    });
  }

  private void loadLoanDependentTables(TxnLoan txnLoan) {
    TxnLoanOverDueCollectionTarget txnLoanOverDueCollectionTarget = this.getTxnLoanOverdueCollectionTarget(txnLoan);
    txnLoanOverDueCollectionTargetRepository.save(txnLoanOverDueCollectionTarget);

    TxnLoanDueCollectionTarget txnLoanDueCollectionTarget = this.getTxnLoanDueCollectionTarget(txnLoan, txnLoanOverDueCollectionTarget);
    txnDueLoanCollectionTargetRepository.save(txnLoanDueCollectionTarget);

    TxnLoanDisbursementTarget txnLoanDisbursementTarget = new TxnLoanDisbursementTarget();
    Optional<TxnLoanDisbursementTarget> txnLoanDisbursementTargetOptional = txnLoanDisbursementTargetRepository.findFirstByBranchBranchCodeAndLoanTypeLoanProductCode(txnLoan.getBranch().getBranchCode(), txnLoan.getLoanType().getLoanProductCode());
    if (txnLoanDisbursementTargetOptional.isPresent()) {
      txnLoanDisbursementTarget = txnLoanDisbursementTargetOptional.get();
    }

    TxnLoanGrowthTarget txnLoanGrowthTarget = this.getTxnLoanGrowthTarget(txnLoan, txnLoanOverDueCollectionTarget, txnLoanDueCollectionTarget, txnLoanDisbursementTarget);
    txnLoanGrowthTargetRepository.save(txnLoanGrowthTarget);
  }

  private TxnLoanGrowthTarget getTxnLoanGrowthTarget(TxnLoan txnLoan, TxnLoanOverDueCollectionTarget txnLoanOverDueCollectionTarget, TxnLoanDueCollectionTarget txnLoanDueCollectionTarget, TxnLoanDisbursementTarget txnLoanDisbursementTarget) {
    TxnLoanGrowthTarget txnLoanGrowthTarget = new TxnLoanGrowthTarget();

    txnLoanGrowthTarget.setBranch(txnLoan.getBranch());
    txnLoanGrowthTarget.setLoanType(txnLoan.getLoanType());

    txnLoanGrowthTarget.setFirstqtrterm(
      txnLoanDisbursementTarget.getFirstqtrshort() - txnLoanDueCollectionTarget.getFirstqtrshort() - txnLoanOverDueCollectionTarget.getFirstqtrshort()
    );
    txnLoanGrowthTarget.setFirstqtrterm(
      txnLoanDisbursementTarget.getFirstqtrterm() - txnLoanDueCollectionTarget.getFirstqtrterm() - txnLoanOverDueCollectionTarget.getFirstqtrterm()
    );
    txnLoanGrowthTarget.setFirstqtrtotal(
      txnLoanGrowthTarget.getFirstqtrshort() + txnLoanGrowthTarget.getFirstqtrterm()
    );

    txnLoanGrowthTarget.setSecondqtrshort(
      txnLoanDisbursementTarget.getSecondqtrshort() - txnLoanDueCollectionTarget.getSecondqtrshort() - txnLoanOverDueCollectionTarget.getSecondqtrshort()
    );
    txnLoanGrowthTarget.setSecondqtrterm(
      txnLoanDisbursementTarget.getSecondqtrterm() - txnLoanDueCollectionTarget.getSecondqtrterm() - txnLoanOverDueCollectionTarget.getSecondqtrterm()
    );
    txnLoanGrowthTarget.setSecondqtrtotal(
      txnLoanGrowthTarget.getSecondqtrshort() + txnLoanGrowthTarget.getSecondqtrterm()
    );

    txnLoanGrowthTarget.setThirdqtrshort(
      txnLoanDisbursementTarget.getThirdqtrshort() - txnLoanDueCollectionTarget.getThirdqtrshort() - txnLoanOverDueCollectionTarget.getThirdqtrshort()
    );
    txnLoanGrowthTarget.setThirdqtrterm(
      txnLoanDisbursementTarget.getThirdqtrterm() - txnLoanDueCollectionTarget.getThirdqtrterm() - txnLoanOverDueCollectionTarget.getThirdqtrterm()
    );
    txnLoanGrowthTarget.setThirdqtrtotal(
      txnLoanGrowthTarget.getThirdqtrshort() + txnLoanGrowthTarget.getThirdqtrterm()
    );

    txnLoanGrowthTarget.setFourthqtrshort(
      txnLoanDisbursementTarget.getFourthqtrshort() - txnLoanDueCollectionTarget.getFourthqtrshort() - txnLoanOverDueCollectionTarget.getFourthqtrshort()
    );
    txnLoanGrowthTarget.setFourthqtrterm(
      txnLoanDisbursementTarget.getFourthqtrterm() - txnLoanDueCollectionTarget.getFourthqtrterm() - txnLoanOverDueCollectionTarget.getFourthqtrterm()
    );
    txnLoanGrowthTarget.setFourthqtrtotal(
      txnLoanGrowthTarget.getFourthqtrshort() + txnLoanGrowthTarget.getFourthqtrterm()
    );

    txnLoanGrowthTarget.setSumqtrshort(
      txnLoanGrowthTarget.getFirstqtrshort() + txnLoanGrowthTarget.getSecondqtrshort()
        + txnLoanGrowthTarget.getThirdqtrshort() + txnLoanGrowthTarget.getFourthqtrshort()
    );
    txnLoanGrowthTarget.setSumqtrterm(
      txnLoanGrowthTarget.getFirstqtrterm() + txnLoanGrowthTarget.getSecondqtrterm()
        + txnLoanGrowthTarget.getThirdqtrterm() + txnLoanGrowthTarget.getFourthqtrtotal()
    );
    txnLoanGrowthTarget.setSumqtrtotal(
      txnLoanGrowthTarget.getSumqtrshort() + txnLoanGrowthTarget.getSumqtrterm()
    );

    txnLoanGrowthTarget.setLoantotalcurestimated(txnLoan.getLoanTotalCurEstimated());
    txnLoanGrowthTarget.setTotalloanoutestimated(txnLoan.getTotalLoanOutEstimated());
    return txnLoanGrowthTarget;
  }


  private TxnLoanDueCollectionTarget getTxnLoanDueCollectionTarget(TxnLoan txnLoan, TxnLoanOverDueCollectionTarget txnLoanOverDueCollectionTarget) {
    TxnLoanDueCollectionTarget txnLoanDueCollectionTarget = new TxnLoanDueCollectionTarget();
    txnLoanDueCollectionTarget.setBranch(txnLoan.getBranch());
    txnLoanDueCollectionTarget.setLoanType(txnLoan.getLoanType());

    //FIRST QTR
    txnLoanDueCollectionTarget.setFirstqtrtotal(
      (int)
        Math.round(txnLoan.getCollectionPrFromDue() * 0.35)
    );
    txnLoanDueCollectionTarget.setFirstqtrshort(
      (int)
        Math.round(txnLoanDueCollectionTarget.getFirstqtrtotal() * 0.87)
    );
    txnLoanDueCollectionTarget.setFirstqtrterm(
      txnLoanDueCollectionTarget.getFirstqtrtotal() - txnLoanDueCollectionTarget.getFirstqtrshort()
    );

    //SECOND QTR
    txnLoanDueCollectionTarget.setSecondqtrtotal(
      txnLoanDueCollectionTarget.getFirstqtrtotal()
    );
    txnLoanDueCollectionTarget.setSecondqtrshort(
      (int)
        Math.round(txnLoanDueCollectionTarget.getSecondqtrtotal() * 0.88)
    );
    txnLoanDueCollectionTarget.setSecondqtrterm(
      txnLoanDueCollectionTarget.getSecondqtrtotal() - txnLoanDueCollectionTarget.getSecondqtrshort()
    );

    //THIRD QTR
    txnLoanDueCollectionTarget.setThirdqtrtotal(
      (int) Math.round((txnLoanDueCollectionTarget.getSecondqtrtotal() / 3.5) * 2)
    );
    txnLoanDueCollectionTarget.setThirdqtrshort(
      (int)
        Math.round(txnLoanDueCollectionTarget.getThirdqtrtotal() * 0.88)
    );
    txnLoanDueCollectionTarget.setThirdqtrterm(
      txnLoanDueCollectionTarget.getThirdqtrtotal() - txnLoanDueCollectionTarget.getThirdqtrshort()
    );

    //SUM QTR
    txnLoanDueCollectionTarget.setSumqtrshort(
      txnLoan.getLoanWcOutCurEstimated() + txnLoan.getDisburseLoanWcTarget() - txnLoan.getLoanOutWcEstimated()
        - txnLoanOverDueCollectionTarget.getSumqtrshort()
    );

    txnLoanDueCollectionTarget.setSumqtrterm(
      txnLoan.getLoanTlOutCurEstimated() + txnLoan.getDisburseLoanTlTarget() - txnLoan.getLoanOutTlEstimated()
        - txnLoanOverDueCollectionTarget.getSumqtrterm()
    );
    txnLoanDueCollectionTarget.setSumqtrtotal(
      txnLoanDueCollectionTarget.getSumqtrshort() + txnLoanDueCollectionTarget.getSumqtrterm()
    );

    //FOURTH QTR
    txnLoanDueCollectionTarget.setFourthqtrshort(
      txnLoanDueCollectionTarget.getSumqtrshort() - txnLoanDueCollectionTarget.getFirstqtrshort() - txnLoanDueCollectionTarget.getSecondqtrshort()
        - txnLoanDueCollectionTarget.getThirdqtrshort()
    );

    txnLoanDueCollectionTarget.setFourthqtrterm(
      txnLoanDueCollectionTarget.getSumqtrtotal() - txnLoanDueCollectionTarget.getFirstqtrterm() - txnLoanDueCollectionTarget.getSecondqtrterm()
        - txnLoanDueCollectionTarget.getThirdqtrterm()
    );

    txnLoanDueCollectionTarget.setFourthqtrtotal(
      txnLoanDueCollectionTarget.getFourthqtrshort() + txnLoanDueCollectionTarget.getFourthqtrterm()
    );

    return txnLoanDueCollectionTarget;
  }

  private TxnLoanOverDueCollectionTarget getTxnLoanOverdueCollectionTarget(TxnLoan txnLoan) {
    TxnLoanOverDueCollectionTarget txnLoanOverDueCollectionTarget = new TxnLoanOverDueCollectionTarget();
    txnLoanOverDueCollectionTarget.setBranch(txnLoan.getBranch());
    txnLoanOverDueCollectionTarget.setLoanType(txnLoan.getLoanType());

    //FIRST QTR
    txnLoanOverDueCollectionTarget.setFirstqtrtotal(
      (int)
        Math.round(txnLoan.getTotalCollectionPrFromOverDue() * 0.4)
    );
    txnLoanOverDueCollectionTarget.setFirstqtrshort(
      (int)
        Math.round(txnLoanOverDueCollectionTarget.getFirstqtrtotal() * 0.6)
    );
    txnLoanOverDueCollectionTarget.setFirstqtrterm(
      txnLoanOverDueCollectionTarget.getFirstqtrtotal() - txnLoanOverDueCollectionTarget.getFirstqtrshort()
    );

    //SECOND QTR
    txnLoanOverDueCollectionTarget.setSecondqtrtotal(
      (txnLoanOverDueCollectionTarget.getFirstqtrtotal() / 4) * 3
    );
    txnLoanOverDueCollectionTarget.setSecondqtrshort(
      (int)
        Math.round(txnLoanOverDueCollectionTarget.getSecondqtrtotal() * 0.6)
    );
    txnLoanOverDueCollectionTarget.setSecondqtrterm(
      txnLoanOverDueCollectionTarget.getSecondqtrtotal() - txnLoanOverDueCollectionTarget.getSecondqtrshort()
    );

    //THIRD QTR
    txnLoanOverDueCollectionTarget.setThirdqtrtotal(
      (txnLoanOverDueCollectionTarget.getSecondqtrtotal() / 3) * 2
    );
    txnLoanOverDueCollectionTarget.setThirdqtrshort(
      (int)
        Math.round(txnLoanOverDueCollectionTarget.getThirdqtrtotal() * 0.6)
    );
    txnLoanOverDueCollectionTarget.setThirdqtrterm(
      txnLoanOverDueCollectionTarget.getThirdqtrtotal() - txnLoanOverDueCollectionTarget.getThirdqtrshort()
    );
    //FOURTH QTR
    txnLoanOverDueCollectionTarget.setFourthqtrtotal(
      txnLoanOverDueCollectionTarget.getThirdqtrtotal() / 2
    );
    txnLoanOverDueCollectionTarget.setFourthqtrshort(
      (int)
        Math.round(txnLoanOverDueCollectionTarget.getFourthqtrtotal() * 0.6)
    );
    txnLoanOverDueCollectionTarget.setFourthqtrterm(
      txnLoanOverDueCollectionTarget.getFourthqtrtotal() - txnLoanOverDueCollectionTarget.getFourthqtrshort()
    );
    //SUM QTR
    txnLoanOverDueCollectionTarget.setSumqtrshort(
      txnLoanOverDueCollectionTarget.getFirstqtrshort() + txnLoanOverDueCollectionTarget.getSecondqtrshort()
        + txnLoanOverDueCollectionTarget.getThirdqtrshort() + txnLoanOverDueCollectionTarget.getFourthqtrshort()
    );
    txnLoanOverDueCollectionTarget.setSumqtrterm(
      txnLoanOverDueCollectionTarget.getFirstqtrterm() + txnLoanOverDueCollectionTarget.getSecondqtrterm()
        + txnLoanOverDueCollectionTarget.getThirdqtrterm() + txnLoanOverDueCollectionTarget.getFourthqtrterm()
    );
    txnLoanOverDueCollectionTarget.setSumqtrtotal(
      txnLoanOverDueCollectionTarget.getSumqtrshort() + txnLoanOverDueCollectionTarget.getSumqtrterm()
    );


    return txnLoanOverDueCollectionTarget;
  }
}
