package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnLoanGrowthTarget;
import com.budgetplan.app.txn.repository.TxnLoanGrowthTargetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TxnLoanGrowthTargetServiceImpl implements TxnLoanGrowthTargetService {
    private TxnLoanGrowthTargetRepository txnLoanGrowthTargetRepository;

    @Autowired
    public void setTxnLoanGrowthTargetRepository(TxnLoanGrowthTargetRepository txnLoanGrowthTargetRepository) {
        this.txnLoanGrowthTargetRepository = txnLoanGrowthTargetRepository;
    }

    @Override
    public List<TxnLoanGrowthTarget> getByBranchCode(String branchCode) {
        return txnLoanGrowthTargetRepository.findAllByBranchBranchCode(branchCode);
    }
}
