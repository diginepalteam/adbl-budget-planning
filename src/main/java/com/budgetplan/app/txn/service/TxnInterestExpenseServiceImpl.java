package com.budgetplan.app.txn.service;

import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnInterestExpenseProjection;
import com.budgetplan.app.txn.repository.TxnInterestExpenseProjectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class TxnInterestExpenseServiceImpl implements GenericCrudService<TxnInterestExpenseProjection, Integer> {
    private TxnInterestExpenseProjectionRepository txnInterestExpenseProjectionRepository;

    @Autowired
    public void setTxnInterestExpenseProjectionRepository(TxnInterestExpenseProjectionRepository txnInterestExpenseProjectionRepository) {
        this.txnInterestExpenseProjectionRepository = txnInterestExpenseProjectionRepository;
    }

    @Override
    public Optional<TxnInterestExpenseProjection> save(TxnInterestExpenseProjection entity) {
        return Optional.of(txnInterestExpenseProjectionRepository.save(entity));
    }

    @Override
    public List<TxnInterestExpenseProjection> saveAll(List<TxnInterestExpenseProjection> entities) {
        return txnInterestExpenseProjectionRepository.saveAll(entities);
    }

    //todo
    @Override
    public Optional<TxnInterestExpenseProjection> update(TxnInterestExpenseProjection entity, Integer id) {
        return Optional.empty();
    }

    @Override
    public List<TxnInterestExpenseProjection> getAll() {
        return txnInterestExpenseProjectionRepository.findAll();
    }

    @Override
    public Optional<TxnInterestExpenseProjection> getOne(Integer id) {
        return txnInterestExpenseProjectionRepository.findById(id);
    }
}
