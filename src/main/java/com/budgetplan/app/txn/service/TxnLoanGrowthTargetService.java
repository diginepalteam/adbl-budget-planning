package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnLoanGrowthTarget;

import java.util.List;

public interface TxnLoanGrowthTargetService {
    List<TxnLoanGrowthTarget> getByBranchCode(String branchCode);
}
