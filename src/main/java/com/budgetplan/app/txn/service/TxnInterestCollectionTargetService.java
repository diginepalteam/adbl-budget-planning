package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnInterestCollectionTarget;

import java.util.List;

public interface TxnInterestCollectionTargetService {
    List<TxnInterestCollectionTarget> getByBranchCode(String branchCode);
}
