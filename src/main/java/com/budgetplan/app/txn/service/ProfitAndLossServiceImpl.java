package com.budgetplan.app.txn.service;

import com.budgetplan.app.integration.MisIntegrationService;
import com.budgetplan.app.integration.responses.MisProfitAndLoss;
import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.TxnProfitAndLossHeadingEnum;
import com.budgetplan.app.txn.entity.ProfitAndLoss;
import com.budgetplan.app.txn.repository.ProfitAndLossRepository;
import com.budgetplan.app.utils.CustomBeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class ProfitAndLossServiceImpl implements ProfitAndLossService, GenericCrudService<ProfitAndLoss, Integer> {
  private ProfitAndLossRepository profitAndLossRepository;
  private MisIntegrationService misIntegrationService;

  @Autowired
  public void setProfitAndLossRepository(ProfitAndLossRepository profitAndLossRepository) {
    this.profitAndLossRepository = profitAndLossRepository;
  }

  @Autowired
  public void setMisIntegrationService(MisIntegrationService misIntegrationService) {
    this.misIntegrationService = misIntegrationService;
  }

  @Override
  public Optional<ProfitAndLoss> save(ProfitAndLoss entity) {
    return Optional.of(profitAndLossRepository.save(entity));
  }

  @Override
  public List<ProfitAndLoss> saveAll(List<ProfitAndLoss> entities) {
    return profitAndLossRepository.saveAll(entities);
  }

  @Override
  public Optional<ProfitAndLoss> update(ProfitAndLoss entity, Integer id) {
    Optional<ProfitAndLoss> profitAndLossOptional = this.getOne(id);
    if (profitAndLossOptional.isPresent()) {
      ProfitAndLoss profitAndLoss = profitAndLossOptional.get();
      CustomBeanUtils.copyNonNullAndNonZeroProperties(entity, profitAndLoss);
      return this.save(profitAndLoss);
    }
    return Optional.empty();
  }

  @Override
  public List<ProfitAndLoss> getAll() {
    return profitAndLossRepository.findAll();
  }

  @Override
  public Optional<ProfitAndLoss> getOne(Integer id) {
    return Optional.of(profitAndLossRepository.getOne(id));
  }

  @Override
  public List<ProfitAndLoss> saveProfitAndLossFromMis() {
    List<ProfitAndLoss> profitAndLosses = new ArrayList<>();

    List<MisProfitAndLoss> profitAndLossesThisYear = misIntegrationService.getProfitAndLoss();
    profitAndLossesThisYear.forEach(misProfitAndLoss -> {
      ProfitAndLoss profitAndLoss = new ProfitAndLoss();

      MdBranch branch = new MdBranch();
      branch.setBranchCode(misProfitAndLoss.getBranchcode());
      profitAndLoss.setBranch(branch);
      profitAndLoss.setDescription(misProfitAndLoss.getBalancehead());
      profitAndLoss.setThisyearactual(misProfitAndLoss.getBalanceamount());
      profitAndLoss.setLastyearactual(misProfitAndLoss.getBalanceamountlastyr());

      profitAndLosses.add(profitAndLoss);
    });

    return this.saveAll(profitAndLosses);
  }

  @Override
  public List<ProfitAndLoss> getByBranchCode(String branchCode) {
    return profitAndLossRepository.findAllByBranchBranchCode(branchCode);
  }

  private void setHead(MisProfitAndLoss misProfitAndLoss, Map<String, Integer> map) {
    Field[] fields = misProfitAndLoss.getClass().getDeclaredFields();
    for (Field field : fields) {
      try {
        int value = (Integer) field.get(misProfitAndLoss);
        String head = TxnProfitAndLossHeadingEnum.valueOf(field.getName()).getHead();
        map.put(head, value);
      } catch (Exception e) {
        log.warn("Value not found for field {}", field.getName());
      }
    }
  }
}
