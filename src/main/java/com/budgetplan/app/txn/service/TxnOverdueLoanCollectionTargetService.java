package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnLoanOverDueCollectionTarget;

import java.util.List;

public interface TxnOverdueLoanCollectionTargetService {
    List<TxnLoanOverDueCollectionTarget> getByBranchCode(String branchCode);
}
