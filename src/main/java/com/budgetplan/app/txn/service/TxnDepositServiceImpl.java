package com.budgetplan.app.txn.service;

import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnDeposit;
import com.budgetplan.app.txn.entity.TxnLoan;
import com.budgetplan.app.txn.repository.TxnDepositRepository;
import com.budgetplan.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TxnDepositServiceImpl implements TxnDepositService, GenericCrudService<TxnDeposit,Integer> {
    private TxnDepositRepository txnDepositRepository;

    @Autowired
    public void setTxnDepositRepository(TxnDepositRepository txnDepositRepository) {
        this.txnDepositRepository = txnDepositRepository;
    }

    @Override
    public Optional<TxnDeposit> save(TxnDeposit entity) {
        return Optional.of(txnDepositRepository.save(entity));
    }

    @Override
    public List<TxnDeposit> saveAll(List<TxnDeposit> entities) {
        return txnDepositRepository.saveAll(entities);
    }

    @Override
    public Optional<TxnDeposit> update(TxnDeposit entity, Integer id) {
        Optional<TxnDeposit> txnDepositOptional = this.getOne(id);
        if(txnDepositOptional.isPresent()){
            TxnDeposit existingTxnDeposit = txnDepositOptional.get();
            CustomBeanUtils.copyNonNullAndNonZeroProperties(entity, existingTxnDeposit);
            return Optional.of(txnDepositRepository.save(existingTxnDeposit));
        }
        return Optional.empty();
    }

    @Override
    public List<TxnDeposit> getAll() {
        return txnDepositRepository.findAll();
    }

    @Override
    public Optional<TxnDeposit> getOne(Integer id) {
        return txnDepositRepository.findById(id);
    }

    @Override
    public List<TxnDeposit> getByBranchCode(String branchCode) {
        return txnDepositRepository.findAllByBranchBranchCode(branchCode);
    }
}
