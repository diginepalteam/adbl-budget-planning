package com.budgetplan.app.txn.service;

import com.budgetplan.app.integration.MisIntegrationService;
import com.budgetplan.app.integration.responses.MisBalanceSheet;
import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.txn.entity.TxnBalanceSheet;
import com.budgetplan.app.txn.repository.TxnBalanceSheetRepository;
import com.budgetplan.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TxnBalanceSheetServiceImpl implements TxnBalanceSheetService {
  private TxnBalanceSheetRepository txnBalanceSheetRepository;
  private MisIntegrationService misIntegrationService;

  @Autowired
  public void setTxnBalanceSheetRepository(TxnBalanceSheetRepository txnBalanceSheetRepository) {
    this.txnBalanceSheetRepository = txnBalanceSheetRepository;
  }

  @Autowired
  public void setMisIntegrationService(MisIntegrationService misIntegrationService) {
    this.misIntegrationService = misIntegrationService;
  }

  @Override
  public List<TxnBalanceSheet> syncWithMis() {
    List<MisBalanceSheet> misBalanceSheets = misIntegrationService.getBalanceSheets();
    if (!misBalanceSheets.isEmpty()) {
      List<TxnBalanceSheet> balanceSheets = new ArrayList<>();
      misBalanceSheets.forEach(misBalanceSheet -> {
        TxnBalanceSheet txnBalanceSheet = new TxnBalanceSheet();

        txnBalanceSheet.setDescription(misBalanceSheet.getBalancehead());
        txnBalanceSheet.setLastyearactual(misBalanceSheet.getBalanceamountlastyr());
        txnBalanceSheet.setThisyearactual(misBalanceSheet.getBalanceamount());

        //todo set other values from mis
        MdBranch branch = new MdBranch();
        branch.setBranchCode(misBalanceSheet.getBranchcode());

        txnBalanceSheet.setBranch(branch);

        balanceSheets.add(txnBalanceSheet);
      });

      return txnBalanceSheetRepository.saveAll(balanceSheets);
    }
    return new ArrayList<>();
  }

  @Override
  public List<TxnBalanceSheet> getByBranchCode(String branchCode) {
    return txnBalanceSheetRepository.findAllByBranchBranchCode(branchCode);
  }

  @Override
  public Optional<TxnBalanceSheet> save(TxnBalanceSheet entity) {
    return Optional.of(txnBalanceSheetRepository.save(entity));
  }

  @Override
  public List<TxnBalanceSheet> saveAll(List<TxnBalanceSheet> entities) {
    return txnBalanceSheetRepository.saveAll(entities);
  }

  @Override
  public Optional<TxnBalanceSheet> update(TxnBalanceSheet entity, Integer id) {
    Optional<TxnBalanceSheet> balanceSheetOptional = this.getOne(id);
    if (balanceSheetOptional.isPresent()) {
      TxnBalanceSheet existingBalanceSheet = balanceSheetOptional.get();
      CustomBeanUtils.copyNonNullAndNonZeroProperties(entity, existingBalanceSheet);
      return this.save(existingBalanceSheet);
    }
    return Optional.empty();
  }

  @Override
  public List<TxnBalanceSheet> getAll() {
    return txnBalanceSheetRepository.findAll();
  }

  @Override
  public Optional<TxnBalanceSheet> getOne(Integer id) {
    return Optional.of(txnBalanceSheetRepository.getOne(id));
  }
}
