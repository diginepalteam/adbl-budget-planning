package com.budgetplan.app.txn.service;

import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnInterestExpenseProjection;
import com.budgetplan.app.txn.repository.TxnInterestExpenseProjectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TxnInterestExpenseProjectionServiceImpl implements TxnInterestExpenseProjectionService {
    private TxnInterestExpenseProjectionRepository txnInterestExpenseProjectionRepository;

    @Autowired
    public void setTxnInterestExpenseProjectionRepository(TxnInterestExpenseProjectionRepository txnInterestExpenseProjectionRepository) {
        this.txnInterestExpenseProjectionRepository = txnInterestExpenseProjectionRepository;
    }

    @Override
    public List<TxnInterestExpenseProjection> findByBranchCode(String branchCode) {
        return txnInterestExpenseProjectionRepository.findAllByBranchBranchCode(branchCode);
    }
}
