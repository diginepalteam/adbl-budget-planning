package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.CurrentYearRemainingCommit;

import java.util.List;

public interface CurrentYearRemainingCommitService {
  List<CurrentYearRemainingCommit> getByBranchCode(String branchCode);
}
