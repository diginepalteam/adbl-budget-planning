package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnLoanDisbursementTarget;
import com.budgetplan.app.txn.entity.TxnLoanOverDueCollectionTarget;
import com.budgetplan.app.txn.repository.TxnLoanDisbursementTargetRepository;
import com.budgetplan.app.txn.repository.TxnLoanOverDueCollectionTargetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TxnOverdueLoanCollectionTargetServiceImpl implements TxnOverdueLoanCollectionTargetService{
    private TxnLoanOverDueCollectionTargetRepository txnLoanOverDueCollectionTargetRepository;

    @Autowired
    public void setTxnLoanOverDueCollectionTargetRepository(TxnLoanOverDueCollectionTargetRepository txnLoanOverDueCollectionTargetRepository) {
        this.txnLoanOverDueCollectionTargetRepository = txnLoanOverDueCollectionTargetRepository;
    }

    @Override
    public List<TxnLoanOverDueCollectionTarget> getByBranchCode(String branchCode) {
        return txnLoanOverDueCollectionTargetRepository.findAllByBranchBranchCode(branchCode);
    }
}
