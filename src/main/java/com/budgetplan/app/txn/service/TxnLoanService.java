package com.budgetplan.app.txn.service;

import com.budgetplan.app.txn.entity.TxnLoan;

import java.util.List;
import java.util.Optional;

public interface TxnLoanService {
    List<TxnLoan> getByBranchCode(String branchCode);

    void postLoanEvent();
}
