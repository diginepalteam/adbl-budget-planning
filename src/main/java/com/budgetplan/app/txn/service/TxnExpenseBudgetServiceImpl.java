package com.budgetplan.app.txn.service;

import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.md.entity.MdIncomeFromLoan;
import com.budgetplan.app.md.repository.MdIncomeFromLoanRepository;
import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnExpenseBudget;
import com.budgetplan.app.txn.repository.TxnExpenseBudgetRepository;
import com.budgetplan.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TxnExpenseBudgetServiceImpl implements TxnExpenseBudgetService, GenericCrudService<TxnExpenseBudget, Integer> {
    private TxnExpenseBudgetRepository txnExpenseBudgetRepository;
    private MdIncomeFromLoanRepository mdIncomeFromLoanRepository;

    @Autowired
    public void setTxnExpenseBudgetRepository(TxnExpenseBudgetRepository txnExpenseBudgetRepository) {
        this.txnExpenseBudgetRepository = txnExpenseBudgetRepository;
    }

    @Autowired
    public void setMdIncomeFromLoanRepository(MdIncomeFromLoanRepository mdIncomeFromLoanRepository) {
        this.mdIncomeFromLoanRepository = mdIncomeFromLoanRepository;
    }

    @Override
    public Optional<TxnExpenseBudget> save(TxnExpenseBudget entity) {
        return Optional.of(txnExpenseBudgetRepository.save(entity));
    }

    @Override
    public List<TxnExpenseBudget> saveAll(List<TxnExpenseBudget> entities) {
        return txnExpenseBudgetRepository.saveAll(entities);
    }

    @Override
    public Optional<TxnExpenseBudget> update(TxnExpenseBudget entity, Integer id) {
        Optional<TxnExpenseBudget> txnExpenseBudgetOptional = this.getOne(id);
        if (txnExpenseBudgetOptional.isPresent()) {
            TxnExpenseBudget existingExpenseBudget = txnExpenseBudgetOptional.get();
            CustomBeanUtils.copyNonNullAndNonZeroProperties(entity, existingExpenseBudget);
            return Optional.of(txnExpenseBudgetRepository.save(existingExpenseBudget));
        }
        return Optional.empty();
    }

    @Override
    public List<TxnExpenseBudget> getAll() {
        return txnExpenseBudgetRepository.findAll();
    }

    @Override
    public Optional<TxnExpenseBudget> getOne(Integer id) {
        return txnExpenseBudgetRepository.findById(id);
    }

    @Override
    public List<TxnExpenseBudget> getByBranchCode(String branchCode) {
        return txnExpenseBudgetRepository.findAllByBranchBranchCode(branchCode);
    }

    @Override
    public List<TxnExpenseBudget> setInitialEntry(String branchCode) {
        if (txnExpenseBudgetRepository.countAllByBranchBranchCode(branchCode) == 0) {
            List<MdIncomeFromLoan> mdIncomeFromLoans = mdIncomeFromLoanRepository.findAll();
            if (!mdIncomeFromLoans.isEmpty()) {
                MdBranch branch = new MdBranch();
                branch.setBranchCode(branchCode);

                List<TxnExpenseBudget> txnExpenseBudgets = new ArrayList<>();
                mdIncomeFromLoans.forEach(mdIncomeFromLoan -> {
                    TxnExpenseBudget txnExpenseBudget = new TxnExpenseBudget();
                    txnExpenseBudget.setBranch(branch);

                    txnExpenseBudgets.add(txnExpenseBudget);
                });

                if (!txnExpenseBudgets.isEmpty()) {
                    return this.saveAll(txnExpenseBudgets);
                }
            }
        }
        return new ArrayList<>();
    }
}
