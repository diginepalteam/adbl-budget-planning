package com.budgetplan.app.txn.service;

import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnInterestCollectionTarget;
import com.budgetplan.app.txn.repository.TxnInterestCollectionTargetRepository;
import com.budgetplan.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TxnInterestCollectionTargetServiceImpl implements GenericCrudService<TxnInterestCollectionTarget, Integer>, TxnInterestCollectionTargetService {
  private TxnInterestCollectionTargetRepository txnInterestCollectionTargetRepository;

  @Autowired
  public void setTxnInterestCollectionTargetRepository(TxnInterestCollectionTargetRepository txnInterestCollectionTargetRepository) {
    this.txnInterestCollectionTargetRepository = txnInterestCollectionTargetRepository;
  }


  @Override
  public Optional<TxnInterestCollectionTarget> save(TxnInterestCollectionTarget entity) {
    return Optional.of(txnInterestCollectionTargetRepository.save(entity));
  }

  @Override
  public List<TxnInterestCollectionTarget> saveAll(List<TxnInterestCollectionTarget> entities) {
    return txnInterestCollectionTargetRepository.saveAll(entities);
  }

  @Override
  public Optional<TxnInterestCollectionTarget> update(TxnInterestCollectionTarget entity, Integer id) {
    if (null != id && id.equals(0)) {
      Optional<TxnInterestCollectionTarget> txnInterestCollectionTarget = this.getOne(id);
      txnInterestCollectionTarget.ifPresent(t -> {
        CustomBeanUtils.copyNonNullAndNonZeroProperties(entity, t);
        this.save(t);
      });
    } else {
      this.save(entity);
    }
    return Optional.of(entity);
  }

  @Override
  public List<TxnInterestCollectionTarget> getAll() {
    return txnInterestCollectionTargetRepository.findAll();
  }

  @Override
  public Optional<TxnInterestCollectionTarget> getOne(Integer id) {
    return txnInterestCollectionTargetRepository.findById(id);
  }

  @Override
  public List<TxnInterestCollectionTarget> getByBranchCode(String branchCode) {
    return txnInterestCollectionTargetRepository.findAllByBranchBranchCode(branchCode);
  }
}
