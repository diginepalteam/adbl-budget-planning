package com.budgetplan.app.txn.entity;

import com.budgetplan.app.txn.listener.TxnLoanEntityListener;
import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.md.entity.MdLoanType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@EntityListeners(TxnLoanEntityListener.class)
@Getter
@Setter
@Entity
@Table(name = "txn_loan")
public class TxnLoan {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private int id;
  @ManyToOne(optional = true)
  private MdBranch branch;
  @ManyToOne(optional = true)
  private MdLoanType loanType;
  @Column(name = "workingcapitaloutstandingcurrent")
  private int workingCapitalOutstandingCurrent;
  @Column(name = "termloanoutcur")
  @ColumnDefault(value="0")
  private int termLoanOutCur;
  @Column(name = "totalloanoutcur")
  @ColumnDefault(value="0")
  private int totalLoanOutCur;
  @Column(name = "pastdueloanoutcur")
  @ColumnDefault(value="0")
  private int pastDueLoanOutCur;
  @Column(name = "nonperformingloanoutcur")
  @ColumnDefault(value="0")
  private int nonPerformingLoanOutCur;
  @Column(name = "nonperforminginterestoutcur")
  @ColumnDefault(value="0")
  private int nonPerformingInterestOutCur;
  @Column(name = "disbursementcurestimated")
  @ColumnDefault(value="0")
  private int disbursementCurEstimated;
  @Column(name = "collectionprcurestimated")
  @ColumnDefault(value="0")
  private int collectionPrCurEstimated;
  @Column(name = "interestadditioncurestimated")
  @ColumnDefault(value="0")
  private int interestAdditionCurEstimated;
  @Column(name = "interestcollectioncurestimated")
  @ColumnDefault(value="0")
  private int interestCollectionCurEstimated;
  @Column(name = "loanwcoutcurestimated")
  @ColumnDefault(value="0")
  private int loanWcOutCurEstimated;
  @Column(name = "loantloutcurestimated")
  @ColumnDefault(value="0")
  private int loanTlOutCurEstimated;
  @Column(name = "loantotalcurestimated")
  @ColumnDefault(value="0")
  private int loanTotalCurEstimated;
  @Column(name = "pastdueloanoutcurestimated")
  @ColumnDefault(value="0")
  private int pastDueLoanOutCurEstimated;
  @Column(name = "nonperformingloanoutcurestimated")
  @ColumnDefault(value="0")
  private int nonPerformingLoanOutCurEstimated;
  @Column(name = "nonperformingontoutcurestimated")
  @ColumnDefault(value="0")
  private int nonPerformingOnTOutCurEstimated;
  @Column(name = "disburseloanwctarget")
  @ColumnDefault(value="0")
  private int disburseLoanWcTarget;
  @Column(name = "disburseloantltarget")
  @ColumnDefault(value="0")
  private int disburseLoanTlTarget;
  @Column(name = "disburseloantotaltarget")
  @ColumnDefault(value="0")
  private int disburseLoanTotalTarget;
  @Column(name = "getduelastdisbursedloan")
  @ColumnDefault(value="0")
  private int getDueLastDisbursedLoan;
  @Column(name = "getduethisdisbursedloan")
  @ColumnDefault(value="0")
  private int getDueThisDisbursedLoan;
  @Column(name = "collectionprfromdue")
  @ColumnDefault(value="0")
  private int collectionPrFromDue;
  @Column(name = "collectionprfromnonperming")
  @ColumnDefault(value="0")
  private int collectionPrFromNonPerming;
  @Column(name = "totalcollectionprfromoverdue")
  @ColumnDefault(value="0")
  private int totalCollectionPrFromOverDue;
  @Column(name = "totalprcollectiontarget")
  @ColumnDefault(value="0")
  private int totalPrCollectionTarget;
  @Column(name = "collectionnonperforminginttarget")
  @ColumnDefault(value="0")
  private int collectionNonPerformingIntTarget;
  @Column(name = "annualint")
  @ColumnDefault(value="0")
  private int annualInt;
  @Column(name = "annualinttarget")
  @ColumnDefault(value="0")
  private int annualIntTarget;
  @Column(name = "totalintcoltarget")
  @ColumnDefault(value="0")
  private int totalIntColTarget;
  @Column(name = "loanoutwcestimated")
  @ColumnDefault(value="0")
  private int loanOutWcEstimated;
  @Column(name = "loanouttlestimated")
  @ColumnDefault(value="0")
  private int loanOutTlEstimated;
  @Column(name = "totalloanoutestimated")
  @ColumnDefault(value="0")
  private int totalLoanOutEstimated;
  @Column(name = "overdueloanestimated")
  @ColumnDefault(value="0")
  private int overDueLoanEstimated;
  @Column(name = "nonperformingloanestimated")
  @ColumnDefault(value="0")
  private int nonPerformingLoanEstimated;
  @Column(name = "nonperformingintestimated")
  @ColumnDefault(value="0")
  private int nonPerformingIntEstimated;

  @Transient
  private String outStandingWeight;
  @Transient
  private boolean calculation; // if calculation is true then the whole required calculation is done before preInsert / preUpdate
}

