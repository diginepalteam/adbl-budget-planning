package com.budgetplan.app.txn.entity;

import com.budgetplan.app.txn.listener.TxnLoanEntityListener;
import com.budgetplan.app.md.entity.MdBranch;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@EntityListeners(TxnLoanEntityListener.class)
@Getter
@Setter
@Entity
@Table(name = "txn_interestcollectiontarget")
public class TxnInterestCollectionTarget {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private int id;
  @ManyToOne(optional = true)
  private MdBranch branch;
  private String head;
  @ColumnDefault(value = "0")
  private int shrawan;
  @ColumnDefault(value = "0")
  private int bhadra;
  @ColumnDefault(value = "0")
  private int ahwin;
  @ColumnDefault(value = "0")
  private int kartik;
  @ColumnDefault(value = "0")
  private int mangsir;
  @ColumnDefault(value = "0")
  private int paush;
  @ColumnDefault(value = "0")
  private int magh;
  @ColumnDefault(value = "0")
  private int falgun;
  @ColumnDefault(value = "0")
  private int chaitra;
  @ColumnDefault(value = "0")
  private int baisekh;
  @ColumnDefault(value = "0")
  private int jesth;
  @ColumnDefault(value = "0")
  private int ashadh;
  @ColumnDefault(value = "0")
  private int total;
}

