package com.budgetplan.app.txn.entity;

import com.budgetplan.app.md.entity.MdBranch;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "txn_balancesheet")
public class TxnBalanceSheet {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @ManyToOne(optional = true)
  private MdBranch branch;

  private String blid;
  private String description;
  private int lastyearactual;
  private int thisyearactual;
  private int thisyearprojectedtotal;
  private int nextyearprojected;
}
