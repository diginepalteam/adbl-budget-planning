package com.budgetplan.app.txn.entity;

import com.budgetplan.app.md.entity.MdBranch;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Data
@Entity
@Table(name = "txn_currentyearremaingcommit")
public class CurrentYearRemainingCommit {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  @OneToOne(optional = true)
  private MdBranch branch;

  @ColumnDefault(value="0")
  private int currentoutstanding;
  @ColumnDefault(value="0")
  private int disbursementwithrenewal;
  @ColumnDefault(value="0")
  private int collectiontwithrenewaloverdue;
  @ColumnDefault(value="0")
  private int collectiontwithrenewaldue;
  @ColumnDefault(value="0")
  private int outstandinginashadh;
}
