package com.budgetplan.app.txn.entity;

import com.budgetplan.app.md.entity.MdBranch;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "txn_interestexpenseprojection")
public class TxnInterestExpenseProjection {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
    @ManyToOne(optional = true)
    private MdBranch branch;
    private String mainType;
    private String depositType;

    @ColumnDefault(value="0")
  private int depositoutcur;
    @ColumnDefault(value="0")
  private int depositoutcurtotalestimated;
    @ColumnDefault(value="0")
  private int firstqtrnextprojected;
    @ColumnDefault(value="0")
  private int secondqtrnextprojected;
    @ColumnDefault(value="0")
  private int thirdqtrnextprojected;
    @ColumnDefault(value="0")
  private int fourthqtrnextprojected;
    @ColumnDefault(value="0")
  private int firstqtrnextaverageoutstanding;
    @ColumnDefault(value="0")
  private int secondqtrnextaverageoutstanding;
    @ColumnDefault(value="0")
  private int thirdqtrnextaverageoutstanding;
    @ColumnDefault(value="0")
  private int fourthqtrnextaverageoutstanding;
    private double interestrate;
    @ColumnDefault(value="0")
  private int firstqtrnextinterestexp;
    @ColumnDefault(value="0")
  private int secondqtrnextinterestexp;
    @ColumnDefault(value="0")
  private int thirdqtrnextinterestexp;
    @ColumnDefault(value="0")
  private int fourthqtrnextinterestexp;
    @ColumnDefault(value="0")
  private int totalnextinterestexp;

    private String category;

    @ColumnDefault(value="0")
  private int txnDepositId;
}
