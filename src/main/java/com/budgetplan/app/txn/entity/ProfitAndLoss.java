package com.budgetplan.app.txn.entity;

import com.budgetplan.app.md.entity.MdBranch;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "txn_profitandloss")
public class ProfitAndLoss {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  @ManyToOne(optional = true)
  private MdBranch branch;

  private String plid;
  private String description;
  private int lastyearactual;
  private int thisyearactual;
  private int thisyearprojectedremained;
  private int thisyeartotal;
  private int nextyearprojected;

}
