package com.budgetplan.app.txn.entity;

import com.budgetplan.app.commons.Auditable;
import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.md.entity.MdLoanType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "txn_loanoverduecollectiontarget")
public class TxnLoanOverDueCollectionTarget extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

    @ManyToOne(optional = true)
    private MdBranch branch;
    @ManyToOne(optional = true)
    private MdLoanType loanType;

    @ColumnDefault(value="0")
  private int firstqtrshort;
    @ColumnDefault(value="0")
  private int firstqtrterm;
    @ColumnDefault(value="0")
  private int firstqtrtotal;

    @ColumnDefault(value="0")
  private int secondqtrshort;
    @ColumnDefault(value="0")
  private int secondqtrterm;
    @ColumnDefault(value="0")
  private int secondqtrtotal;

    @ColumnDefault(value="0")
  private int thirdqtrshort;
    @ColumnDefault(value="0")
  private int thirdqtrterm;
    @ColumnDefault(value="0")
  private int thirdqtrtotal;

    @ColumnDefault(value="0")
  private int fourthqtrshort;
    @ColumnDefault(value="0")
  private int fourthqtrterm;
    @ColumnDefault(value="0")
  private int fourthqtrtotal;

    @ColumnDefault(value="0")
  private int sumqtrshort;
    @ColumnDefault(value="0")
  private int sumqtrterm;
    @ColumnDefault(value="0")
  private int sumqtrtotal;
}
