package com.budgetplan.app.txn.entity;

import com.budgetplan.app.txn.listener.TxnLoanClassificationEntityListener;
import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.md.entity.MdLoanType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@EntityListeners(TxnLoanClassificationEntityListener.class)
@Getter
@Setter
@Entity
@Table(name = "txn_loanclassification")
public class TxnLoanClassification {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
  private int id;
    @ManyToOne(optional = true)
    private MdBranch branch;
    @ManyToOne(optional = true)
    private MdLoanType loanType;

    @ColumnDefault(value="0")
  private int passcur;
    @ColumnDefault(value="0")
  private int watchlistcur;
    @ColumnDefault(value="0")
  private int substandardcur;
    @ColumnDefault(value="0")
  private int doubtfulcur;
    @ColumnDefault(value="0")
  private int losscur;
    @ColumnDefault(value="0")
  private int totalcur;
    @ColumnDefault(value="0")
  private int passthisprojected;
    @ColumnDefault(value="0")
  private int watchlistthisprojected;
    @ColumnDefault(value="0")
  private int substandardthisprojected;
    @ColumnDefault(value="0")
  private int doubtfulthisprojected;
    @ColumnDefault(value="0")
  private int lossthisprojected;
    @ColumnDefault(value="0")
  private int totalthisprojected;
    @ColumnDefault(value="0")
  private int passnextprojected;
    @ColumnDefault(value="0")
  private int watchlistnextprojected;
    @ColumnDefault(value="0")
  private int substandardnextprojected;
    @ColumnDefault(value="0")
  private int doubtfulnextprojected;
    @ColumnDefault(value="0")
  private int lossnextprojected;
    @ColumnDefault(value="0")
  private int totalnextprojected;
}
