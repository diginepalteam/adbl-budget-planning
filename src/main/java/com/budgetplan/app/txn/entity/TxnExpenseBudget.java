package com.budgetplan.app.txn.entity;

import com.budgetplan.app.txn.listener.TxnExpenseBudgetListener;
import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.md.entity.MdExpenseCode;
import com.budgetplan.app.md.entity.MdExpenseMain;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@EntityListeners(TxnExpenseBudgetListener.class)
@Getter
@Setter
@Entity
@Table(name = "txn_expensebudget")
public class TxnExpenseBudget {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  @ManyToOne(optional = true)
  private MdBranch branch;
  @ManyToOne(optional = true)
  @JoinColumn(name = "expense_code")
  private MdExpenseCode expenseCode;

 @ManyToOne
 @JoinColumn(name = "maincode")
 private MdExpenseMain expenseMain;

  @ColumnDefault(value = "0")
  private int actualexpenselastyear;

  @ColumnDefault(value = "0")
  private int currentyearbudget;
  @ColumnDefault(value = "0")
  private int currentyearadditionalbudget;
  @ColumnDefault(value = "0")
  private int totalcurrentyearbudget;
  @ColumnDefault(value = "0")
  private int actualexpensethisyear;
  @ColumnDefault(value = "0")
  private int currentyearexpenseprojected;
  @ColumnDefault(value = "0")
  private int currentyearexpenseprojectedannual;
  @ColumnDefault(value = "0")
  private int nextyearexpensebudgetprojection;

  private String remarks;
}
