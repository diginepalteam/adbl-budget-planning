package com.budgetplan.app.txn.entity;

import com.budgetplan.app.txn.listener.TxnDepositEntityListener;
import com.budgetplan.app.md.entity.MdBranch;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@EntityListeners(TxnDepositEntityListener.class)
@Getter
@Setter
@Entity
@Table(name = "txn_deposit")
public class TxnDeposit {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  @ManyToOne(optional = true)
  private MdBranch branch;
  //    @ManyToOne(optional = true)
//    private MdDepositType depositType;
  private String mainType;
  private String depositType;
  @ColumnDefault(value = "0")
  private int depositoutcur;
  @ColumnDefault(value = "0")
  private int depositoutstgrowth;
  @ColumnDefault(value = "0")
  private int depositoutcurtotalestimated;
  @ColumnDefault(value = "0")
  private int firstqtr;
  @ColumnDefault(value = "0")
  private int secondqtr;
  @ColumnDefault(value = "0")
  private int thirdqtr;
  @ColumnDefault(value = "0")
  private int fourthqtr;
  @ColumnDefault(value = "0")
  private int annualgrowth;
  @ColumnDefault(value = "0")
  private int annualexpecteddepositestimated;

  private double interestrate;
  private String category;

}
