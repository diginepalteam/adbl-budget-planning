package com.budgetplan.app.txn.controller;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.auth.service.UserService;
import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.CurrentYearRemainingCommit;
import com.budgetplan.app.txn.service.CurrentYearRemainingCommitService;
import com.budgetplan.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("impl/api/v1")
public class CurrentYearRemainingCommitController {
  private CurrentYearRemainingCommitService currentYearRemainingCommitService;
  private GenericCrudService<CurrentYearRemainingCommit, Integer> currentYearRemainingCommitIntegerGenericCrudService;
  private UserService userService;

  @Autowired
  public void setCurrentYearRemainingCommitIntegerGenericCrudService(GenericCrudService<CurrentYearRemainingCommit, Integer> currentYearRemainingCommitIntegerGenericCrudService) {
    this.currentYearRemainingCommitIntegerGenericCrudService = currentYearRemainingCommitIntegerGenericCrudService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setCurrentYearRemainingCommitService(CurrentYearRemainingCommitService currentYearRemainingCommitService) {
    this.currentYearRemainingCommitService = currentYearRemainingCommitService;
  }

  @GetMapping("commits")
  public ResponseEntity<?> getCommits() {
    Optional<String> branchCode = userService.getTokenUserBranchCode();
    if (branchCode.isPresent()) {
      return new ApiResponse()
        .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), currentYearRemainingCommitService.getByBranchCode(branchCode.get()));
    } else {
      return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), new ArrayList<>());
    }
  }

  @PostMapping("commits")
  public ResponseEntity<?> saveCommits(@RequestBody CurrentYearRemainingCommit currentYearRemainingCommit) {
    Optional<String> branchCode = userService.getTokenUserBranchCode();
    if (branchCode.isPresent()) {
      List<CurrentYearRemainingCommit> currentYearRemainingCommitList = currentYearRemainingCommitService.getByBranchCode(branchCode.get());
      if (!currentYearRemainingCommitList.isEmpty()) {
        CurrentYearRemainingCommit existingCurrentYearCommit = currentYearRemainingCommitList.get(0);
        CustomBeanUtils.copyNonNullAndNonZeroProperties(currentYearRemainingCommit, existingCurrentYearCommit);
        currentYearRemainingCommitIntegerGenericCrudService.save(existingCurrentYearCommit);
      } else {
        MdBranch branch = new MdBranch();
        branch.setBranchCode(branchCode.get());
        currentYearRemainingCommit.setBranch(branch);
        currentYearRemainingCommitIntegerGenericCrudService.save(currentYearRemainingCommit);
      }
      return new ApiResponse()
        .getSuccessResponse(HttpStatus.OK.getReasonPhrase(), currentYearRemainingCommit);
    }
    return new ApiResponse()
      .getFailureResponse(HttpStatus.BAD_REQUEST.getReasonPhrase());
  }
}
