package com.budgetplan.app.txn.controller;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.auth.service.UserService;
import com.budgetplan.app.txn.service.TxnBalanceSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/impl/api/v1")
public class TxnBalanceSheetController {
  private TxnBalanceSheetService txnBalanceSheetService;
  private UserService userService;

  @Autowired
  public void setTxnBalanceSheetService(TxnBalanceSheetService txnBalanceSheetService) {
    this.txnBalanceSheetService = txnBalanceSheetService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/txn/balancesheets")
  public ResponseEntity<?> getBalanceSheets() {
    Optional<String> branchCode = userService.getTokenUserBranchCode();
    if (branchCode.isPresent()) {
      return new ApiResponse()
        .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnBalanceSheetService.getByBranchCode(branchCode.get()));
    } else {
      return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), new ArrayList<>());
    }
  }
}
