package com.budgetplan.app.txn.controller;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.auth.service.UserService;
import com.budgetplan.app.txn.service.TxnInterestExpenseProjectionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class TxnInterestExpenseProjectionController {
    private UserService userService;
    private TxnInterestExpenseProjectionService txnInterestExpenseProjectionService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTxnInterestExpenseProjectionService(TxnInterestExpenseProjectionService txnInterestExpenseProjectionService) {
        this.txnInterestExpenseProjectionService = txnInterestExpenseProjectionService;
    }

    @GetMapping("txn/interestexpenseprojection")
    public ResponseEntity<?> getTxnLoans() {
        Optional<String> branchCode = userService.getTokenUserBranchCode();
        if (branchCode.isPresent()) {
            return new ApiResponse()
                    .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnInterestExpenseProjectionService.findByBranchCode(branchCode.get()));
        } else {
            return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), new ArrayList<>());
        }
    }
}
