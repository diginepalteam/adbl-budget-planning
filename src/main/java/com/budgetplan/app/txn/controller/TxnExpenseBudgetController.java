package com.budgetplan.app.txn.controller;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.auth.service.UserService;
import com.budgetplan.app.integration.MisIntegrationService;
import com.budgetplan.app.integration.responses.ModelIncomeAndExpenses;
import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.md.entity.MdExpenseCode;
import com.budgetplan.app.md.entity.MdExpenseMain;
import com.budgetplan.app.md.repository.MdExpenseCodeRepository;
import com.budgetplan.app.md.repository.MdExpenseMainRepository;
import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnExpenseBudget;
import com.budgetplan.app.txn.service.TxnExpenseBudgetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class TxnExpenseBudgetController {
  private UserService userService;
  private GenericCrudService<TxnExpenseBudget, Integer> genericCrudService;
  private TxnExpenseBudgetService txnExpenseBudgetService;
  private MisIntegrationService misIntegrationService;
  @Autowired
  private MdExpenseMainRepository mdExpenseMainRepository;
  @Autowired
  private MdExpenseCodeRepository mdExpenseCodeRepository;

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setGenericCrudService(GenericCrudService<TxnExpenseBudget, Integer> genericCrudService) {
    this.genericCrudService = genericCrudService;
  }

  @Autowired
  public void setTxnExpenseBudgetService(TxnExpenseBudgetService txnExpenseBudgetService) {
    this.txnExpenseBudgetService = txnExpenseBudgetService;
  }

  @Autowired
  public void setMisIntegrationService(MisIntegrationService misIntegrationService) {
    this.misIntegrationService = misIntegrationService;
  }

  @GetMapping("txn/expense/budget")
  public ResponseEntity<?> getExpenseBudgets() {
    Optional<String> branchCode = userService.getTokenUserBranchCode();
    if (branchCode.isPresent()) {
      return new ApiResponse()
        .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnExpenseBudgetService.getByBranchCode(branchCode.get()));
    } else {
      return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), new ArrayList<>());
    }
  }

  @GetMapping("txn/expense/mains")
  public ResponseEntity<?> getExpenseMains(){
    return new ResponseEntity<>(mdExpenseMainRepository.findAll(), HttpStatus.OK);
  }

  @GetMapping("txn/expense/codes")
  public ResponseEntity<?> getExpenseCodes(){
    return new ResponseEntity<>(mdExpenseCodeRepository.findAll(), HttpStatus.OK);
  }

  @GetMapping("txn/expense/budget/{id}")
  public ResponseEntity<?> getExpenseBudgetById(@PathVariable int id) {
    Optional<TxnExpenseBudget> txnExpenseBudgetOptional = genericCrudService.getOne(id);
    if (txnExpenseBudgetOptional.isPresent()) {
      return new ApiResponse()
        .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnExpenseBudgetOptional.get());
    } else {
      return new ApiResponse()
        .getSuccessResponse(HttpStatus.NOT_FOUND.getReasonPhrase(), new ArrayList<>());
    }
  }

  @PostMapping("txn/expense/budget/{id}")
  public ResponseEntity<?> updateExpenseBudgetById(@PathVariable int id, @RequestBody TxnExpenseBudget txnExpenseBudget) {
    Optional<TxnExpenseBudget> txnExpenseBudgetOptional = genericCrudService.update(txnExpenseBudget, id);
    if (txnExpenseBudgetOptional.isPresent()) {
      return new ApiResponse()
        .getSuccessResponse(HttpStatus.OK.getReasonPhrase(), txnExpenseBudgetOptional.get());
    } else {
      return new ApiResponse()
        .getFailureResponse(HttpStatus.BAD_REQUEST.getReasonPhrase());

    }
  }

  @GetMapping("txn/expense/budget/syncs/mis")
  public ResponseEntity<?> syncDataFromMis() {
    List<TxnExpenseBudget> txnExpenseBudgets = new ArrayList<>();
    List<ModelIncomeAndExpenses> expenses = misIntegrationService.getExpenseBudgets();
    expenses.forEach(modelIncomeAndExpenses -> {
      MdBranch branch = new MdBranch();
      branch.setBranchCode(modelIncomeAndExpenses.getBranch_branchcode());

      MdExpenseCode expenseCode = new MdExpenseCode();
      try {
        expenseCode.setCode(Integer.parseInt(modelIncomeAndExpenses.getLineno()));
      } catch (NumberFormatException e) {
        log.warn("Number format exception syncing TxnExpenseBudget from Mis");
      }
      TxnExpenseBudget txnExpenseBudget = new TxnExpenseBudget();
      txnExpenseBudget.setBranch(branch);
      txnExpenseBudget.setActualexpensethisyear(modelIncomeAndExpenses.getAmount());
      txnExpenseBudget.setActualexpenselastyear(modelIncomeAndExpenses.getAmountLastYr());
      txnExpenseBudget.setExpenseCode(expenseCode);

      MdExpenseMain mdExpenseMain = new MdExpenseMain();
      mdExpenseMain.setCode(modelIncomeAndExpenses.getMaincode());
      txnExpenseBudget.setExpenseCode(expenseCode);
      txnExpenseBudgets.add(txnExpenseBudget);
    });
    if (!txnExpenseBudgets.isEmpty()) {
      genericCrudService.saveAll(txnExpenseBudgets);
      return new ApiResponse()
        .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnExpenseBudgets);
    }
    return new ApiResponse()
      .getFailureResponse(HttpStatus.NOT_FOUND.getReasonPhrase());
  }
}
