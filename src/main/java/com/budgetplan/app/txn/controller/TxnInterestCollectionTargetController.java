package com.budgetplan.app.txn.controller;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.auth.service.UserService;
import com.budgetplan.app.integration.MisIntegrationService;
import com.budgetplan.app.integration.responses.MisLoanInterestTarget;
import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnInterestCollectionTarget;
import com.budgetplan.app.txn.entity.TxnLoan;
import com.budgetplan.app.txn.service.TxnInterestCollectionTargetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("impl/api/v1")
public class TxnInterestCollectionTargetController {
  private UserService userService;
  private TxnInterestCollectionTargetService txnInterestCollectionTargetService;
  private GenericCrudService<TxnInterestCollectionTarget, Integer> txnInterestCollectionTargetIntegerGenericCrudService;
  private GenericCrudService<TxnLoan, Integer> txnLoanIntegerGenericCrudService;
  private MisIntegrationService misIntegrationService;

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setTxnInterestCollectionTargetService(TxnInterestCollectionTargetService txnInterestCollectionTargetService) {
    this.txnInterestCollectionTargetService = txnInterestCollectionTargetService;
  }

  @Autowired
  public void setTxnInterestCollectionTargetIntegerGenericCrudService(GenericCrudService<TxnInterestCollectionTarget, Integer> txnInterestCollectionTargetIntegerGenericCrudService) {
    this.txnInterestCollectionTargetIntegerGenericCrudService = txnInterestCollectionTargetIntegerGenericCrudService;
  }

  @Autowired
  public void setTxnLoanIntegerGenericCrudService(GenericCrudService<TxnLoan, Integer> txnLoanIntegerGenericCrudService) {
    this.txnLoanIntegerGenericCrudService = txnLoanIntegerGenericCrudService;
  }

  @Autowired
  public void setMisIntegrationService(MisIntegrationService misIntegrationService) {
    this.misIntegrationService = misIntegrationService;
  }

  @GetMapping("/txn/interest/collection/target")
  public ResponseEntity<?> getInterestLoanCollectionTarget() {
    Optional<String> branchCode = userService.getTokenUserBranchCode();
    if (branchCode.isPresent()) {
      return new ApiResponse()
        .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnInterestCollectionTargetService.getByBranchCode(branchCode.get()));
    } else {
      return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), new ArrayList<>());
    }
  }

  @GetMapping("/txn/interest/collection/target/sync")
  public ResponseEntity<?> syncInterestLoanCollectionTarget() {
    if (txnInterestCollectionTargetIntegerGenericCrudService.getAll().isEmpty()) {
    List<MisLoanInterestTarget> misLoanInterestTargets = misIntegrationService.getLoanInterestTargets();

    List<TxnLoan> txnLoanList = txnLoanIntegerGenericCrudService.getAll();

    List<TxnInterestCollectionTarget> txnInterestCollectionTargets = new ArrayList<>();
    misLoanInterestTargets.forEach(misLoanInterestTarget -> {

      List<TxnLoan> filteredLoans = txnLoanList.stream().filter(txnLoan -> {
        return null != txnLoan.getBranch()
          && txnLoan.getBranch().getBranchCode()
          .equals(misLoanInterestTarget.getBranch_branchcode());
      }).collect(Collectors.toList());

      if (!filteredLoans.isEmpty()) {
        int sum = 0;
        switch (misLoanInterestTarget.getIncomeid()) {
          case 1:
            sum = filteredLoans.stream().mapToInt(TxnLoan::getAnnualIntTarget).sum();
            break;
          case 2:
            sum = filteredLoans.stream().mapToInt(TxnLoan::getCollectionNonPerformingIntTarget).sum();
            break;
          default:
        }

        TxnInterestCollectionTarget txnInterestCollectionTarget = new TxnInterestCollectionTarget();

        MdBranch branch = new MdBranch();
        branch.setBranchCode(misLoanInterestTarget.getBranch_branchcode());
        txnInterestCollectionTarget.setBranch(branch);
        txnInterestCollectionTarget.setTotal(sum);

        txnInterestCollectionTarget.setShrawan(
          (int) Math.round(sum * 0.07)
        );
        txnInterestCollectionTarget.setBhadra(
          (int) Math.round(sum * 0.07)
        );
        txnInterestCollectionTarget.setAhwin(
          (int) Math.round(sum * 0.07)
        );

        txnInterestCollectionTarget.setKartik(
          (int) Math.round(sum * 0.08)
        );
        txnInterestCollectionTarget.setMangsir(
          (int) Math.round(sum * 0.08)
        );
        txnInterestCollectionTarget.setPaush(
          (int) Math.round(sum * 0.08)
        );

        txnInterestCollectionTarget.setMagh(
          (int) Math.round(sum * 0.0875)
        );
        txnInterestCollectionTarget.setFalgun(
          (int) Math.round(sum * 0.0875)
        );
        txnInterestCollectionTarget.setChaitra(
          (int) Math.round(sum * 0.0875)
        );

        txnInterestCollectionTarget.setBaisekh(
          (int) Math.round(sum * 0.095)
        );
        txnInterestCollectionTarget.setJesth(
          (int) Math.round(sum * 0.095)
        );

        int totalBeforeAshadh = txnInterestCollectionTarget.getBaisekh() + txnInterestCollectionTarget.getJesth() + txnInterestCollectionTarget.getShrawan()
          + txnInterestCollectionTarget.getBhadra() + txnInterestCollectionTarget.getAhwin() + txnInterestCollectionTarget.getKartik() + txnInterestCollectionTarget.getMangsir()
          + txnInterestCollectionTarget.getPaush() + txnInterestCollectionTarget.getMagh() + txnInterestCollectionTarget.getFalgun()
          + txnInterestCollectionTarget.getChaitra();

        txnInterestCollectionTarget.setAshadh(
          sum - totalBeforeAshadh
        );

        txnInterestCollectionTarget.setHead(misLoanInterestTarget.getIncomehead());

        txnInterestCollectionTargets.add(txnInterestCollectionTarget);
      }
    });


      txnInterestCollectionTargetIntegerGenericCrudService.saveAll(txnInterestCollectionTargets);
      return new ApiResponse()
        .getSuccessResponse(HttpStatus.OK.getReasonPhrase(), null);
    } else {
      return new ApiResponse()
        .getFailureResponse("Interest Loan Target already exists!");
    }

  }
}
