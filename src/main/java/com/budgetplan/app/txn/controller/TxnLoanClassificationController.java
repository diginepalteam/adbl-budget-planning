package com.budgetplan.app.txn.controller;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.auth.service.UserService;
import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnLoanClassification;
import com.budgetplan.app.txn.service.TxnLoanClassificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class TxnLoanClassificationController {
    private TxnLoanClassificationService txnLoanClassificationService;
    private UserService userService;
    private GenericCrudService<TxnLoanClassification, Integer> genericCrudService;

    @Autowired
    public void setTxnLoanClassificationService(TxnLoanClassificationService txnLoanClassificationService) {
        this.txnLoanClassificationService = txnLoanClassificationService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setGenericCrudService(GenericCrudService<TxnLoanClassification, Integer> genericCrudService) {
        this.genericCrudService = genericCrudService;
    }

    @GetMapping("txn/loan/classification")
    public ResponseEntity<?> getTxnLoans() {
        Optional<String> branchCode = userService.getTokenUserBranchCode();
        if (branchCode.isPresent()) {
            return new ApiResponse()
                    .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnLoanClassificationService.getByBranchCode(branchCode.get()));
        } else {
            return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), new ArrayList<>());
        }
    }

    @PostMapping("txn/loan/classification/{id}")
    public ResponseEntity<?> updateTxnLoanClassification(@PathVariable int id, @RequestBody TxnLoanClassification txnLoanClassification) {
        Optional<TxnLoanClassification> txnLoanClassificationOptional = genericCrudService.update(txnLoanClassification, id);
        if (txnLoanClassificationOptional.isPresent()) {
            return new ApiResponse()
                    .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnLoanClassificationOptional.get());
        }
        return new ApiResponse().getFailureResponse(HttpStatus.BAD_REQUEST.getReasonPhrase());
    }


}
