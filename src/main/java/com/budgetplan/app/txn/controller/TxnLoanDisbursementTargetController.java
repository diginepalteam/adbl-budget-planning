package com.budgetplan.app.txn.controller;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.auth.service.UserService;
import com.budgetplan.app.txn.service.TxnLoanDisbursementTargetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("impl/api/v1")
public class TxnLoanDisbursementTargetController {
    private UserService userService;
    private TxnLoanDisbursementTargetService txnLoanDisbursementTargetService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTxnLoanDisbursementTargetService(TxnLoanDisbursementTargetService txnLoanDisbursementTargetService) {
        this.txnLoanDisbursementTargetService = txnLoanDisbursementTargetService;
    }

    @GetMapping("txn/loan/disbursement")
    public ResponseEntity<?> getTxnLoans() {
        Optional<String> branchCode = userService.getTokenUserBranchCode();
        if (branchCode.isPresent()) {
            return new ApiResponse()
                    .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnLoanDisbursementTargetService.getByBranchCode(branchCode.get()));
        } else {
            return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), new ArrayList<>());
        }
    }
}
