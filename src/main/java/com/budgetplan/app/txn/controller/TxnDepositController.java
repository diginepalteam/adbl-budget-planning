package com.budgetplan.app.txn.controller;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.auth.service.UserService;
import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnDeposit;
import com.budgetplan.app.txn.service.TxnDepositService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class TxnDepositController {
    private UserService userService;
    private TxnDepositService txnDepositService;
    private GenericCrudService<TxnDeposit, Integer> genericCrudService;


    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTxnDepositService(TxnDepositService txnDepositService) {
        this.txnDepositService = txnDepositService;
    }

    @Autowired
    public void setGenericCrudService(GenericCrudService<TxnDeposit, Integer> genericCrudService) {
        this.genericCrudService = genericCrudService;
    }

    @GetMapping("txn/deposits")
    public ResponseEntity<?> getTxnDeposits() {
        Optional<String> branchCode = userService.getTokenUserBranchCode();
        if (branchCode.isPresent()) {
            return new ApiResponse()
                    .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnDepositService.getByBranchCode(branchCode.get()));
        } else {
            return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), new ArrayList<>());
        }
    }

    @PostMapping("txn/deposits/{id}")
    public ResponseEntity<?> updateTxnLoans(@PathVariable int id, @RequestBody TxnDeposit txnDeposit) {
        Optional<TxnDeposit> txnDepositOptional = genericCrudService.update(txnDeposit, id);
        if (txnDepositOptional.isPresent()) {
            return new ApiResponse()
                    .getSuccessResponse(HttpStatus.OK.getReasonPhrase(), txnDepositOptional.get());
        } else {
            return new ApiResponse()
                    .getFailureResponse(HttpStatus.BAD_REQUEST.getReasonPhrase());

        }
    }
}
