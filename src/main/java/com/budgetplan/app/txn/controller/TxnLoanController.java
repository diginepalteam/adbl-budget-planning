package com.budgetplan.app.txn.controller;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.auth.service.UserService;
import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnLoan;
import com.budgetplan.app.txn.service.TxnLoanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class TxnLoanController {
    private TxnLoanService txnLoanService;
    private UserService userService;
    private GenericCrudService<TxnLoan, Integer> genericCrudService;

    @Autowired
    public void setTxnLoanService(TxnLoanService txnLoanService) {
        this.txnLoanService = txnLoanService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setGenericCrudService(GenericCrudService<TxnLoan, Integer> genericCrudService) {
        this.genericCrudService = genericCrudService;
    }

    @GetMapping("txn/loan")
    public ResponseEntity<?> getTxnLoans() {
        Optional<String> branchCode = userService.getTokenUserBranchCode();
        if (branchCode.isPresent()) {
            return new ApiResponse()
                    .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnLoanService.getByBranchCode(branchCode.get()));
        } else {
            return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), new ArrayList<>());
        }
    }

    @GetMapping("txn/loan/{id}")
    public ResponseEntity<?> getTxnLoans(@PathVariable int id) {
        Optional<TxnLoan> txnLoanOptional = genericCrudService.getOne(id);
        if (txnLoanOptional.isPresent()) {
            return new ApiResponse()
                    .getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), txnLoanOptional.get());
        } else {
            return new ApiResponse()
                    .getSuccessResponse(HttpStatus.NOT_FOUND.getReasonPhrase(), new ArrayList<>());
        }
    }

    @PostMapping("txn/loan/{id}")
    public ResponseEntity<?> updateTxnLoans(@PathVariable int id, @RequestBody TxnLoan txnLoan) {
        Optional<TxnLoan> txnLoanOptional = genericCrudService.update(txnLoan, id);
        if (txnLoanOptional.isPresent()) {
            return new ApiResponse()
                    .getSuccessResponse(HttpStatus.OK.getReasonPhrase(), txnLoanOptional.get());
        } else {
            return new ApiResponse()
                    .getFailureResponse(HttpStatus.BAD_REQUEST.getReasonPhrase());

        }
    }
}
