package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.CurrentYearRemainingCommit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CurrentYearRemainingCommitRepository extends JpaRepository<CurrentYearRemainingCommit, Integer> {
  List<CurrentYearRemainingCommit> findAllByBranchBranchCode(String branchCode);
}
