package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.TxnLoanDueCollectionTarget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TxnDueLoanCollectionTargetRepository extends JpaRepository<TxnLoanDueCollectionTarget, Integer> {
    List<TxnLoanDueCollectionTarget> findAllByBranchBranchCode(String branchCode);

    Optional<TxnLoanDueCollectionTarget> findFirstByBranchBranchCodeAndLoanTypeLoanProductCode(String branch, String loanProductCode);

}
