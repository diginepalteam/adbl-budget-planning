package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.TxnLoanDisbursementTarget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TxnLoanDisbursementTargetRepository extends JpaRepository<TxnLoanDisbursementTarget, Integer> {
    List<TxnLoanDisbursementTarget> findAllByBranchBranchCode(String branchCode);
    Optional<TxnLoanDisbursementTarget> findFirstByBranchBranchCodeAndLoanTypeLoanProductCode(String branch, String loanProductCode);
}
