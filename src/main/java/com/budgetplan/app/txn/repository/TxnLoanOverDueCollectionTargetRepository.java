package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.TxnLoanOverDueCollectionTarget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TxnLoanOverDueCollectionTargetRepository extends JpaRepository<TxnLoanOverDueCollectionTarget, Integer> {
    List<TxnLoanOverDueCollectionTarget> findAllByBranchBranchCode(String branchCode);

    Optional<TxnLoanOverDueCollectionTarget> findFirstByBranchBranchCodeAndLoanTypeLoanProductCode(String branch, String loanProductCode);

}
