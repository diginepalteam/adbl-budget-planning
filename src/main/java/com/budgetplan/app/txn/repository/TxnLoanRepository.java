package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.TxnLoan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TxnLoanRepository extends JpaRepository<TxnLoan, Integer> {
    List<TxnLoan> findAllByBranchBranchCode(String branchCode);

    Optional<TxnLoan> findFirstByBranchBranchCodeAndLoanTypeLoanProductCode(String branchCode, String loanType);
}
