package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.TxnLoanClassification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TxnLoanClassificationRepository extends JpaRepository<TxnLoanClassification, Integer> {
    List<TxnLoanClassification> findAllByBranchBranchCode(String branchCode);
}
