package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.TxnBalanceSheet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TxnBalanceSheetRepository extends JpaRepository<TxnBalanceSheet, Integer> {
  List<TxnBalanceSheet> findAllByBranchBranchCode(String branchCode);
}
