package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.TxnExpenseBudget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TxnExpenseBudgetRepository extends JpaRepository<TxnExpenseBudget, Integer> {
    List<TxnExpenseBudget> findAllByBranchBranchCode(String branchCode);
    int countAllByBranchBranchCode(String branchCode);
}
