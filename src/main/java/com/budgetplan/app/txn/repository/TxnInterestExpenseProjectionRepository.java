package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.TxnInterestExpenseProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TxnInterestExpenseProjectionRepository extends JpaRepository<TxnInterestExpenseProjection, Integer> {
    Optional<TxnInterestExpenseProjection> findFirstByTxnDepositId(int depositId);
    List<TxnInterestExpenseProjection> findAllByBranchBranchCode(String branchCode);
}
