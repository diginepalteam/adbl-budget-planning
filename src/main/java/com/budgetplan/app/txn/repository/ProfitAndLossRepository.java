package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.ProfitAndLoss;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProfitAndLossRepository extends JpaRepository<ProfitAndLoss, Integer> {
  List<ProfitAndLoss> findAllByBranchBranchCode(String branchCode);
}
