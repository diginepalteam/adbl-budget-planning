package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.TxnLoanGrowthTarget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TxnLoanGrowthTargetRepository extends JpaRepository<TxnLoanGrowthTarget, Integer> {
    List<TxnLoanGrowthTarget> findAllByBranchBranchCode(String branchCode);

    Optional<TxnLoanGrowthTarget> findFirstByBranchBranchCodeAndLoanTypeLoanProductCode(String branch, String loanProductCode);

}
