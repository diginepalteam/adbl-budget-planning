package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.TxnDeposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TxnDepositRepository extends JpaRepository<TxnDeposit, Integer> {
    List<TxnDeposit> findAllByBranchBranchCode(String branchCode);
}
