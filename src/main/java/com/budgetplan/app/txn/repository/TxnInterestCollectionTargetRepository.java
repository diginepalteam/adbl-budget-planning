package com.budgetplan.app.txn.repository;

import com.budgetplan.app.txn.entity.TxnInterestCollectionTarget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TxnInterestCollectionTargetRepository extends JpaRepository<TxnInterestCollectionTarget, Integer> {
    List<TxnInterestCollectionTarget> findAllByBranchBranchCode(String branchCode);

}
