package com.budgetplan.app.txn.listener;

import com.budgetplan.app.txn.entity.TxnDeposit;
import com.budgetplan.app.txn.entity.TxnInterestExpenseProjection;
import com.budgetplan.app.txn.repository.TxnInterestExpenseProjectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Optional;

@Component
public class TxnDepositEntityListener {
    private TxnInterestExpenseProjectionRepository txnInterestExpenseProjectionRepository;

    @Autowired
    public void setTxnInterestExpenseProjectionRepository(TxnInterestExpenseProjectionRepository txnInterestExpenseProjectionRepository) {
        this.txnInterestExpenseProjectionRepository = txnInterestExpenseProjectionRepository;
    }

    @PreUpdate
    @PrePersist
    public void prePostPersist(TxnDeposit txnDeposit) {
        this.putLogic(txnDeposit);
    }

    private void putLogic(TxnDeposit txnDeposit) {
        int annualgrowth = txnDeposit.getFirstqtr() + txnDeposit.getSecondqtr() + txnDeposit.getThirdqtr() + txnDeposit.getFourthqtr();
        txnDeposit.setAnnualgrowth(annualgrowth);

        int depositoutcurtotalestimated = txnDeposit.getDepositoutcur() + txnDeposit.getDepositoutstgrowth();
        txnDeposit.setDepositoutcurtotalestimated(depositoutcurtotalestimated);

        int annualexpecteddepositestimated = annualgrowth + depositoutcurtotalestimated;
        txnDeposit.setAnnualexpecteddepositestimated(annualexpecteddepositestimated);
    }

    @PostUpdate
    @PostPersist
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void postPersist(TxnDeposit txnDeposit) {
        TxnInterestExpenseProjection txnInterestExpenseProjection = this.getTxnInterestExpenseProjection(txnDeposit);

        Optional<TxnInterestExpenseProjection> txnInterestExpenseProjectionOptional = txnInterestExpenseProjectionRepository.findFirstByTxnDepositId(txnDeposit.getId());

        txnInterestExpenseProjectionOptional.ifPresent(interestExpenseProjection -> {
            txnInterestExpenseProjection.setId(interestExpenseProjection.getId());
            txnInterestExpenseProjectionRepository.delete(interestExpenseProjection);
        });
        txnInterestExpenseProjectionRepository.save(txnInterestExpenseProjection);
    }

    private TxnInterestExpenseProjection getTxnInterestExpenseProjection(TxnDeposit txnDeposit) {
        TxnInterestExpenseProjection txnInterestExpenseProjection = new TxnInterestExpenseProjection();
        txnInterestExpenseProjection.setCategory(txnDeposit.getCategory());

        txnInterestExpenseProjection.setTxnDepositId(txnDeposit.getId());

        txnInterestExpenseProjection.setMainType(txnDeposit.getMainType());
        txnInterestExpenseProjection.setBranch(txnDeposit.getBranch());
        txnInterestExpenseProjection.setDepositType(txnDeposit.getDepositType());

        txnInterestExpenseProjection.setDepositoutcur(txnDeposit.getDepositoutcur());
        txnInterestExpenseProjection.setDepositoutcurtotalestimated(txnDeposit.getDepositoutcurtotalestimated());

        int firstqtrnextprojected = txnDeposit.getFirstqtr() + txnDeposit.getDepositoutcurtotalestimated();
        txnInterestExpenseProjection.setFirstqtrnextprojected(firstqtrnextprojected);

        int secondqtrnextprojected = firstqtrnextprojected + txnDeposit.getSecondqtr();
        txnInterestExpenseProjection.setSecondqtrnextprojected(secondqtrnextprojected);

        int thirdqtrnextprojected = secondqtrnextprojected + txnDeposit.getThirdqtr();
        txnInterestExpenseProjection.setThirdqtrnextprojected(thirdqtrnextprojected);

        int fourthqtrnextprojected = thirdqtrnextprojected + txnDeposit.getFourthqtr();
        txnInterestExpenseProjection.setFourthqtrnextprojected(fourthqtrnextprojected);

        txnInterestExpenseProjection.setFirstqtrnextaverageoutstanding(firstqtrnextprojected / 2);
        txnInterestExpenseProjection.setSecondqtrnextaverageoutstanding(secondqtrnextprojected / 2);
        txnInterestExpenseProjection.setThirdqtrnextaverageoutstanding(thirdqtrnextprojected / 2);
        txnInterestExpenseProjection.setFourthqtrnextaverageoutstanding(fourthqtrnextprojected / 2);

        double interestRate = txnDeposit.getInterestrate();
        txnInterestExpenseProjection.setInterestrate(interestRate);

        int firstqtrnextinterestexp = (int) Math.round(
                (
                        txnInterestExpenseProjection.getFirstqtrnextaverageoutstanding() * (interestRate / 100)
                ) / 4
        );
        txnInterestExpenseProjection.setFirstqtrnextinterestexp(firstqtrnextinterestexp);

        int secondqtrnextinterestexp = (int) Math.round(
                (
                        txnInterestExpenseProjection.getSecondqtrnextaverageoutstanding() * (interestRate / 100)
                ) / 4
        );
        txnInterestExpenseProjection.setSecondqtrnextinterestexp(secondqtrnextinterestexp);

        int thirdqtrnextinterestexp = (int) Math.round(
                (
                        txnInterestExpenseProjection.getThirdqtrnextaverageoutstanding() * (interestRate / 100)
                ) / 4
        );
        txnInterestExpenseProjection.setThirdqtrnextinterestexp(thirdqtrnextinterestexp);

        int fourthqtrnextinterestexp = (int) Math.round(
                (
                        txnInterestExpenseProjection.getFourthqtrnextaverageoutstanding() * (interestRate / 100)
                ) / 4
        );
        txnInterestExpenseProjection.setFourthqtrnextinterestexp(fourthqtrnextinterestexp);

        txnInterestExpenseProjection.setTotalnextinterestexp(firstqtrnextinterestexp + secondqtrnextinterestexp + thirdqtrnextinterestexp + fourthqtrnextinterestexp);

        return txnInterestExpenseProjection;
    }
}
