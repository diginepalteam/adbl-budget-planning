package com.budgetplan.app.txn.listener;

import com.budgetplan.app.txn.entity.*;
import com.budgetplan.app.txn.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Optional;

@Component
public class TxnLoanEntityListener {
  private TxnLoanDisbursementTargetRepository txnLoanDisbursementTargetRepository;
  private TxnLoanOverDueCollectionTargetRepository txnLoanOverDueCollectionTargetRepository;
  private TxnDueLoanCollectionTargetRepository txnDueLoanCollectionTargetRepository;
  private TxnLoanGrowthTargetRepository txnLoanGrowthTargetRepository;
  private TxnLoanRepository txnLoanRepository;

  @Autowired
  public void setTxnLoanRepository(TxnLoanRepository txnLoanRepository) {
    this.txnLoanRepository = txnLoanRepository;
  }

  @Autowired
  public void setTxnLoanOverDueCollectionTargetRepository(TxnLoanOverDueCollectionTargetRepository txnLoanOverDueCollectionTargetRepository) {
    this.txnLoanOverDueCollectionTargetRepository = txnLoanOverDueCollectionTargetRepository;
  }

  @Autowired
  public void setTxnDueLoanCollectionTargetRepository(TxnDueLoanCollectionTargetRepository txnDueLoanCollectionTargetRepository) {
    this.txnDueLoanCollectionTargetRepository = txnDueLoanCollectionTargetRepository;
  }

  @Autowired
  public void setTxnLoanDisbursementTargetRepository(TxnLoanDisbursementTargetRepository txnLoanDisbursementTargetRepository) {
    this.txnLoanDisbursementTargetRepository = txnLoanDisbursementTargetRepository;
  }

  @Autowired
  public void setTxnLoanGrowthTargetRepository(TxnLoanGrowthTargetRepository txnLoanGrowthTargetRepository) {
    this.txnLoanGrowthTargetRepository = txnLoanGrowthTargetRepository;
  }

  @PreUpdate
  @PrePersist
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void prePersistUpdate(TxnLoan txnLoan) {
    if (txnLoan.getId() > 0) {
      Optional<TxnLoan> txnLoanOptional = txnLoanRepository.findById(txnLoan.getId());
      if (txnLoanOptional.isPresent()) {
        Optional<TxnLoanDisbursementTarget> txnLoanDisbursementTargetOptional = txnLoanDisbursementTargetRepository.findFirstByBranchBranchCodeAndLoanTypeLoanProductCode(txnLoanOptional.get().getBranch().getBranchCode(), txnLoanOptional.get().getLoanType().getLoanProductCode());
        txnLoanDisbursementTargetOptional.ifPresent(txnLoanDisbursementTarget -> txnLoanDisbursementTargetRepository.delete(txnLoanDisbursementTarget));
      }
    }

    TxnLoanDisbursementTarget txnLoanDisbursementTarget = this.getTxnLoanDisbursementTarget(txnLoan);
    txnLoanDisbursementTargetRepository.save(txnLoanDisbursementTarget);

    this.putLogic(txnLoan, txnLoanDisbursementTarget);
  }

  @PostUpdate
  @PostPersist
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void postPersistUpdate(TxnLoan txnLoan) {
    if (txnLoan.getId() > 0) {
      Optional<TxnLoan> txnLoanOptional = txnLoanRepository.findById(txnLoan.getId());
      if (txnLoanOptional.isPresent()) {
        Optional<TxnLoanOverDueCollectionTarget> txnLoanOverDueCollectionTargetOptional = txnLoanOverDueCollectionTargetRepository.findFirstByBranchBranchCodeAndLoanTypeLoanProductCode(txnLoanOptional.get().getBranch().getBranchCode(), txnLoanOptional.get().getLoanType().getLoanProductCode());
        txnLoanOverDueCollectionTargetOptional.ifPresent(txnLoanOverDueCollectionTarget -> txnLoanOverDueCollectionTargetRepository.delete(txnLoanOverDueCollectionTarget));

        Optional<TxnLoanDueCollectionTarget> txnLoanDueCollectionTargetOptional = txnDueLoanCollectionTargetRepository.findFirstByBranchBranchCodeAndLoanTypeLoanProductCode(txnLoanOptional.get().getBranch().getBranchCode(), txnLoanOptional.get().getLoanType().getLoanProductCode());
        txnLoanDueCollectionTargetOptional.ifPresent(txnLoanDisbursementTarget -> txnDueLoanCollectionTargetRepository.delete(txnLoanDisbursementTarget));

        Optional<TxnLoanGrowthTarget> txnLoanGrowthTargetOptional = txnLoanGrowthTargetRepository.findFirstByBranchBranchCodeAndLoanTypeLoanProductCode(txnLoanOptional.get().getBranch().getBranchCode(), txnLoanOptional.get().getLoanType().getLoanProductCode());
        txnLoanGrowthTargetOptional.ifPresent(txnLoanGrowthTarget -> txnLoanGrowthTargetRepository.delete(txnLoanGrowthTarget));
      }
    }

  }





  private void putLogic(TxnLoan txnLoan, TxnLoanDisbursementTarget txnLoanDisbursementTarget) {
    txnLoan.setInterestAdditionCurEstimated(
      (int) Math.round(
        (txnLoan.getTotalLoanOutCur() * 0.09) / 4));

    txnLoan.setInterestCollectionCurEstimated(
      (int) Math.round(
        txnLoan.getInterestAdditionCurEstimated()
          + (txnLoan.getNonPerformingInterestOutCur() * 0.25)));
    //14
    txnLoan.setLoanTotalCurEstimated(
      txnLoan.getTotalLoanOutCur() + txnLoan.getDisbursementCurEstimated() - txnLoan.getCollectionPrCurEstimated()
    );
    //12
    txnLoan.setLoanWcOutCurEstimated(
      (int) Math.round(txnLoan.getLoanTotalCurEstimated() * 0.88)
    );
    //13
    txnLoan.setLoanTlOutCurEstimated(
      txnLoan.getLoanTotalCurEstimated() - txnLoan.getLoanWcOutCurEstimated()
    );
    //17
    txnLoan.setNonPerformingOnTOutCurEstimated(
      txnLoan.getNonPerformingInterestOutCur() + txnLoan.getInterestAdditionCurEstimated() - txnLoan.getInterestCollectionCurEstimated()
    );
    //18
    txnLoan.setDisburseLoanWcTarget(
      txnLoanDisbursementTarget.getSumqtrshort()
    );
    //19
    txnLoan.setDisburseLoanTlTarget(
      txnLoanDisbursementTarget.getSumqtrterm()
    );
    //21
    txnLoan.setGetDueLastDisbursedLoan(
      (int) Math.round(
        txnLoan.getLoanWcOutCurEstimated() + (txnLoan.getLoanTlOutCurEstimated() * 0.12)
      )
    );
    //22
    txnLoan.setGetDueThisDisbursedLoan(
      (int) Math.round(
        txnLoan.getDisburseLoanTlTarget() * 0.02
      )
    );
    //23
    txnLoan.setCollectionPrFromDue(
      txnLoan.getGetDueLastDisbursedLoan() + txnLoan.getGetDueThisDisbursedLoan()
    );
    //24
    txnLoan.setCollectionPrFromNonPerming(
      (int) Math.round(txnLoan.getNonPerformingLoanOutCurEstimated() * 0.25)
    );
    //25
    txnLoan.setTotalCollectionPrFromOverDue(
      (int) Math.round(
        txnLoan.getPastDueLoanOutCurEstimated() * 0.2
      )
    );
    //26
    txnLoan.setTotalPrCollectionTarget(
      txnLoan.getCollectionPrFromDue() + txnLoan.getTotalCollectionPrFromOverDue()
    );
    //27
    txnLoan.setCollectionNonPerformingIntTarget(
      (int) Math.round(
        txnLoan.getNonPerformingOnTOutCurEstimated() * 0.2
      )
    );
    //33
    txnLoan.setOverDueLoanEstimated(
      txnLoan.getLoanTotalCurEstimated() + txnLoan.getDisburseLoanTotalTarget() + txnLoan.getTotalPrCollectionTarget()
    );

    //29
    txnLoan.setAnnualIntTarget(txnLoan.getAnnualInt());
    //30
    txnLoan.setTotalIntColTarget(
      txnLoan.getCollectionNonPerformingIntTarget() + txnLoan.getAnnualIntTarget()
    );
    //31
    txnLoan.setLoanOutWcEstimated(
      (int) Math.round(
        (txnLoan.getLoanWcOutCurEstimated() + txnLoan.getDisburseLoanWcTarget()) - (txnLoan.getGetDueLastDisbursedLoan() * 0.88)
      )
    );
    //33
    txnLoan.setTotalLoanOutEstimated(
      txnLoan.getDisburseLoanTotalTarget() + txnLoan.getLoanTotalCurEstimated() - txnLoan.getTotalPrCollectionTarget()
    );
    //28
    txnLoan.setAnnualInt(
      (int) Math.round(
        ((txnLoan.getLoanTotalCurEstimated() + txnLoan.getTotalLoanOutEstimated()) / 2) * 0.0978
      )
    );
    //32
    txnLoan.setLoanOutTlEstimated(
      txnLoan.getTotalLoanOutEstimated() - txnLoan.getLoanOutWcEstimated()
    );
    //34
    txnLoan.setOverDueLoanEstimated(
      txnLoan.getPastDueLoanOutCurEstimated() + txnLoan.getGetDueLastDisbursedLoan()
        + txnLoan.getGetDueThisDisbursedLoan() - txnLoan.getCollectionPrFromDue() - txnLoan.getTotalCollectionPrFromOverDue()
    );
    //36
    txnLoan.setNonPerformingIntEstimated(
      txnLoan.getLoanTlOutCurEstimated() + txnLoan.getGetDueLastDisbursedLoan() - txnLoan.getCollectionPrFromDue()
    );
    //35
    txnLoan.setNonPerformingLoanEstimated(
      (int) Math.round(
        txnLoan.getOverDueLoanEstimated() + txnLoan.getNonPerformingIntEstimated() * 0.025
      )
    );
  }

  private TxnLoanDisbursementTarget getTxnLoanDisbursementTarget(TxnLoan txnLoan) {
    TxnLoanDisbursementTarget txnLoanDisbursementTarget = new TxnLoanDisbursementTarget();
    txnLoanDisbursementTarget.setBranch(txnLoan.getBranch());
    txnLoanDisbursementTarget.setLoanType(txnLoan.getLoanType());

    /*
     * FIRST
     */
    txnLoanDisbursementTarget.setFirstqtrtotal(
      (int) Math.round(txnLoan.getDisburseLoanTotalTarget() * 0.35)
    );

    txnLoanDisbursementTarget.setFirstqtrshort(
      (int) Math.round(txnLoanDisbursementTarget.getFirstqtrtotal() * 0.6)
    );

    txnLoanDisbursementTarget.setFirstqtrterm(
      txnLoanDisbursementTarget.getFirstqtrtotal() - txnLoanDisbursementTarget.getFirstqtrshort()
    );

    /*
     * SECOND
     */
    txnLoanDisbursementTarget.setSecondqtrtotal(
      (int) Math.round(((double) txnLoanDisbursementTarget.getFirstqtrtotal() / 4) * 4)
    );

    txnLoanDisbursementTarget.setSecondqtrshort(
      (int) Math.round(txnLoanDisbursementTarget.getSecondqtrtotal() * 0.6)
    );

    txnLoanDisbursementTarget.setSecondqtrterm(
      txnLoanDisbursementTarget.getSecondqtrtotal() - txnLoanDisbursementTarget.getSecondqtrshort()
    );

    /*
     * THIRD
     */
    txnLoanDisbursementTarget.setThirdqtrtotal(
      (int) Math.round(txnLoan.getDisburseLoanTotalTarget() * 0.2)
    );

    txnLoanDisbursementTarget.setThirdqtrshort(
      (int) Math.round(txnLoanDisbursementTarget.getThirdqtrtotal() * 0.6)
    );

    txnLoanDisbursementTarget.setThirdqtrterm(
      txnLoanDisbursementTarget.getThirdqtrtotal() - txnLoanDisbursementTarget.getThirdqtrshort()
    );

    /*
     * FOURTH
     */
    txnLoanDisbursementTarget.setFourthqtrtotal(
      (int) Math.round((double) txnLoanDisbursementTarget.getThirdqtrtotal() / 2)
    );

    txnLoanDisbursementTarget.setFourthqtrshort(
      (int) Math.round(txnLoanDisbursementTarget.getFourthqtrtotal() * 0.6)
    );

    txnLoanDisbursementTarget.setFourthqtrterm(
      txnLoanDisbursementTarget.getFourthqtrtotal() - txnLoanDisbursementTarget.getFourthqtrshort()
    );
    /*
     *SUM
     */
    txnLoanDisbursementTarget.setSumqtrshort(
      txnLoanDisbursementTarget.getFirstqtrshort()
        + txnLoanDisbursementTarget.getSecondqtrshort()
        + txnLoanDisbursementTarget.getThirdqtrshort()
        + txnLoanDisbursementTarget.getFourthqtrshort()
    );
    txnLoanDisbursementTarget.setSumqtrterm(
      txnLoanDisbursementTarget.getFirstqtrterm()
        + txnLoanDisbursementTarget.getSecondqtrterm()
        + txnLoanDisbursementTarget.getThirdqtrterm()
        + txnLoanDisbursementTarget.getFourthqtrterm()
    );

    txnLoanDisbursementTarget.setSumqtrtotal(
      txnLoanDisbursementTarget.getSumqtrshort() + txnLoanDisbursementTarget.getSumqtrterm()
    );
    return txnLoanDisbursementTarget;
  }
}
