package com.budgetplan.app.txn.listener;

import com.budgetplan.app.txn.entity.TxnLoan;
import com.budgetplan.app.txn.entity.TxnLoanClassification;
import com.budgetplan.app.txn.repository.TxnLoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Optional;

@Component
public class TxnLoanClassificationEntityListener {
    private TxnLoanRepository txnLoanRepository;

    @Autowired
    public void setTxnLoanRepository(TxnLoanRepository txnLoanRepository) {
        this.txnLoanRepository = txnLoanRepository;
    }

    @PreUpdate
    @PrePersist
    public void prePersistUpdate(TxnLoanClassification classification) {
        Optional<TxnLoan> txnLoanOptional = txnLoanRepository.findFirstByBranchBranchCodeAndLoanTypeLoanProductCode(classification.getBranch().getBranchCode(), classification.getLoanType().getLoanProductCode());
        if (txnLoanOptional.isPresent()) {
            TxnLoan txnLoan = txnLoanOptional.get();
            this.setValuesFromLoan(txnLoan, classification);
        }
    }

    private void setValuesFromLoan(TxnLoan txnLoan, TxnLoanClassification classification) {
        classification.setPassthisprojected(
                txnLoan.getLoanTotalCurEstimated() - txnLoan.getNonPerformingLoanOutCurEstimated() - classification.getWatchlistthisprojected()
        );

        classification.setSubstandardthisprojected(
                txnLoan.getNonPerformingLoanOutCurEstimated() - txnLoan.getPastDueLoanOutCurEstimated()
        );
        classification.setDoubtfulthisprojected(
                (int) Math.round(txnLoan.getPastDueLoanOutCurEstimated() * 0.6)
        );
        classification.setLossthisprojected(
                (int) Math.round(txnLoan.getPastDueLoanOutCurEstimated() * 0.4)
        );
        classification.setTotalthisprojected(
                classification.getPassthisprojected() + classification.getSubstandardthisprojected()
                        + classification.getDoubtfulthisprojected() + classification.getLossthisprojected()
        );

        classification.setPassnextprojected(
                txnLoan.getTotalLoanOutEstimated() - txnLoan.getNonPerformingLoanEstimated() - classification.getWatchlistnextprojected()
        );

        classification.setSubstandardnextprojected(
                txnLoan.getTotalLoanOutEstimated() - txnLoan.getNonPerformingLoanEstimated() - classification.getSubstandardnextprojected()
        );
        classification.setDoubtfulnextprojected(
                (int) Math.round(txnLoan.getOverDueLoanEstimated() * 0.6)
        );

        classification.setLossnextprojected(
                (int) Math.round(txnLoan.getOverDueLoanEstimated() * 0.4)
        );

        classification.setTotalnextprojected(
                txnLoan.getTotalLoanOutEstimated()
        );
    }
}
