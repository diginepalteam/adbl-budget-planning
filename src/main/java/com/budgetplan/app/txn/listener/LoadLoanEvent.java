package com.budgetplan.app.txn.listener;

import org.springframework.context.ApplicationEvent;

public class LoadLoanEvent extends ApplicationEvent {
    private String message;

    public LoadLoanEvent(Object source, String message) {
        super(source);
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
}

