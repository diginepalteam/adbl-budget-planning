package com.budgetplan.app.txn.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class LoadLoanEventPublisher {
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void publishLoanLoanEvent(final String message) {
        LoadLoanEvent loadLoanEvent = new LoadLoanEvent(this, message);
        applicationEventPublisher.publishEvent(loadLoanEvent);
    }
}
