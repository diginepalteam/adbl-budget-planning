package com.budgetplan.app.txn.listener;

import com.budgetplan.app.integration.*;
import com.budgetplan.app.integration.responses.ModelProductWiseDeposit;
import com.budgetplan.app.integration.responses.ModelProductWiseLoan;
import com.budgetplan.app.integration.responses.ModelProductWiseLoanClassification;
import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.md.entity.MdLoanType;
import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnDeposit;
import com.budgetplan.app.txn.entity.TxnExpenseBudget;
import com.budgetplan.app.txn.entity.TxnLoan;
import com.budgetplan.app.txn.entity.TxnLoanClassification;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@EnableAsync
@Component
public class LoadLoanEventListener implements ApplicationListener<LoadLoanEvent> {
  private MisIntegrationService misIntegrationService;
  private GenericCrudService<TxnLoan, Integer> genericCrudService;
  private GenericCrudService<MdBranch, String> branchStringGenericCrudService;
  private GenericCrudService<MdLoanType, String> mdLoanTypeStringGenericCrudService;
  private GenericCrudService<TxnDeposit, Integer> txnDepositStringGenericCrudService;
  private GenericCrudService<TxnLoanClassification, Integer> txnLoanClassificationIntegerGenericCrudService;

  @Autowired
  public void setMisIntegrationService(MisIntegrationService misIntegrationService) {
    this.misIntegrationService = misIntegrationService;
  }

  @Autowired
  public void setGenericCrudService(GenericCrudService<TxnLoan, Integer> genericCrudService) {
    this.genericCrudService = genericCrudService;
  }

  @Autowired
  public void setBranchStringGenericCrudService(GenericCrudService<MdBranch, String> branchStringGenericCrudService) {
    this.branchStringGenericCrudService = branchStringGenericCrudService;
  }

  @Autowired
  public void setMdLoanTypeStringGenericCrudService(GenericCrudService<MdLoanType, String> mdLoanTypeStringGenericCrudService) {
    this.mdLoanTypeStringGenericCrudService = mdLoanTypeStringGenericCrudService;
  }

  @Autowired
  public void setTxnDepositStringGenericCrudService(GenericCrudService<TxnDeposit, Integer> txnDepositStringGenericCrudService) {
    this.txnDepositStringGenericCrudService = txnDepositStringGenericCrudService;
  }

  @Autowired
  public void setTxnLoanClassificationIntegerGenericCrudService(GenericCrudService<TxnLoanClassification, Integer> txnLoanClassificationIntegerGenericCrudService) {
    this.txnLoanClassificationIntegerGenericCrudService = txnLoanClassificationIntegerGenericCrudService;
  }

  @SneakyThrows
  @Override
  @Async
  public void onApplicationEvent(LoadLoanEvent event) {
    log.info("Started LoadLoanEvent");
    List<ModelProductWiseLoan> misLoans = misIntegrationService.getLoanDetails();
    if (!misLoans.isEmpty()) {
      try {
        List<TxnLoan> txnLoans = this.mapMisLoansToTxnLoans(misLoans);
        genericCrudService.saveAll(txnLoans);
      } catch (Exception e) {
        log.error("Exception in mapping mis loan to txn loan", e);
      }
    }

    List<ModelProductWiseDeposit> misDeposits = misIntegrationService.getDepositDetails();
    if (!misDeposits.isEmpty()) {
      try {
        List<TxnDeposit> txnDeposits = this.mapMisDepositToTxnDeposit(misDeposits);
        txnDepositStringGenericCrudService.saveAll(txnDeposits);
      } catch (Exception e) {
        log.error("Exception in mapping mis deposit to txn deposit", e);
      }
    }


    List<ModelProductWiseLoanClassification> misLoanClassifications = misIntegrationService.getLoanClassifications();
    if (!misLoanClassifications.isEmpty()) {
      try {
        List<TxnLoanClassification> txnLoanClassifications = this.mapTxnLoanClassification(misLoanClassifications);
        txnLoanClassificationIntegerGenericCrudService.saveAll(txnLoanClassifications);
      } catch (Exception e) {
        log.error("Exception in mapping mis deposit to txn deposit", e);
      }
    }

    log.info("Completed LoadLoanEvent");
  }

  private List<TxnLoanClassification> mapTxnLoanClassification(List<ModelProductWiseLoanClassification> misLoanClassifications) {
    List<TxnLoanClassification> txnLoanClassifications = new ArrayList<>();
    misLoanClassifications.forEach(modelProductWiseLoanClassification -> {
      TxnLoanClassification txnLoanClassification = new TxnLoanClassification();

      Optional<MdBranch> mdBranchOptional = branchStringGenericCrudService.getOne(modelProductWiseLoanClassification.getBranch_branchcode());
      mdBranchOptional.ifPresent(txnLoanClassification::setBranch);

      Optional<MdLoanType> loanTypeOptional = mdLoanTypeStringGenericCrudService.getOne(modelProductWiseLoanClassification.getLoan_type_loanproductcode());
      loanTypeOptional.ifPresent(txnLoanClassification::setLoanType);

      txnLoanClassification.setPasscur(modelProductWiseLoanClassification.getPassloan());
      txnLoanClassification.setWatchlistcur(modelProductWiseLoanClassification.getWatchlist());
      txnLoanClassification.setSubstandardcur(modelProductWiseLoanClassification.getSubstandard());
      txnLoanClassification.setDoubtfulcur(modelProductWiseLoanClassification.getDoubtful());
      txnLoanClassification.setLosscur(modelProductWiseLoanClassification.getLoss());

      txnLoanClassification.setTotalcur(
        txnLoanClassification.getPasscur() + txnLoanClassification.getWatchlistcur()
          + txnLoanClassification.getSubstandardcur() + txnLoanClassification.getDoubtfulcur()
          + txnLoanClassification.getLosscur()
      );

      txnLoanClassifications.add(txnLoanClassification);
    });
    return txnLoanClassifications;
  }

  private List<TxnDeposit> mapMisDepositToTxnDeposit(List<ModelProductWiseDeposit> misDeposits) {
    List<TxnDeposit> txnDeposits = new ArrayList<>();
    misDeposits.forEach(modelProductWiseDeposit -> {
      TxnDeposit txnDeposit = new TxnDeposit();

      Optional<MdBranch> mdBranchOptional = branchStringGenericCrudService.getOne(modelProductWiseDeposit.getBranch_branchcode());
      mdBranchOptional.ifPresent(txnDeposit::setBranch);

      txnDeposit.setInterestrate(modelProductWiseDeposit.getInterestrate());
      txnDeposit.setCategory(modelProductWiseDeposit.getCategory());
      txnDeposit.setDepositoutcur(modelProductWiseDeposit.getDeposit_amount());
      txnDeposit.setMainType(modelProductWiseDeposit.getDeposit_maintype());
      txnDeposit.setDepositType(modelProductWiseDeposit.getDeposit_code());


      txnDeposits.add(txnDeposit);
    });

    return txnDeposits;
  }

  private List<TxnLoan> mapMisLoansToTxnLoans(List<ModelProductWiseLoan> misLoans) {
    List<TxnLoan> txnLoans = new ArrayList<>();
    misLoans.forEach(modelProductWiseLoan -> {
      TxnLoan txnLoan = new TxnLoan();

      Optional<MdBranch> mdBranchOptional = branchStringGenericCrudService.getOne(modelProductWiseLoan.getBranch_branchcode());
      mdBranchOptional.ifPresent(txnLoan::setBranch);

      Optional<MdLoanType> loanTypeOptional = mdLoanTypeStringGenericCrudService.getOne(modelProductWiseLoan.getLoan_type_loanproductcode());
      loanTypeOptional.ifPresent(txnLoan::setLoanType);

      txnLoan.setWorkingCapitalOutstandingCurrent(Integer.parseInt(modelProductWiseLoan.getWorkingcapitaloutstandingcurrent()));
      txnLoan.setTermLoanOutCur(Integer.parseInt(modelProductWiseLoan.getTermloanoutcur()));
      txnLoan.setTotalLoanOutCur(Integer.parseInt(modelProductWiseLoan.getTotalloanoutcur()));
      txnLoan.setPastDueLoanOutCur(Integer.parseInt(modelProductWiseLoan.getPastdueloanoutcur()));
      txnLoan.setNonPerformingLoanOutCur(Integer.parseInt(modelProductWiseLoan.getNonperformingloanoutcur()));
      txnLoan.setNonPerformingInterestOutCur(Integer.parseInt(modelProductWiseLoan.getNonperforminginterestoutcur()));

      txnLoans.add(txnLoan);
    });
    return txnLoans;
  }

}
