package com.budgetplan.app.txn.listener;

import com.budgetplan.app.txn.entity.TxnExpenseBudget;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@Slf4j
@Component
public class TxnExpenseBudgetListener {

  @PreUpdate
  @PrePersist
  public void preUpdatePersist(TxnExpenseBudget txnExpenseBudget) {
    txnExpenseBudget.setTotalcurrentyearbudget(txnExpenseBudget.getCurrentyearbudget() + txnExpenseBudget.getCurrentyearadditionalbudget());
    txnExpenseBudget.setCurrentyearexpenseprojectedannual(txnExpenseBudget.getActualexpensethisyear() + txnExpenseBudget.getCurrentyearexpenseprojected());
  }
}
