package com.budgetplan.app.txn;

import lombok.Getter;

@Getter
public enum TxnProfitAndLossHeadingEnum {
  interestIncome("1. Interest Income"),
  interestExpense("2. Interest Expense"),
  netinterestIncome("3. Net Interest Income (1-2)"),
  feeandcommissionIncome("4. Fee and commission Income"),
  feeandcommissionExpense("5. Fee and commission Expense"),
  netfeeandcommissionIncome("6. Net fee and commission income (4-5)"),
  netInterestandfeecommissionIncome("7. Net interest fee and commission income (3+6)"),
  currencyexchangeIncome("8. Currency updown income"),
  miscoperatingIncome("9. Misc operating income"),
  netoperatingIncome("10. Net operating income"),
  loanandotherassetlossProvision("11. Loan other asset loss provision"),
  netoperatingIncome1("12. Net operating income (10+11)"),
  staffExpense("13. Staff expense"),
  otheroperatingExpense("14. Misc operating expense"),
  depramortExpense("15. Depriciation and amortization"),
  operatingProfit("16. Operating profit (12-13-14-15)"),
  nonoperatingIncome("17. Non-operating income"),
  nonoperatingExpense("18. Non-operating expense"),
  profitbeforeTax("19. Profit before tax (16+17-18)"),
  ftpexpenseIncome("20. FTP expense (-) / FTP income (+)"),
  profitafterFtp("21. Profit/loss after FTP");

  private final String head;

  TxnProfitAndLossHeadingEnum(String head) {
    this.head = head;
  }
}
