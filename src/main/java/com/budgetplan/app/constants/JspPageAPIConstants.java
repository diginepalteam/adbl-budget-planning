
package com.budgetplan.app.constants;

public class JspPageAPIConstants {
    private JspPageAPIConstants() {

    }

    public static final String SPRING_SECURITY_CONTEXT = "SPRING_SECURITY_CONTEXT";
    public static final String V1_LOGIN = "/v1/login-page";
    public static final String V1_DASHBOARD = "/v1/dashboard-page";
    public static final String V1_USER_CREATE = "/v1/user-create-page";
    public static final String V1_USER_VIEW = "/v1/user-view-page";
    public static final String V1_USER_PROFILE_VIEW = "/v1/user-profile-page";

}
