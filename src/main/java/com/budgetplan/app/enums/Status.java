
package com.budgetplan.app.enums;

public enum Status {
    ACTIVE, //pod
    INACTIVE,
    IN_PROGRESS,
    DELIVERED, // pod
    UNDELIVERED, // pod
    CANCEL, // pod
    DAMAGED //pod
}