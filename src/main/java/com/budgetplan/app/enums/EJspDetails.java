
package com.budgetplan.app.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EJspDetails {
    PAGE_TITLE("pagetitle", ""),
    LOGIN("v1/login", ""),
    DASHBOARD("v1/dashboard", "DASHBOARD"),
    USER_CREATE("v1/user/create-page", "CREATE USER"),
    USER_VIEW("v1/user/view-page", "VIEW USER"),
    USER_PROFILE_VIEW("v1/user/profile-page", "PROFILE"),

    DEPOSIT_DETAIL_AND_PROJECTION("v1/deposit-detail-and-projection", "Deposit detail and projection"),
    LOAN_DETAIL_AND_PROJECTION("v1/loan-detail-and-projection", "Loan detail and projection"),
    LOAN_DISBURSEMENT_TARGET("v1/loan-disbursement-target", "Loan Disbursement Target"),
    DUE_LOAN_COLLECTION_TARGET("v1/loan-due-collection-target", "Due Loan Collection Target"),
    OVERDUE_LOAN_COLLECTION_TARGET("v1/overdue-loan-collection-target", "Over-due Loan Collection Target"),
    LOAN_GROWTH_DETAIL("v1/loan-growth-target", "Loan Growth Detail"),
    LOAN_CLASSIFICATION("v1/loan-classification", "Loan Classification"),
    INTEREST_COLLECTION_TARGET("v1/interest-collection-target", "Interest Collection Target"),
    INTEREST_EXPENSE_PROJECTION("v1/interest-expense-projection", "Interest Expense Projection"),
    KARMACHARI_DARBANDI("v1/table10", "Karmachari Darbandi"),
    EXPENSE_BUDGET("v1/expense-budget", "Expense budget"),
    INCOME_FROM_FEE_AND_COMMISSION_TARGET("v1/income-from-fee-and-commission-target", "Income from fee and commission target"),
    BUDGET_DATE_PARAMETER("/v1/budget-date-parameter","Budget Date Parameter"),
    COMMITS("/v1/commits","CURRENT YEAR REMAINING COMMIT"),
    PROJECTION_PROFIT_AND_LOSS("/v1/profit-and-loss","PROFIT AND LOSS"),
    PROJECTION_BALANCE_SHEET("/v1/balance-sheet","BALANCE SHEET"),
    REPORTS("/v1/reports","GENERATE REPORTS"),
    MIS_TRIGGERS("/v1/mis-triggers","SYNC WITH MIS");

    private final String page;
    private final String title;
}
