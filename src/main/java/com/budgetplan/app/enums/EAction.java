
package com.budgetplan.app.enums;

public enum EAction {
    CREATE,
    UPDATE,
    DELETE
}
