package com.budgetplan.app.md.service;

import com.budgetplan.app.md.entity.MdBudgetDateParameter;
import com.budgetplan.app.md.repository.MdBudgetDateParameterRepository;
import com.budgetplan.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MdBudgetDateParameterServiceImpl implements GenericCrudService<MdBudgetDateParameter, Integer> {
    private MdBudgetDateParameterRepository mdBudgetDateParameterRepository;

    @Autowired
    public void setMdBudgetDateParameterRepository(MdBudgetDateParameterRepository mdBudgetDateParameterRepository) {
        this.mdBudgetDateParameterRepository = mdBudgetDateParameterRepository;
    }

    /**
     * <p>
     * if {@link MdBudgetDateParameter} is already available in the system then new data is not saved otherwise
     * new record is saved.
     * Summary: {@link MdBudgetDateParameter} should always flush every year and should have only one row initialized
     * before record keeping in other tables.
     * </p>
     *
     * @param entity {@link MdBudgetDateParameter}
     * @return Optional of {@link MdBudgetDateParameter}
     */
    @Override
    public Optional<MdBudgetDateParameter> save(MdBudgetDateParameter entity) {
        if (this.getAll().isEmpty()) {
            return Optional.of(mdBudgetDateParameterRepository.save(entity));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<MdBudgetDateParameter> saveAll(List<MdBudgetDateParameter> entities) {
        return mdBudgetDateParameterRepository.saveAll(entities);
    }

    @Override
    public Optional<MdBudgetDateParameter> update(MdBudgetDateParameter entity, Integer id) {
        Optional<MdBudgetDateParameter> mdBudgetDateParameterOptional = this.getOne(id);
        if (mdBudgetDateParameterOptional.isPresent()) {
            MdBudgetDateParameter mdBudgetDateParameter = new MdBudgetDateParameter();
            CustomBeanUtils.copyNonNullProperties(mdBudgetDateParameterOptional.get(), mdBudgetDateParameter);
            return Optional.of(mdBudgetDateParameterRepository.save(mdBudgetDateParameter));
        }
        return Optional.empty();
    }

    @Override
    public List<MdBudgetDateParameter> getAll() {
        return mdBudgetDateParameterRepository.findAll();
    }

    @Override
    public Optional<MdBudgetDateParameter> getOne(Integer id) {
        return mdBudgetDateParameterRepository.findById(id);
    }
}
