package com.budgetplan.app.md.service;

import com.budgetplan.app.md.entity.MdLoanType;
import com.budgetplan.app.md.repository.MdLoanTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MdLoanTypeServiceImpl implements GenericCrudService<MdLoanType, String> {
    private MdLoanTypeRepository mdLoanTypeRepository;

    @Autowired
    public void setMdLoanTypeRepository(MdLoanTypeRepository mdLoanTypeRepository) {
        this.mdLoanTypeRepository = mdLoanTypeRepository;
    }

    @Override
    public Optional<MdLoanType> save(MdLoanType entity) {
        return Optional.of(mdLoanTypeRepository.save(entity));
    }

    @Override
    public List<MdLoanType> saveAll(List<MdLoanType> entities) {
        return mdLoanTypeRepository.saveAll(entities);
    }

    @Override
    public Optional<MdLoanType> update(MdLoanType entity, String id) {
        return Optional.empty();
    }

    @Override
    public List<MdLoanType> getAll() {
        return null;
    }

    @Override
    public Optional<MdLoanType> getOne(String id) {
        return mdLoanTypeRepository.findById(id);
    }
}
