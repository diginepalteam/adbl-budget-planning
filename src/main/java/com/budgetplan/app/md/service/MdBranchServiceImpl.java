package com.budgetplan.app.md.service;

import com.budgetplan.app.md.entity.MdBranch;
import com.budgetplan.app.md.repository.MdBranchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MdBranchServiceImpl implements GenericCrudService<MdBranch, String> {
    private MdBranchRepository mdBranchRepository;

    @Autowired
    public void setMdBranchRepository(MdBranchRepository mdBranchRepository) {
        this.mdBranchRepository = mdBranchRepository;
    }

    @Override
    public Optional<MdBranch> save(MdBranch entity) {
        return Optional.of(mdBranchRepository.save(entity));
    }

    @Override
    public List<MdBranch> saveAll(List<MdBranch> entities) {
        return mdBranchRepository.saveAll(entities);
    }

    @Override
    public Optional<MdBranch> update(MdBranch entity, String id) {
        return Optional.empty();
    }

    @Override
    public List<MdBranch> getAll() {
        return null;
    }

    @Override
    public Optional<MdBranch> getOne(String id) {
        return mdBranchRepository.findById(id);
    }
}
