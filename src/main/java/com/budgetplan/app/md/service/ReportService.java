package com.budgetplan.app.md.service;

import com.budgetplan.app.md.entity.ReportEntity;

import java.util.Optional;

public interface ReportService {
  Optional<ReportEntity> getByType(String type);
}
