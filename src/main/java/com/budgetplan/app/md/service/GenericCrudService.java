package com.budgetplan.app.md.service;

import java.util.List;
import java.util.Optional;

public interface GenericCrudService<E, I> {
    Optional<E> save(E entity);

    List<E> saveAll(List<E> entities);

    Optional<E> update(E entity, I id);

    List<E> getAll();

    Optional<E> getOne(I id);
}
