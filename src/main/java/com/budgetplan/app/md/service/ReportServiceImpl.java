package com.budgetplan.app.md.service;

import com.budgetplan.app.md.entity.ReportEntity;
import com.budgetplan.app.md.repository.ReportRepository;
import com.budgetplan.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReportServiceImpl implements ReportService, GenericCrudService<ReportEntity, Integer>{
  private ReportRepository reportRepository;

  @Autowired
  public void setReportRepository(ReportRepository reportRepository) {
    this.reportRepository = reportRepository;
  }

  @Override
  public Optional<ReportEntity> save(ReportEntity entity) {
    return Optional.of(reportRepository.save(entity));
  }

  @Override
  public List<ReportEntity> saveAll(List<ReportEntity> entities) {
    return reportRepository.saveAll(entities);
  }

  @Override
  public Optional<ReportEntity> update(ReportEntity entity, Integer id) {
    Optional<ReportEntity> reportEntityOptional = this.getOne(id);
    if(reportEntityOptional.isPresent()){
      ReportEntity reportEntity = reportEntityOptional.get();
      CustomBeanUtils.copyNonNullProperties(entity, reportEntity);
      return this.save(reportEntity);
    }
    return Optional.empty();
  }

  @Override
  public List<ReportEntity> getAll() {
    return reportRepository.findAll();
  }

  @Override
  public Optional<ReportEntity> getOne(Integer id) {
    return reportRepository.findById(id);
  }

  @Override
  public Optional<ReportEntity> getByType(String type) {
    return reportRepository.findFirstByType(type);
  }
}
