package com.budgetplan.app.md.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "md_expensemain")
public class MdExpenseMain {
  @Id
  @Column(name = "maincode")
  private int code;
  @Column(name = "mainhead")
  private String head;
}
