package com.budgetplan.app.md.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

//todo auto creation of this table data
@Getter
@Setter
@Entity
@Table(name = "md_expensecode")
public class MdExpenseCode {
  @Id
  @Column(name = "expense_code", unique = true)
  private int code;
  @Column(name = "expense_head")
  private String head;
  @Column(name = "expense_headnp")
  private String headNp;
  @ManyToOne
  @JoinColumn(name = "maincode")
  private MdExpenseMain mainCode;
}
