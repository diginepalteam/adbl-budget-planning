package com.budgetplan.app.md.entity;

import com.budgetplan.app.commons.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "md_deposittype")
public class MdDepositType extends Auditable<String> {
    @Id
    @Column(name = "depositcode", unique = true)
    private String depositCode;
    @Column(name = "name")
    private String name;
    @Column(name = "namenp")
    private String nameNp;
}
