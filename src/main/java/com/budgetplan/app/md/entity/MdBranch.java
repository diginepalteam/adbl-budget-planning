package com.budgetplan.app.md.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "md_branch")
public class MdBranch {
    @Id
    @Column(name = "branchcode",unique = true)
    private String branchCode;
    @Column(name = "name")
    private String name;
    @Column(name = "namenp")
    private String nameNp;
    @Column(name = "district")
    private String district;
    @Column(name = "province")
    private String province;
    @Column(name = "nrbocode")
    private String nrbOfficeCode;
}
