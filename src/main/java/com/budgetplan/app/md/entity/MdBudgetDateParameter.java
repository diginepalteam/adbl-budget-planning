package com.budgetplan.app.md.entity;

import com.budgetplan.app.commons.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "md_budget_date_parameter")
public class MdBudgetDateParameter extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String currentYearActual;
    private String currentYearGrowth;
    private String currentYearProjection;
    private String nextYearProjection;
    private String nextYearEnd;
}
