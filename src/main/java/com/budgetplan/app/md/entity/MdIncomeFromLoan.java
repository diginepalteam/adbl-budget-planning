package com.budgetplan.app.md.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "md_incomefromloan")
public class MdIncomeFromLoan {
    @Id
    @Column(name = "incomeid", unique = true)
    private int incomeId;
    @Column(name = "incomehead")
    private String incomeHead;
    @Column(name = "incomeheadnp")
    private String incomeHeadNp;

}
