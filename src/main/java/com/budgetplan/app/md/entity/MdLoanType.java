package com.budgetplan.app.md.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "md_loantype")
public class MdLoanType {
    @Id
    @Column(name = "loanproductcode", unique = true)
    private String loanProductCode;
    @Column(name = "name")
    private String name;
    @Column(name = "namenp")
    private String nameNp;
}
