package com.budgetplan.app.md.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "md_reports")
@Getter
@Setter
public class ReportEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  private String type;
  private String path; // example: /opt/tomcat/webapps/file-name.jrxml
  private String description;
}
