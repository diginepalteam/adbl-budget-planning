package com.budgetplan.app.md.controller;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.txn.listener.LoadLoanEventPublisher;
import com.budgetplan.app.md.entity.MdBudgetDateParameter;
import com.budgetplan.app.md.service.GenericCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.budgetplan.app.auth.constants.ApiConstants.MD_BUDGET_DATE_PARAMETER;
import static com.budgetplan.app.auth.constants.ApiConstants.VERSION_1;

@RestController
@RequestMapping(VERSION_1)
public class MdBudgetDateParameterController {

    private GenericCrudService<MdBudgetDateParameter, Integer> genericCrudService;
    private LoadLoanEventPublisher loadLoanEventPublisher;

    @Autowired
    public void setGenericCrudService(GenericCrudService<MdBudgetDateParameter, Integer> genericCrudService) {
        this.genericCrudService = genericCrudService;
    }

    @Autowired
    public void setLoadLoanEventPublisher(LoadLoanEventPublisher loadLoanEventPublisher) {
        this.loadLoanEventPublisher = loadLoanEventPublisher;
    }

    @PostMapping(MD_BUDGET_DATE_PARAMETER)
    public ResponseEntity<?> createMdBudgetDateParameter(@RequestBody MdBudgetDateParameter mdBudgetDateParameter) {
        Optional<MdBudgetDateParameter> mdBudgetDateParameterOptional = genericCrudService.save(mdBudgetDateParameter);
        if (mdBudgetDateParameterOptional.isPresent()) {

            loadLoanEventPublisher.publishLoanLoanEvent(null);

            return new ApiResponse().getSuccessResponse(HttpStatus.OK.getReasonPhrase(), mdBudgetDateParameterOptional.get());
        } else {
            return new ApiResponse().getFailureResponse(HttpStatus.BAD_REQUEST.getReasonPhrase());
        }
    }

    @GetMapping(MD_BUDGET_DATE_PARAMETER)
    public ResponseEntity<?> getOneMdBudgetDateParameter() {
        List<MdBudgetDateParameter> mdBudgetDateParameterList = genericCrudService.getAll();
        if (!mdBudgetDateParameterList.isEmpty()) {
            return new ApiResponse().getSuccessResponse(HttpStatus.FOUND.getReasonPhrase(), mdBudgetDateParameterList.get(0));
        } else {
            return new ApiResponse().getSuccessResponse(HttpStatus.NO_CONTENT.getReasonPhrase(), new ArrayList<>());
        }
    }
}
