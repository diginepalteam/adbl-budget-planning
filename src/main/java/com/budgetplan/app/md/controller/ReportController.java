package com.budgetplan.app.md.controller;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.md.dto.ReportRequest;
import com.budgetplan.app.md.entity.ReportEntity;
import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.md.service.ReportService;
import com.budgetplan.app.utils.jasper.EFileType;
import com.budgetplan.app.utils.jasper.JasperRequestDto;
import com.budgetplan.app.utils.jasper.ReportUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class ReportController {
  @Value("${budget.db.name}")
  private String dbName;
  private ReportService reportService;
  private GenericCrudService<ReportEntity, Integer> reportEntityIntegerGenericCrudService;
  private ReportUtility reportUtility;

  @Autowired
  public void setReportService(ReportService reportService) {
    this.reportService = reportService;
  }

  @Autowired
  public void setReportEntityIntegerGenericCrudService(GenericCrudService<ReportEntity, Integer> reportEntityIntegerGenericCrudService) {
    this.reportEntityIntegerGenericCrudService = reportEntityIntegerGenericCrudService;
  }

  @Autowired
  public void setReportUtility(ReportUtility reportUtility) {
    this.reportUtility = reportUtility;
  }

  @PostMapping(value = "reports")
  public ResponseEntity<?> saveReportsMetadata(@RequestBody ReportEntity reportEntity) {
    Optional<ReportEntity> reportEntityOptional = reportEntityIntegerGenericCrudService.save(reportEntity);
    if (reportEntityOptional.isPresent()) {
      return new ApiResponse().getSuccessResponse(HttpStatus.OK.getReasonPhrase(), reportEntityOptional.get());
    } else {
      return new ApiResponse().getFailureResponse(HttpStatus.BAD_REQUEST.getReasonPhrase());
    }
  }

  @GetMapping(value = "reports")
  public void generateReport(@ModelAttribute ReportRequest request, HttpServletResponse httpServletResponse) {
    Optional<ReportEntity> reportEntityOptional = reportService.getByType(request.getType());
    if (reportEntityOptional.isPresent()) {
      ReportEntity reportEntity = reportEntityOptional.get();

      Map<String, Object> params = new HashMap<>();
      params.put("db", dbName);

      JasperRequestDto jasperRequestDto = reportUtility.generateJasperRequest(
        null, null, request.getType(), reportEntity.getPath(), params
      );

      if (null != jasperRequestDto) {
        jasperRequestDto.setOutputFileType(EFileType.PDF);
        try {
          reportUtility.exportDownloadableReport(httpServletResponse, jasperRequestDto);
        } catch (Exception e) {
          log.error("generateReport() : Exception downloading {} report", request.getType(), e);
        }
      }
    }
  }
}
