package com.budgetplan.app.md.repository;

import com.budgetplan.app.md.entity.MdBudgetDateParameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MdBudgetDateParameterRepository extends JpaRepository<MdBudgetDateParameter, Integer> {
}
