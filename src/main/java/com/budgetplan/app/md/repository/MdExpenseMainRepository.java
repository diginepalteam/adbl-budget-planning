package com.budgetplan.app.md.repository;

import com.budgetplan.app.md.entity.MdExpenseMain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MdExpenseMainRepository extends JpaRepository<MdExpenseMain, Integer> {
}
