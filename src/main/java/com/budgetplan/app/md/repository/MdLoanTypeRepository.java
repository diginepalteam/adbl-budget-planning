package com.budgetplan.app.md.repository;

import com.budgetplan.app.md.entity.MdLoanType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MdLoanTypeRepository extends JpaRepository<MdLoanType,String> {
}
