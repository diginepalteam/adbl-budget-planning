package com.budgetplan.app.md.repository;

import com.budgetplan.app.md.entity.ReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ReportRepository extends JpaRepository<ReportEntity, Integer> {
  Optional<ReportEntity> findFirstByType(String type);
}
