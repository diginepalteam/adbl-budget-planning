package com.budgetplan.app.md.repository;

import com.budgetplan.app.md.entity.MdBranch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MdBranchRepository extends JpaRepository<MdBranch, String> {
}
