package com.budgetplan.app.md.repository;

import com.budgetplan.app.md.entity.MdIncomeFromLoan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MdIncomeFromLoanRepository extends JpaRepository<MdIncomeFromLoan, String> {
}
