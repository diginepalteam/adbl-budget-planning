package com.budgetplan.app.md.repository;

import com.budgetplan.app.md.entity.MdExpenseCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MdExpenseCodeRepository extends JpaRepository<MdExpenseCode, Integer> {
}
