package com.budgetplan.app.md.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportRequest {
  private String type;
}
