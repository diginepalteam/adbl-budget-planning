
package com.budgetplan.app.dms;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;


public interface FilesStorageService {
    void init();

    void save(MultipartFile file, String fileName);

    Resource load(String filename);

    void deleteAll();

    Stream<Path> loadAll();

    Optional<Documents> saveDocuments(Documents documents);

    Optional<Documents> createDocuments(MultipartFile file, FileInfo fileInfo);

    List<Documents> findDocuments(String type, int refId);

    Optional<Documents> findDocuments(String type, String subType, int refId);

    List<Documents> findByCreatedBy(String createdBy);
}
