
package com.budgetplan.app.dms;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class FilesController {
    private FilesStorageService filesStorageService;
    private DocumentsDtoAssembler documentsDtoAssembler;

    @Autowired
    public void setFilesStorageService(FilesStorageService filesStorageService) {
        this.filesStorageService = filesStorageService;
    }

    @Autowired
    public void setDocumentsDtoAssembler(DocumentsDtoAssembler documentsDtoAssembler) {
        this.documentsDtoAssembler = documentsDtoAssembler;
    }

    @PostMapping("/uploads")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file, @RequestParam(value = "jsondata") String jsonData) {
        log.info("Processing file upload {}", jsonData);
        String message = "";
        try {
            FileInfo fileInfo = new ObjectMapper().readValue(jsonData, FileInfo.class);
            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return new ApiResponse().getSuccessResponse(message, filesStorageService.createDocuments(file, fileInfo).get());
        } catch (Exception e) {
            log.error("Exception in file upload ",e);
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return new ApiResponse().getFailureResponse(message);
        }
    }

    @GetMapping("/uploads/files")
    public ResponseEntity<List<FileInfo>> getListFiles() {
        List<FileInfo> fileInfos = filesStorageService.loadAll().map(path -> {
            String filename = path.getFileName().toString();
            String url = MvcUriComponentsBuilder
                    .fromMethodName(FilesController.class, "getFile", path.getFileName().toString()).build().toString();

            return new FileInfo(filename, url);
        }).collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
    }

    @GetMapping("/uploads/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = filesStorageService.load(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("/uploads/documents/{type}/{refId}")
    @ResponseBody
    public ResponseEntity<?> getDocuments(@PathVariable String type, @PathVariable int refId) {
        List<Documents> documents = filesStorageService.findDocuments(type, refId);
        return new ApiResponse().getSuccessResponse(null, documentsDtoAssembler.toModels(documents));
    }
}
