
package com.budgetplan.app.dms;

import com.budgetplan.app.utils.CustomBeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DocumentsDtoAssembler {
    public FileInfo toModel(Documents entity) {
        FileInfo model = new FileInfo();
        CustomBeanUtils.copyNonNullProperties(entity, model);
        return model;
    }

    public List<FileInfo> toModels(List<Documents> entities) {
        List<FileInfo> models = new ArrayList<>();
        entities.forEach(Documents -> models.add(this.toModel(Documents)));
        return models;
    }

    public Documents toEntity(FileInfo model) {
        Documents entity = new Documents();
        CustomBeanUtils.copyNonNullProperties(model, entity);
        return entity;
    }

    public List<Documents> toEntities(List<FileInfo> models) {
        List<Documents> entities = new ArrayList<>();
        models.forEach(FileInfo -> entities.add(this.toEntity(FileInfo)));
        return entities;
    }
}
