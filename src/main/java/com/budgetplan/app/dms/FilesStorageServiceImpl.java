
package com.budgetplan.app.dms;

import com.budgetplan.app.utils.CustomBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class FilesStorageServiceImpl implements FilesStorageService {

    //todo make path readable from properties or database
    private final Path root = Paths.get("/opt/budgetplan/uploads");
    private DocumentsRepository documentsRepository;

    @Autowired
    public void setDocumentsRepository(DocumentsRepository documentsRepository) {
        this.documentsRepository = documentsRepository;
    }

    @Override
    public void init() {
        try {
            Files.createDirectory(root);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    @Override
    public void save(MultipartFile file, String fileName) {
        try {
            Files.copy(file.getInputStream(), this.root.resolve(fileName));
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    @Override
    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(root.toFile());
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
        } catch (IOException e) {
            throw new RuntimeException("Could not load the files!");
        }
    }

    @Override
    public Optional<Documents> saveDocuments(Documents documents) {
        return Optional.of(documentsRepository.save(documents));
    }

    @Override
    public Optional<Documents> createDocuments(MultipartFile file, FileInfo fileInfo) {
        String extension = this.getExtension(file);

        String fileName = fileInfo.getType().concat("-")
                .concat(fileInfo.getSubType())
                .concat("-").concat(String.valueOf(fileInfo.getRefId()))
                .concat("-")
                .concat(UUID.randomUUID().toString())
                .concat("." + extension);

        this.save(file, fileName);

        String url = this.generateDocumentUrl(fileName);
        fileInfo.setUrl(url);
        fileInfo.setName(fileName);

        Documents documents = new Documents();
        CustomBeanUtils.copyNonNullProperties(fileInfo, documents);

        Optional<Documents> existingDocumentsOptional = documentsRepository.findFirstByTypeAndSubTypeAndRefId(fileInfo.getType(), fileInfo.getSubType(), fileInfo.getRefId());
        if (existingDocumentsOptional.isPresent()) {
            Documents existingDocuments = existingDocumentsOptional.get();

            documents.setId(existingDocuments.getId());
            CustomBeanUtils.copyNonNullProperties(documents, existingDocuments);

            return this.saveDocuments(existingDocuments);
        } else {
            return this.saveDocuments(documents);
        }
    }

    @Override
    public List<Documents> findDocuments(String type, int refId) {
        return documentsRepository.findByTypeAndRefId(type, refId);
    }

    @Override
    public Optional<Documents> findDocuments(String type, String subType, int refId) {
        return documentsRepository.findFirstByTypeAndSubTypeAndRefId(type, subType, refId);
    }

    @Override
    public List<Documents> findByCreatedBy(String createdBy) {
        return documentsRepository.findAllByCreatedBy(createdBy);
    }

    private String generateDocumentUrl(String fileName) {
        return MvcUriComponentsBuilder
                .fromMethodName(FilesController.class, "getFile", fileName).build().toString();
    }

    private String getExtension(MultipartFile file) {
        return file.getOriginalFilename().split("\\.")[1];
    }
}
