
package com.budgetplan.app.dms;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DocumentsRepository extends JpaRepository<Documents, Integer> {
    Optional<Documents> findFirstByTypeAndSubTypeAndRefId(String type, String subType, int refId);

    List<Documents> findByTypeAndRefId(String type, int refId);

    List<Documents> findAllByCreatedBy(String createdBy);
}
