
package com.budgetplan.app.dms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FileInfo {
    private String name;
    private String url;
    private String type;
    private String subType;
    private int refId;

    public FileInfo(String name, String url) {
        this.name = name;
        this.url = url;
    }
}
