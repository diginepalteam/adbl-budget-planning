
package com.budgetplan.app.dms;

import com.budgetplan.app.commons.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "txn_documents")
public class Documents extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String type;
    private String subType;
    private String url;
    private String name;
    private int refId;
}
