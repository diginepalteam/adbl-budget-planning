
package com.budgetplan.app.dms.documents;

import com.budgetplan.app.apiresponse.ApiResponse;
import com.budgetplan.app.auth.entity.User;
import com.budgetplan.app.auth.service.UserService;
import com.budgetplan.app.dms.FilesStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("impl/api/v1")
public class DocumentsController {
    private FilesStorageService filesStorageService;
    private UserService userService;

    @Autowired
    public void setFilesStorageService(FilesStorageService filesStorageService) {
        this.filesStorageService = filesStorageService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("documents")
    public ResponseEntity<?> getAllDocuments(){
        Optional<User> userOptional = userService.getCurrentUserFromToken();
        if(userOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, filesStorageService.findByCreatedBy(userOptional.get().getUsername()));
        }else{
            return new ApiResponse().getFailureResponse(null);
        }
    }
}
