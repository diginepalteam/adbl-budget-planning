
package com.budgetplan.app.controller;

import com.budgetplan.app.auth.service.UserService;
import com.budgetplan.app.constants.JspPageAPIConstants;
import com.budgetplan.app.enums.EJspDetails;
import com.budgetplan.app.txn.service.TxnLoanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.KeyPairGenerator;
import java.security.Signature;

@Slf4j
@RestController
public class JspPagesController {
    private UserService userService;
    private TxnLoanService txnLoanService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTxnLoanService(TxnLoanService txnLoanService) {
        this.txnLoanService = txnLoanService;
    }

    @GetMapping(value = "/")
    public ModelAndView index(HttpServletRequest request) {
        return dashboard(request);
    }

    @GetMapping(value = JspPageAPIConstants.V1_LOGIN)
    public ModelAndView login(String error, String logout) {
        ModelAndView model = new ModelAndView(EJspDetails.LOGIN.getPage());
        if (!StringUtils.isEmpty(error)) {
            model.addObject("msg", error);
        }
        if (!StringUtils.isEmpty(logout)) {
            model.addObject("msg", logout);
        }
        return model;
    }

    @GetMapping(JspPageAPIConstants.V1_DASHBOARD)
    public ModelAndView dashboard(HttpServletRequest request) {
        ModelAndView model = new ModelAndView(EJspDetails.DASHBOARD.getPage());
        model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.DASHBOARD.getTitle());
        return checkSessionSecurityContext(request, model);
    }

    @GetMapping(JspPageAPIConstants.V1_USER_CREATE)
    public ModelAndView createUser(HttpServletRequest request) {
        ModelAndView model = new ModelAndView(EJspDetails.USER_CREATE.getPage());
        model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.USER_CREATE.getTitle());
        return checkSessionSecurityContext(request, model);
    }

    @GetMapping(JspPageAPIConstants.V1_USER_VIEW)
    public ModelAndView viewUser(HttpServletRequest request) {
        ModelAndView model = new ModelAndView(EJspDetails.USER_VIEW.getPage());
        model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.USER_VIEW.getTitle());
        return checkSessionSecurityContext(request, model);
    }

    @GetMapping(JspPageAPIConstants.V1_USER_PROFILE_VIEW)
    public ModelAndView viewMyProfile(HttpServletRequest request) {
        ModelAndView model = new ModelAndView(EJspDetails.USER_PROFILE_VIEW.getPage());
        model.addObject(EJspDetails.PAGE_TITLE.getPage(), EJspDetails.USER_PROFILE_VIEW.getTitle());
        return checkSessionSecurityContext(request, model);
    }

    /*
     * NAVIGATION PAGES CONTROLLER
     */
    @GetMapping("v1/pages/navs/{pageType}")
    public ModelAndView viewMyProfile(HttpServletRequest request, @PathVariable String pageType) {
        EJspDetails eJspDetails = EJspDetails.valueOf(pageType);
        ModelAndView model = new ModelAndView(eJspDetails.getPage());
        model.addObject(EJspDetails.PAGE_TITLE.getPage(), eJspDetails.getTitle());
        return checkSessionSecurityContext(request, model);
    }

    private ModelAndView checkSessionSecurityContext(HttpServletRequest request, ModelAndView modelAndView) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if (securityContext == null) {
            log.error("checkSessionSecurityContext() : Security context not found");
            //todo: this line should be uncommented. fix security context being null after some time
//            return login("Please Login", "");
        } else {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String currentPrincipalName = authentication.getName();
            log.info("checkSessionSecurityContext() : Security context found as {}", currentPrincipalName);
        }
        return modelAndView;
    }
}
