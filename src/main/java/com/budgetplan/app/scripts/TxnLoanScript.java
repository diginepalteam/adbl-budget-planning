package com.budgetplan.app.scripts;

import com.budgetplan.app.integration.MisIntegrationService;
import com.budgetplan.app.integration.responses.ModelProductWiseLoan;
import com.budgetplan.app.md.service.GenericCrudService;
import com.budgetplan.app.txn.entity.TxnLoan;
import com.budgetplan.app.txn.service.TxnLoanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * todo write script to update nonPerformingLoanOutCur from {@link com.budgetplan.app.txn.entity.TxnLoan} and insert new headings for each table
 * as provided by Sunil Karki from MIS API
 */
@Slf4j
@Component
public class TxnLoanScript {
  private MisIntegrationService misIntegrationService;
  private TxnLoanService txnLoanService;
  GenericCrudService<TxnLoan, Integer> genericCrudService;

  @Autowired
  public void setMisIntegrationService(MisIntegrationService misIntegrationService) {
    this.misIntegrationService = misIntegrationService;
  }

  @Autowired
  public void setTxnLoanService(TxnLoanService txnLoanService) {
    this.txnLoanService = txnLoanService;
  }

  @Autowired
  public void setGenericCrudService(GenericCrudService<TxnLoan, Integer> genericCrudService) {
    this.genericCrudService = genericCrudService;
  }

  public void updateNonPerformingLoanOutCur(){
    List<ModelProductWiseLoan> loans = misIntegrationService.getLoanDetails();
    if(null!=loans && !loans.isEmpty()){
      List<TxnLoan> txnLoans = genericCrudService.getAll();
      txnLoans.forEach(txnLoan -> {
        List<ModelProductWiseLoan> modelProductWiseLoans = loans.stream().filter(
          modelProductWiseLoan -> {
            return modelProductWiseLoan.getBranch_branchcode().equals(txnLoan.getBranch().getBranchCode())
              && modelProductWiseLoan.getLoan_type_loanproductcode().equals(txnLoan.getLoanType().getLoanProductCode());
          }
        ).collect(Collectors.toList());
        if(!modelProductWiseLoans.isEmpty()){
          try {
            txnLoan.setNonPerformingLoanOutCur(Integer.parseInt(modelProductWiseLoans.get(0).getNonperformingloanoutcur()));
          }catch (NumberFormatException ex){
            log.warn("updateNonPerformingLoanOutCur() : unable to parse nonPerformingLoanOutCur {} to int", modelProductWiseLoans.get(0).getNonperformingloanoutcur());
          }
        }
      });

      genericCrudService.saveAll(txnLoans);
    }
  }
}
