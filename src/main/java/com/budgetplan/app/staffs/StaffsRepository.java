
package com.budgetplan.app.staffs;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StaffsRepository extends JpaRepository<Staffs, Integer> {
    List<Staffs> findAllByBranchBranchCode(String branchCode);
}
