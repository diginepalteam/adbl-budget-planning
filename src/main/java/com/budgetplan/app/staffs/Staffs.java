
package com.budgetplan.app.staffs;

import com.budgetplan.app.commons.Auditable;
import com.budgetplan.app.md.entity.MdBranch;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "staffs")
public class Staffs {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String phoneNumber;
    private String email;
    @OneToOne(optional = true)
    private MdBranch branch;
}
