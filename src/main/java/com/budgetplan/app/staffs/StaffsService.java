
package com.budgetplan.app.staffs;

import java.util.List;
import java.util.Optional;

public interface StaffsService {
    List<Staffs> getByBranchCode(String branchCode);

    Optional<Staffs> save(Staffs staffs);

    Optional<Staffs> update(int id, Staffs staffs);

    Optional<Staffs> getOne(int id);

    List<Staffs> getAll();

    void deleteById(int id);
}
