
package com.budgetplan.app.staffs;

import com.budgetplan.app.apiresponse.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("impl/api/v1")
public class StaffsController {
    private StaffsService staffsService;
    private StaffsDtoAssembler staffsDtoAssembler;

    @Autowired
    public void setStaffsDtoAssembler(StaffsDtoAssembler staffsDtoAssembler) {
        this.staffsDtoAssembler = staffsDtoAssembler;
    }

    @Autowired
    public void setStaffsService(StaffsService staffsService) {
        this.staffsService = staffsService;
    }

    @GetMapping("/staffs")
    public ResponseEntity<?> getAllStaffs() {
        return new ApiResponse().getSuccessResponse(null, staffsService.getAll());
    }

    @PostMapping("/staffs")
    public ResponseEntity<?> createStaff(@RequestBody StaffsDto staffsDto) {
        Optional<Staffs> staffsOptional = staffsService.save(staffsDtoAssembler.toDomain(staffsDto));
        if (staffsOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, staffsDtoAssembler.toModel(staffsOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @GetMapping("/staffs/{id}")
    public ResponseEntity<?> getOneStaff(@PathVariable int id) {
        Optional<Staffs> staffsOptional = staffsService.getOne(id);
        if (staffsOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, staffsDtoAssembler.toModel(staffsOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

    @PutMapping("/staffs/{id}")
    public ResponseEntity<?> updateStaff(@PathVariable int id, @RequestBody StaffsDto staffsDto) {
        Optional<Staffs> staffsOptional = staffsService.update(id, staffsDtoAssembler.toDomain(staffsDto));
        if (staffsOptional.isPresent()) {
            return new ApiResponse().getSuccessResponse(null, staffsDtoAssembler.toModel(staffsOptional.get()));
        }
        return new ApiResponse().getFailureResponse(null);
    }

}
