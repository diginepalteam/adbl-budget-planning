
package com.budgetplan.app.staffs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StaffsDto {

    private int id;

    private String firstName;

    private String middleName;

    private String lastName;

    private String phoneNumber;

    private String email;

    private String branchCode;

}
