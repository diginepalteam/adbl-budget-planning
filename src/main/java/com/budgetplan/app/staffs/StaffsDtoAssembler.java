
package com.budgetplan.app.staffs;

import com.budgetplan.app.utils.CustomBeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StaffsDtoAssembler {

    public Staffs toDomain(StaffsDto staffsDto) {
        Staffs staffs = new Staffs();
        CustomBeanUtils.copyNonNullProperties(staffsDto, staffs);
        return staffs;
    }

    public StaffsDto toModel(Staffs staffs) {
        StaffsDto staffsDto = new StaffsDto();
        CustomBeanUtils.copyNonNullProperties(staffs, staffsDto);
        return staffsDto;
    }

    public List<StaffsDto> toModels(List<Staffs> staffsList) {
        List<StaffsDto> staffsDtos = new ArrayList<>();
        staffsList.forEach(staffs -> staffsDtos.add(this.toModel(staffs)));
        return staffsDtos;
    }
}
