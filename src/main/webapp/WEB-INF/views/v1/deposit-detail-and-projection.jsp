<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tags:header/>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"> DEPOSIT DETAIL AND PROJECTION </h2>
    </header>
    <div class="panel-body xScrollable">
        <table class="table table-bordered table-striped mb-none" id="datatable-editable" width="100%" style="color: #000000">
            <thead >
            <tr>
                <td rowspan=3>क्र.सं.
                </td>
                <td rowspan=3></td>
                <td rowspan=3>विवरण</td>
                <td rowspan=2> <span class="currentYearActual"></span> चैत्र मसान्तमा बाँकी रहेको निक्षेप
                </td>
                <td rowspan=2> <span class="currentYearGrowth"></span> वैशाख
                    देखि आषाढ सम्ममा हुने अनुमानित खुद वृद्धि
                </td>
                <td rowspan=2> <span class="currentYearProjection"></span> आषाढ
                    को अन्त्यमा हुने अनुमानित निक्षेप
                </td>
                <td colspan=5>आ.व. <span class="nextYearProjection"></span>
                    मा हुने अनुमानित खुद वृद्धि (त्रैमासिक अनुसार वृद्धि हुने रकम मात्र समवेश गर्ने)
                </td>
                <td rowspan=2>आ.व. <span class="nextYearProjection"></span>
                    को अन्त्यमा हुने अनुमानित निक्षेप
                </td>
            </tr>
            <tr>
                <td>प्रथम त्रैमासिक (सावण देखि आश्विन सम्म)को वृद्धि
                </td>
                <td>दोसो त्रैमासिक (कार्तिक देखि पौष सम्म)को मात्र वृद्धि
                </td>
                <td>तेसो त्रैमासिक(माघ देखि चैत्र सम्म) को मात्र वृद्धि
                </td>
                <td>चौथो त्रैमासिक (वैशाख देखि आषाढ सम्म) को मात्र वृद्धि
                </td>
                <td>वार्षिक वृद्धि जम्मा</td>
            </tr>
            <tr>
                <td>(क)</td>
                <td> (ख)</td>
                <td> (ग)=(क)+(ख)</td>
                <td>(घ)</td>
                <td>(ङ)</td>
                <td>(च)</td>
                <td>(छ)</td>
                <td>(ज)=(घ)+(ङ)+(च)+(छ)</td>
                <td> (झ)=(ग)+(ज)</td>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot style="font-weight: bolder">
            <tr></tr>
            </tfoot>
        </table>
    </div>
</section>

<div class="modal fade" id="updateDataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Update (<span id="title"></span>)</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <b><span class="currentYearGrowth"></span> वैशाख
                    देखि आषाढ सम्ममा हुने अनुमानित खुद वृद्धि</b>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="depositoutstgrowth"></label>
                                <input id="depositoutstgrowth" type="text"
                                       name="depositoutstgrowth" class="form-control">
                            </div>
                        </div>
                    </div>
                    <hr>
                    आ.व. <span class="nextYearProjection"></span>
                    मा हुने अनुमानित खुद वृद्धि (त्रैमासिक अनुसार वृद्धि हुने रकम मात्र समवेश गर्ने)
                    <hr>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="firstqtr">प्रथम त्रैमासिक (सावण देखि आश्विन सम्म)को वृद्धि</label>
                                <input id="firstqtr" type="text" name="firstqtr"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="secondqtr">दोसो त्रैमासिक (कार्तिक देखि पौष सम्म)को मात्र वृद्धि</label>
                                <input id="secondqtr" type="text" name="secondqtr"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label preeti" for="thirdqtr">तेसो त्रैमासिक(माघ देखि चैत्र सम्म) को मात्र वृद्धि</label>
                                <input id="thirdqtr" type="text" name="thirdqtr"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label preeti" for="fourthqtr">चौथो त्रैमासिक (वैशाख देखि आषाढ सम्म) को मात्र वृद्धि</label>
                                <input id="fourthqtr" type="text" name="fourthqtr"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button id="updateDataBtn" class="btn btn-success modal-confirm"><i class="fa fa-save"></i>
                            Update
                        </button>
                        <button id="closeModalBtn" class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
</div>
<tags:footer/>
<script src="${pageContext.request.contextPath }/jsForPages/tables/deposit-detail-and-projection.js"
        type="module"></script>