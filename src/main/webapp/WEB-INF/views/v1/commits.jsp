<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<tags:header/>
<section class="panel">
  <header class="panel-heading">
    <h2 class="panel-title">Enter Commit</h2>
  </header>
  <div class="panel-body">
    <form class="form-horizontal form-bordered" id="currentYearRemainingCommitForm">
      <div class="form-group">
        <label class="col-md-3 control-label" for="currentoutstanding">Current Outstanding</label>
        <div class="col-md-6">
          <input type="number" class="form-control" name="currentoutstanding" id="currentoutstanding">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label" for="disbursementwithrenewal">Disbursement With Renewal</label>
        <div class="col-md-6">
          <input type="number" class="form-control" name="disbursementwithrenewal" id="disbursementwithrenewal">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label" for="collectiontwithrenewaloverdue">Current With Renewal Overdue</label>
        <div class="col-md-6">
          <input type="number" class="form-control" name="collectiontwithrenewaloverdue" id="collectiontwithrenewaloverdue">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label" for="collectiontwithrenewaldue">Collection With Renewal Due</label>
        <div class="col-md-6">
          <input type="number" class="form-control" name="collectiontwithrenewaldue" id="collectiontwithrenewaldue">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-3 control-label" for="outstandinginashadh">Outstanding In Ashadh</label>
        <div class="col-md-6">
          <input type="number" class="form-control" name="outstandinginashadh" id="outstandinginashadh">
        </div>
      </div>
    </form>
  </div>
  <div class="panel-footer">
    <div class="form-group">
      <div class="col-md-6">
        <input type="button" onclick="sendSaveRq()" class="btn btn-success" id="budgetDateParameterSaveBtn"
               value="Submit">
      </div>
    </div>
  </div>
</section>
<section class="panel">
  <header class="panel-heading">
    <h2 class="panel-title">Commits</h2>
  </header>
  <div class="panel-body">
    <table class="table table-bordered table-striped mb-none" id="datatable-editable">
      <thead style=" color: #000000">
      <tr>
        <td>Current Outstanding</td>
        <td>Disbursement With Renewal</td>
        <td>Collection With Renewal Overdue</td>
        <td>Collection With Renewal Due</td>
        <td>Outstanding In Ashadh</td>
      </tr>
      </thead>
    </table>
  </div>
</section>
<tags:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-commits.js"></script>
