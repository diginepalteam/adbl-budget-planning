<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tags:header/>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">LOAN DETAIL AND PROJECTION (तालिका २)</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-editable" style="width: 2000px">
            <thead>
            <tr>
                <th rowspan="3">क्र.सं.</th>
                <th rowspan="3">शिर्षक</th>
                <th colspan="6"><span class="currentYearActual"></span> मसान्तसम्मको यथार्थ</th>
                <th colspan="4">आ.व. <span class="currentYearGrowth"></span> को वैशाख देखि असार सम्मको अनुमानित (३ महिनाको मात्र)</th>
                <th colspan="6"><span class="currentYearProjection"></span> असार मसान्तसम्मको अनुमान</th>
                <th colspan="13">आ.व. <span class="nextYearProjection"></span> को</th>
                <th colspan="6"><span class="nextYearEnd"></span> असार मसान्तसम्ममा हुने</th>
                <th rowspan="4">Outstanding Weight</th>
            </tr>
            <tr>
                <th colspan="3">लगानीमा रहिरहेको कर्जा रकम</th>
                <th rowspan="2">भाखा नाघेको कर्जा</th>
                <th rowspan="2">निस्कृय कर्जा</th>
                <th rowspan="2">निस्कृय व्याज</th>
                <th rowspan="2">लगानी</th>
                <th rowspan="2">साँवा असुली</th>
                <th rowspan="2">३ महिनाको व्याज</th>
                <th rowspan="2">व्याज असुली</th>


                <th colspan="3">लगानीमा रहिरहेको कर्जा रकम</th>
                <th rowspan="2">भाखा नाघ्ने रकम</th>
                <th rowspan="2">निस्कृय कर्जा</th>
                <th>निस्कृय व्याज</th>

                <th colspan="3">लगानी लक्ष्य</th>
                <th colspan="2">म्याद पुग्ने रकम</th>
                <th>म्याद पुग्ने मध्ये असुली लक्ष्य</th>
                <th rowspan="2">निस्कृय कर्जा असुली लक्ष्य</th>
                <th rowspan="2">जम्मा भा.ना. असुली</th>

                <th>जम्मा साँवा असुली लक्ष्य</th>
                <th>निस्कृय व्याजको असुली लक्ष्य</th>
                <th>वार्षिक व्याज</th>
                <th>वार्षिक व्याजको असुली लक्ष्य</th>
                <th>जम्मा व्याज असुली लक्ष्य</th>

                <th colspan="3">लगानीमा रहिरहेको कर्जा</th>
                <th>भाखा नाघ्न सक्ने रकम</th>
                <th>निस्कृय हुन सक्ने साँवा रकम</th>
                <th>निस्कृय व्याज बाँकी</th>
            </tr>
            <tr>
                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>

                <th>छोटो</th>
                <th>आवधिक</th>
                <th>4+8-9</th>

                <th>7+10-11</th>

                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>

                <th>गत आ.व.मा लगानी भएको मध्ये (कोलम नं. १४ मध्ये)</th>
                <th>यस आ.व.मा लगानी हुने मध्ये(कोलम नं. १९ मध्ये)</th>

                <th>21 र 22 को रकम मध्ये</th>

                <th>23+25</th>

                <th>17 को रकम मध्ये</th>
                <th>(14+33)/2 को बार्षिक व्याज</th>
                <th>8 को रकम मध्ये</th>
                <th>20+22</th>

                <th>छोटो</th>
                <th>आवधिक</th>
                <th>14+20-26</th>
                <th>15+21+22-23-25</th>
                <th>34 (वाकी व्याजको सावा रकम)</th>
                <th>17+28-30</th>
            </tr>
            <tr>
                <th></th>
                <th>(1)</th>
                <th>(2)</th>
                <th>(3)</th>
                <th>(4)</th>
                <th>(5)</th>
                <th>(6)</th>
                <th>(7)</th>
                <th>(8)</th>
                <th>(9)</th>
                <th>(10)</th>
                <th>(11)</th>
                <th>(12)</th>
                <th>(13)</th>
                <th>(14)</th>
                <th>(15)</th>
                <th>(16)</th>
                <th>(17)</th>
                <th>(18)</th>
                <th>(19)</th>
                <th>(20)</th>
                <th>(21)</th>
                <th>(22)</th>
                <th>(23)</th>
                <th>(24)</th>
                <th>(25)</th>
                <th>(26)</th>
                <th>(27)</th>
                <th>(28)</th>
                <th>(29)</th>
                <th>(30)</th>
                <th>(31)</th>
                <th>(32)</th>
                <th>(33)</th>
                <th>(34)</th>
                <th>(35)</th>
                <th>(36)</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot style="font-weight: bold">
            <tr></tr>
            </tfoot>
        </table>
    </div>
</section>

<div class="modal fade" id="updateDataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Update (<span id="title"></span>)</h2>
                </header>
                <div class="panel-body">
                    <div class="modal-wrapper">
                        <span><b>आ.व. <span class="currentYearGrowth"></span> को वैशाख देखि असार सम्मको अनुमानित (३ महिनाको मात्र)</b></span>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="disbursementCurEstimated">लगानी</label>
                                    <input id="disbursementCurEstimated" type="text" name="disbursementCurEstimated" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="collectionPrCurEstimated">साँवा असुली</label>
                                    <input id="collectionPrCurEstimated" type="text" name="collectionPrCurEstimated" class="form-control">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <span><b><span class="currentYearProjection"></span> असार मसान्तसम्मको अनुमान</b></span>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="pastDueLoanOutCurEstimated">भाखा नाघ्ने रकम</label>
                                    <input id="pastDueLoanOutCurEstimated" type="text" name="pastDueLoanOutCurEstimated" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="nonPerformingLoanOutCurEstimated">निस्कृय कर्जा</label>
                                    <input id="nonPerformingLoanOutCurEstimated" type="text" name="nonPerformingLoanOutCurEstimated" class="form-control">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <span><b>आ.व. <span class="nextYearProjection"></span> को</b></span>
                        <span><b>लगानी लक्ष्य</b></span>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="disburseLoanTotalTarget">जम्मा</label>
                                    <input id="disburseLoanTotalTarget" type="text" name="disburseLoanTotalTarget" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button id="updateDataBtn" class="btn btn-success modal-confirm"> <i class="fa fa-save"></i> Update</button>
                            <button id="closeModalBtn" class="btn btn-default modal-dismiss">Cancel</button>
                        </div>
                    </div>
                </footer>
            </section>
    </div>
</div>

<tags:footer/>
<script src="${pageContext.request.contextPath }/jsForPages/tables/loan-detail-and-projection.js" type="module"></script>
