<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tags:header/>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title" >Interest Expense Projection</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-editable" style="width:100%">
            <thead>
            <tr>
                <td rowspan=2>क्र.सं.</td>
                <td rowspan=2></td>
                <td rowspan=2>विवरण</td>
                <td rowspan=2> <span class="currentYearActual"></span> चैत्र मसान्तको निक्षेप</td>
                <td rowspan=2> <span class="currentYearProjection"></span> आषाढ मसान्त सम्ममा हुने अनुमानित बाँकी निक्षेप</td>
                <td colspan=4>आ.व. <span class="nextYearProjection"></span> को बाँकी अनुमानित निक्षेप (त्रैमासिक)</td>
                <td colspan=4>आ.व. <span class="nextYearProjection"></span> को औषत बाँकी निक्षेप (त्रैमासिक)</td>
                <td rowspan=2>ब्याज दर</td>
                <td colspan=5>आ.व. <span class="nextYearProjection"></span> को ब्याज खर्च त्रैमासिक</td>
            </tr>
            <tr>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>जम्मा</td>
            </tr>
            </thead>
        </table>
    </div>
</section>
<tags:footer/>
<script src="${pageContext.request.contextPath }/jsForPages/tables/interest-expense-projection.js"
        type="module"></script>