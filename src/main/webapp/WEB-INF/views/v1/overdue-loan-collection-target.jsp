<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<tags:header/>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">तालिका ५ : भाखा नाघेको रकम मध्ये असुली लक्ष्य (तालिका २ कोलम नं. २५ सँग सम्वन्धित)</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-editable" style="width:100%">
            <thead>
            <tr>
                <th rowspan="2">क्र.सं.</th>
                <th rowspan="2">मुख्य उद्देश्य</th>
                <th colspan="3">प्रथम त्रैमासिक (लक्ष्यको ३५ प्रतिशत)</th>
                <th colspan="3">द्वितीय त्रैमासिक(लक्ष्यको ३५ प्रतिशत)</th>
                <th colspan="3">तृतीय त्रैमासिक (लक्ष्यको २० प्रतिशत)</th>
                <th colspan="3">चतुर्थ त्रैमासिक(लक्ष्यको १० प्रतिशत)</th>
                <th colspan="3">कुल जम्मा</th>
            </tr>
            <tr>
                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>

                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>

                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>

                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>

                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot style="font-weight: bold">
            <tr></tr>
            </tfoot>
        </table>
    </div>
</section>

<tags:footer/>
<script src="${pageContext.request.contextPath }/jsForPages/tables/over-due-loan-collection-target.js" type="module"></script>
