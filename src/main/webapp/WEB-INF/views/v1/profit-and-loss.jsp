<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<tags:header/>
<section class="panel">
  <header class="panel-heading">
    <h2 class="panel-title">तालिका १३ : अनुमानित नाफा/नोक्सान हिसाव</h2>
  </header>
  <div class="panel-body">
    <table class="table table-bordered table-striped mb-none" id="datatable-editable" style="width:100%">
      <thead>
      <tr>
        <td rowspan="2">क्र.सं.</td>
        <td rowspan="2">विवरण</td>
        <td rowspan="2">आ.व. २०७६/७७ को यथार्थ</td>
        <td>२०७७ चैत्र मसान्त सम्मको यथार्थ</td>
        <td>२०७८ वैशाख देखि २०७८ आषाढ मसान्त सम्म (३ महिना मात्र)को अनुमानित</td>
        <td>आ.व.२०७७/७८ को जम्मा अनुमानित वार्षिक</td>
        <td>आ.व. २०७८/७९ को लागि वार्षिक अनुमानित</td>
        <td>प्रथम त्रैमासिक अवधि (श्रावण देखि आश्वीन)</td>
        <td>अर्ध वार्षिक अवधि (श्रावण देखि पौष)</td>
        <td>तेश्रो त्रैमासिक अवधि (श्रावण देखि चैत्र सम्म)</td>
        <td>आ.व. २०७८/७९ को लागि वार्षिक अनुमानित</td>
      </tr>
      <tr>
        <td>(क)</td>
        <td>(ख)</td>
        <td>(क)+(ख)</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      </thead>
      <tbody>
      </tbody>
      <tfoot style="font-weight: bold">
      <tr></tr>
      </tfoot>
    </table>
  </div>
</section>

<tags:footer/>
<script src="${pageContext.request.contextPath }/jsForPages/tables/profit-and-loss.js" type="module"></script>
