<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tags:header/>
<section class="panel">
  <header class="panel-heading">
    <h2 class="panel-title"> Expense Budget </h2>
  </header>
  <div class="panel-body">
    <table class="table table-bordered table-striped mb-none" id="datatable-editable" style="width: 100%">
      <thead style=" color: #000000">
      <tr>
        <td rowspan=2>क्र. सं.</td>
        <td rowspan=2></td>
        <td rowspan=2>Line No.</td>
        <td rowspan=2>शीर्षक</td>
        <td rowspan=2>वास्तविक खर्च आ.व.२०७७/७८</td>
        <td colspan=3> आ.व.<span class="currentYearGrowth"></span> को स्वीकृत खर्च</td>
        <td rowspan=2><span class="currentYearActual"></span> चैत्र मसान्त सम्मको यथार्थ खर्च</td>
        <td rowspan=2><span class="currentYearProjection"></span> वैशाख देखि आषाढ मसान्तसम्ममा हुने अनुमानित खर्च</td>
        <td rowspan=2>जम्मा वार्षिक अनुमानित खर्च <span class="currentYearGrowth"></span></td>
        <td rowspan=2><span class="nextYearProjection"></span> को प्रस्तावित खर्च बजेट</td>
        <td rowspan=2>कैफियत</td>
      </tr>
      <tr>
        <td>बजेट पुस्तिका अनुसार</td>
        <td>थप स्विकृत खर्च</td>
        <td>जम्मा</td>
      </tr>
      </thead>
      <tbody>

      </tbody>
      <tfoot style="font-weight: bold">
      <tr>
      </tr>
      </tfoot>
    </table>
  </div>
</section>

<div class="modal fade" id="updateDataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle"
     aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <section class="panel">
      <header class="panel-heading">
        <h2 class="panel-title">Update (<span id="title"></span>)</h2>
      </header>
      <div class="panel-body">
        <div class="modal-wrapper">
          <span><b>आ.व. को स्वीकृत खर्च</b></span>
          <hr>
          <div class="row">
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="currentyearbudget">थप स्विकृत खर्च</label>
                <input id="currentyearbudget" type="number" name="currentyearbudget" class="form-control">
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="currentyearadditionalbudget">बजेट पुस्तिका अनुसार</label>
                <input id="currentyearadditionalbudget" type="number" name="currentyearadditionalbudget" class="form-control">
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="currentyearexpenseprojected">वैशाख देखि आषाढ मसान्तसम्ममा हुने अनुमानित खर्च</label>
                <input id="currentyearexpenseprojected" type="number" name="currentyearexpenseprojected" class="form-control">
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="nextyearexpensebudgetprojection">को प्रस्तावित खर्च बजेट</label>
                <input id="nextyearexpensebudgetprojection" type="number" name="nextyearexpensebudgetprojection" class="form-control">
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="remarks">कैफियत</label>
                <input id="remarks" type="text" name="remarks" class="form-control">
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="panel-footer">
        <div class="row">
          <div class="col-md-12 text-right">
            <button id="updateDataBtn" class="btn btn-success modal-confirm"><i class="fa fa-save"></i>
              Update
            </button>
            <button id="closeModalBtn" class="btn btn-default modal-dismiss">Cancel</button>
          </div>
        </div>
      </footer>
    </section>
  </div>
</div>
<tags:footer/>
<script src="${pageContext.request.contextPath }/jsForPages/tables/expense-budget.js" type="module"></script>
