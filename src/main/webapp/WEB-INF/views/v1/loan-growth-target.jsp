<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<tags:header/>
<section  class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">तालिकाः ६ कर्जा वृद्धि</h2>
    </header>
    <div class="panel-body xScrollable">

        <table class="table table-bordered table-striped mb-none" id="datatable-editable" width="100%">
            <thead>
            <tr>
                <th rowspan="2">क्र.सं.</th>
                <th rowspan="2">मुख्य उद्देश्य</th>
                <th colspan="3">प्रथम त्रैमासिक</th>
                <th colspan="3">द्वितीय त्रैमासिक</th>
                <th colspan="3">तृतीय त्रैमासिक</th>
                <th colspan="3">चतुर्थ त्रैमासिक</th>
                <th colspan="3">कुल जम्मा</th>
                <th rowspan="2"><span class="nextYearEnd"></span> आषाढको अनुमान</th>
                <th rowspan="2">आ.व. <span class="nextYearProjection"></span> को लक्ष्य</th>
            </tr>
            <tr>
                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>

                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>

                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>

                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>

                <th>छोटो</th>
                <th>आवधिक</th>
                <th>जम्मा</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot style="font-weight: bold;">
            <tr></tr>
            </tfoot>
        </table>
    </div>
</section>

<tags:footer/>
<script src="${pageContext.request.contextPath }/jsForPages/tables/loan-growth-target.js" type="module"></script>
