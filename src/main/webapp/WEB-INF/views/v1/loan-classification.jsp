<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<tags:header/>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">तालिका ७ : कर्जाको वर्गिकरण</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-editable" style="width:100%">
            <thead>
            <tr>
                <th rowspan="2">क्र.सं.</th>
                <th rowspan="2">मुख्य उद्देश्य</th>
                <th colspan="6"><span class="currentYearActual"></span> चैत्र मसान्तसम्मको ल.र.र.(तालिका २ को कोलम ४)</th>
                <th colspan="6"><span class="currentYearProjection"></span> आषाढ मसान्तको अनुमानित ल.र.र.(तालिका २ को कोलम १४)</th>
                <th colspan="6"><span class="nextYearProjection"></span> को अन्त्यमा हुने ल.र.र.(तालिका २ को कोलम ३३)</th>
            </tr>
            <tr>
                <th>असल</th>
                <th>सुक्ष्म निगरानी</th>
                <th>कमसल</th>
                <th>शंकास्पद</th>
                <th>खराब</th>
                <th>जम्मा</th>

                <th>असल</th>
                <th>सुक्ष्म निगरानी</th>
                <th>कमसल</th>
                <th>शंकास्पद</th>
                <th>खराब</th>
                <th>जम्मा</th>

                <th>असल</th>
                <th>सुक्ष्म निगरानी</th>
                <th>कमसल</th>
                <th>शंकास्पद</th>
                <th>खराब</th>
                <th>जम्मा</th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot style="font-weight: bold">
            <tr></tr>
            </tfoot>
        </table>
    </div>
</section>

<div class="modal fade" id="updateDataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Update (<span id="title"></span>)</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <span><b><span class="currentYearProjection"></span> आषाढ मसान्तको अनुमानित ल.र.र.(तालिका २ को कोलम १४</b></span>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="watchlistthisprojected">सुक्ष्म निगरानी</label>
                                <input id="watchlistthisprojected" type="text" name="watchlistthisprojected" class="form-control">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <span><b><span class="nextYearProjection"></span> को अन्त्यमा हुने ल.र.र.(तालिका २ को कोलम ३३)</b></span>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="watchlistnextprojected">सुक्ष्म निगरानी</label>
                                <input id="watchlistnextprojected" type="text" name="watchlistnextprojected" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button id="updateDataBtn" class="btn btn-success modal-confirm"> <i class="fa fa-save"></i> Update</button>
                        <button id="closeModalBtn" class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
</div>
<tags:footer/>
<script src="${pageContext.request.contextPath }/jsForPages/tables/loan-classification.js" type="module"></script>
