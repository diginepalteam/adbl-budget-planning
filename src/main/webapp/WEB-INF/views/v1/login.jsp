<!doctype html>
<html lang="en" class="fixed">
<head>
    <title>LOGIN</title>
    <!-- Basic -->
    <meta charset="UTF-8">

    <meta name="keywords" content="ADBL Budget Plan"/>
    <meta name="description" content="ADBL Budget Plan">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <!-- Web Fonts  -->

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath }/vendor/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/vendor/font-awesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/vendor/magnific-popup/magnific-popup.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath }/vendor/bootstrap-datepicker/css/datepicker3.css"/>

    <!-- Theme CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stylesheets/theme.css"/>

    <!-- Skin CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stylesheets/skins/default.css"/>

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath }/stylesheets/theme-custom.css">

    <!-- Head Libs -->
    <script src="${pageContext.request.contextPath }/vendor/modernizr/modernizr.js"></script>

</head>
<body>
<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">
<%--        <a href="/" class="logo pull-left">--%>
<%--            <img src="" id="logoImage" height="54" alt="Porto Admin" />--%>
<%--        </a>--%>

        <div class="panel panel-sign">
            <div class="panel-title-sign mt-xl text-right">
                <h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign In</h2>
            </div>
            <div class="panel-body">
                <form id="loginForm" action="login" method="post">
                    <div class="form-group mb-lg">
                        <label for="username">Username</label>
                        <div class="input-group input-group-icon">
                            <input name="userName" id="username" type="text" class="form-control input-lg"/>
                            <span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
                        </div>
                    </div>

                    <div class="form-group mb-lg">
                        <div class="clearfix">
                            <label for="password" class="pull-left">Password</label>
                        </div>
                        <div class="input-group input-group-icon">
                            <input name="password" id="password" type="password" class="form-control input-lg"/>
                            <span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="checkbox-custom checkbox-default">
                                <input id="RememberMe" name="rememberme" type="checkbox"/>
                                <label for="RememberMe">Remember Me</label>
                            </div>
                        </div>
                        <div class="col-sm-4 text-right">
                            <button type="submit" id="signInBtnHidden" class="btn btn-primary hidden-xs">Sign In
                            </button>
                            <button type="submit" id="signInBtn"
                                    class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In
                            </button>
                        </div>
                    </div>

                    <span class="mt-lg mb-lg text-center text-uppercase">
								<span id="message" style="color:red"><center>${msg }</center></span>
							</span>
                </form>
            </div>
        </div>

        <p class="text-center text-muted mt-md mb-md">&copy; Copyright 2020. All rights reserved. <i><b></b></i></p>
    </div>
</section>


<!-- Vendor -->
<script src="${pageContext.request.contextPath }/vendor/jquery/jquery.js"></script>
<script src="${pageContext.request.contextPath }/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="${pageContext.request.contextPath }/vendor/bootstrap/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath }/vendor/nanoscroller/nanoscroller.js"></script>
<script src="${pageContext.request.contextPath }/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="${pageContext.request.contextPath }/vendor/magnific-popup/magnific-popup.js"></script>
<script src="${pageContext.request.contextPath }/vendor/jquery-placeholder/jquery.placeholder.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="${pageContext.request.contextPath }/javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="${pageContext.request.contextPath }/javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="${pageContext.request.contextPath }/javascripts/theme.init.js"></script>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-login.js"></script>
</body>

</html>
