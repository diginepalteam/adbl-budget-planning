<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<tags:header/>
<section class="panel">
  <header class="panel-heading">
    <h2 class="panel-title">तालिका – १४: प्रस्तावित वित्तीय अवस्थाको विवरण (वासलात)</h2>
  </header>
  <div class="panel-body">
    <table class="table table-bordered table-striped mb-none" id="datatable-editable" style="width:100%">
      <thead>
      <tr>
        <td colspan="4">पुँजी तथा दायित्व</td>
        <td colspan="4">सम्पत्ति</td>
      </tr>
      <tr>
        <td>शीर्षक</td>
        <td>२०७६/७७ को यथार्थ</td>
        <td>२०७८ आषाढको अनुमानित</td>
        <td>२०७९ आषाढको प्रस्तावित</td>
        <td>शीर्षक</td>
        <td>२०७६/७७ को यथार्थ</td>
        <td>२०७८ आषाढको अनुमानित</td>
        <td>२०७९ आषाढको प्रस्तावित</td>
      </tr>
      </thead>
      <tbody>
      </tbody>
      <tfoot style="font-weight: bold">
      <tr></tr>
      </tfoot>
    </table>
  </div>
</section>

<tags:footer/>
<script src="${pageContext.request.contextPath }/jsForPages/tables/balance-sheet.js" type="module"></script>
