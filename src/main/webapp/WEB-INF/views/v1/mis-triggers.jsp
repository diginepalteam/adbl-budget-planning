<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tags:header/>
<section class="panel">
  <header class="panel-heading">
    <h2 class="panel-title"> MIS SYNC EVENTS </h2>
  </header>
  <div class="panel-body xScrollable">
    <table class="table table-bordered table-striped mb-none" id="datatable-editable" width="100%"
           style="color: #000000">
      <thead>
      <tr>
        <td>Name</td>
        <td>Action</td>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td>BUDGET EXPENSE</td>
        <td><a class="btn btn-success" onclick="triggerMisBudgetExpenseSync()">Sync</a></td>
      </tr>
      <tr>
        <td>INTEREST COLLECTION TARGET</td>
        <td><a class="btn btn-success" onclick="triggerInterestLoanCollectionTarget()">Sync</a></td>
      </tr>
      <tr>
        <td>LoanOverDueCollectionTarget, LoanDueCollectionTarget, LoanGrowthTarget</td>
        <td><a class="btn btn-success" onclick="triggerPostLoanEvent()">Sync</a></td>
      </tr>
      <tr>
        <td>PROFIT AND LOSS</td>
        <td><a class="btn btn-success" onclick="triggerProfitAndLossSyncEvent()">Sync</a></td>
      </tr>
      <tr>
        <td>BALANCE SHEET</td>
        <td><a class="btn btn-success" onclick="triggerBalanceSheetSyncEvent()">Sync</a></td>
      </tr>
      </tbody>
    </table>
  </div>
</section>
<tags:footer/>
<tags:loaderModal/>
<script src="${pageContext.request.contextPath }/jsForPages/page-mis-triggers.js" type="module"></script>
