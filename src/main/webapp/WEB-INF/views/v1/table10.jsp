<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<tags:header/>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">तालिका १०ः कर्मचारी दरवन्दी (आ.व. २०७८/७९)</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-editable" style="width:100%">
            <thead>
            <tr>
                <th rowspan="2">क्र.सं.</th>
                <th colspan="2">कर्मचारी विवरण</th>
                <th colspan="3">स्थायी दरवन्दी</th>
                <th colspan="2">करार/अस्थायी</th>
                <th rowspan="2">Action</th>
            </tr>
            <tr>
                <th>पद</th>
                <th>तह</th>

                <th>२०७७/७८ को स्वीकृत दरवन्दी</th>
                <th>पूर्ति(२०७७ चैत्र)</th>
                <th>२०७८/७९ को प्रस्तावित दरवन्दी</th>

                <th>पूर्ति २०७७/७८</th>
                <th>प्रस्तावित २०७८/७९</th>
            </tr>
            </thead>
            <tbody>
            <tr class="gradeX">
                <td>1</td>
                <td>test</td>
                <td>test</td>
                <td>test</td>
                <td>test</td>
                <td>test</td>
                <td>test</td>
                <td>test</td>
                <td class="actions">
                    <a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>
                    <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>
                    <a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>

                </td>
            </tr>
            </tbody>
        </table>
    </div>
</section>

<tags:footer/>
<script>
    function updateDatatableValues(values) {
        console.log("update these values" + values);
    }
</script>