<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tags:header/>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title" style="font-family: Preeti"> tflnsf !@ M z'Ns tyf sldzg cfDbfgLsf] nIo </h2>
        <h2 class="panel-title" style="font-family: Preeti"> -cf=j= @)&amp;*÷&amp;(_ </h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-editable">
            <thead style=" color: #000000">
            <tr>
                <td rowspan=2>qm= ;+=</td>
                <td rowspan=2>ljj/0f</td>
                <td rowspan=2>cf=j= @)&amp;^÷&amp;&amp; sf] oyfy{</td>
                <td rowspan=2> @)&amp;&amp; r}q d;fGt ;Ddsf] oyfy{</td>
                <td rowspan=2> @)&amp;* j}zfv b]lv @)&amp;* cfiff9 ;Ddsf] cfDbfgL</td>
                <td colspan=6> cf=j=@)&amp;*÷&amp;( sf] nflu k|:tfljt</td>
            </tr>
            <tr>
                <td>k|yd q}dfl;s sf] -&gt;fj0f b]lv cflZjg ;Dd_ dfq</td>
                <td>låtLo q}dfl;ssf] -sflt{s b]lv kf}if ;Dd_ dfq</td>
                <td>t[tLo q}dfl;ssf]-df3 b]lv r}q ;Dd_ dfq</td>
                <td>rt'y{ q}dfl;ssf] -@)&amp;( j}zfv b]lv @)&amp;( cfiff9 ;Dd_ dfq</td>
                <td>hDdf aflif{s a[l4 nIo</td>
                <td>jflif{s hDdf</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>s</td>
                <td>v</td>
                <td>u</td>
                <td>3</td>
                <td><td>
                <td>s±v±u±3</td>
            </tr>

            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
            </tr>

            </tbody>
        </table>
    </div>
</section>
<tags:footer/>
<script src="${pageContext.request.contextPath }/jsForPages/tables/income-from-fee-and-commission-target.js"
        type="module"></script>