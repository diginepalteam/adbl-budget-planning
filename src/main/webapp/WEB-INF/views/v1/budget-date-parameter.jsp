<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<tags:header/>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Set Budget Date Parameter</h2>
    </header>
    <div class="panel-body">
        <form class="form-horizontal form-bordered" id="budgetDateParameterSaveForm">
            <div class="form-group">
                <label class="col-md-3 control-label" for="currentYearActual">Current Year Actual</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="currentYearActual" id="currentYearActual">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="currentYearGrowth">Current Year Growth</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="currentYearGrowth" id="currentYearGrowth">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="currentYearProjection">Current Year Projection</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="currentYearProjection" id="currentYearProjection">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="nextYearProjection">Next Year Projection</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="nextYearProjection" id="nextYearProjection">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="nextYearEnd">Next Year End</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="nextYearEnd" id="nextYearEnd">
                </div>
            </div>
        </form>
    </div>
    <div class="panel-footer">
        <div class="form-group">
            <div class="col-md-6">
                <input type="button" onclick="sendSaveRq()" class="btn btn-success" id="budgetDateParameterSaveBtn"
                       value="Submit">
            </div>
        </div>
    </div>
</section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Available Budget Date Parameter</h2>
    </header>
    <div class="panel-body">
        <p>Current Year Actual : <span id="spanCurrentYearActual" style="font-weight: bolder"></span></p>
        <p>Current Year Growth : <span id="spanCurrentYearGrowth" style="font-weight: bolder"></span></p>
        <p>Current Year Projection : <span id="spanCurrentYearProjection" style="font-weight: bolder"></span></p>
        <p>Next Year Projection : <span id="spanNextYearProjection" style="font-weight: bolder"></span></p>
        <p>Next Year End : <span id="spanNextYearEnd" style="font-weight: bolder"></span></p>
    </div>
</section>
<tags:footer/>
<script type="module" src="${pageContext.request.contextPath }/jsForPages/page-budget-date-parameter.js"></script>
