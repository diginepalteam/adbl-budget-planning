<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<tags:header/>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">तालिका ८ः व्याज असुली लक्ष्य</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-editable" style="width:100%">
            <thead>
            <tr>
              <th></th>
                <th>श्रावण</th>
                <th>भाद्र</th>
                <th>आश्विन</th>
                <th>कात्र्तिक</th>
                <th>मंसिर</th>
                <th>पौष</th>
                <th>माघ</th>
                <th>फाल्गुण</th>
                <th>चैत्र</th>
                <th>बैशाख</th>
                <th>जेठ</th>
                <th>आषाढ</th>
                <th>जम्मा</th>
            </tr>
            </thead>
        </table>
    </div>
</section>

<tags:footer/>
<script src="${pageContext.request.contextPath }/jsForPages/tables/interest-collection-target.js"
        type="module"></script>
