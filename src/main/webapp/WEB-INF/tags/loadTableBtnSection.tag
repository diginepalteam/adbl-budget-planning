<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-primary" id="loadTableBtn"><i class="fa fa-refresh" aria-hidden="true"></i> Reload Data</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </footer>
        </section>
    </div>
</div>