<%@ taglib tagdir="/WEB-INF/tags/" prefix="tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html class="fixed sidebar-left-collapsed">
<head>
    <title>${pagetitle }</title>
    <tags:meta/>
    <tags:styles/>
    <style>
        .dataTables_filter {
            width: 50%;
            float: right;
            text-align: right;
        }
        .dataTables_scrollBody tbody td, .dataTables_scrollHeadInner thead th div{
            overflow-x: hidden;
            width: inherit;
        }
        .xScrollable {
            overflow-x: auto;
            white-space: nowrap;
        }
      table {
        color: black;
      }
    </style>

</head>
<body>
<section class="body">

    <!-- start: header -->
    <header class="header">

        <div class="logo-container">
            <a href=".." class="logo">
                <img src="${pageContext.request.contextPath }/images/logo.png" height="35" alt="Admin"/>
            </a>
            <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html"
                 data-fire-event="sidebar-left-opened">
                <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <!-- start: search & user box -->
        <div class="header-right">
            <span><i>Branch: <b id="loginBranchCode"></b></i></span>
            <span class="separator"></span>
            <form action="#" class="search nav-form">
                <div class="input-group input-search">
                    <input type="text" class="form-control" id="q" placeholder="Search...">
                    <span class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
							</span>
                </div>
            </form>

            <span class="separator"></span>
            <div id="userbox" class="userbox">
                <a href="#" data-toggle="dropdown">
                    <figure class="profile-picture">
                        <img src="${pageContext.request.contextPath }/images/logo.png" class="img-circle"/>
                    </figure>
                    <div class="profile-info" data-lock-name="First Last" data-lock-email="username">
                        <span class="name" id="profileName"></span>
                        <span class="role" id="profileUsername"></span>
                    </div>

                    <i class="fa custom-caret"></i>
                </a>

                <div class="dropdown-menu">
                    <ul class="list-unstyled">
                        <li class="divider"></li>
                        <li>
                            <a tabindex="-1" id="userProfileViewPageBtn"><i
                                    class="fa fa-user"></i> My Profile</a>
                        </li>
                        <%--<li>
                            <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>
                        </li>--%>
                        <li>
                            <a role="menuitem" tabindex="-1" id="logoutBtn"><i class="fa fa-power-off"></i> Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end: search & user box -->
    </header>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <aside id="sidebar-left" class="sidebar-left">
            <%--            <nav id="menuUI"></nav>--%>
            <div class="sidebar-header">
                <div class="sidebar-title" style="color:white">
                    Navigation
                </div>
                <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html"
                     data-fire-event="sidebar-left-toggle">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>

            <div class="nano">
                <div class="nano-content">
                    <nav class="nav-main" role="navigation">
                        <ul class="nav nav-main nav-ul" id="navbarId">
                        </ul>
                    </nav>
                </div>
            </div>
        </aside>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
            <header class="page-header">
                <h2>${pagetitle }</h2>

                <div class="right-wrapper pull-right">
                    <ol class="breadcrumbs">
                        <li>
                            <i class="fa fa-home"></i>
                        </li>
                        <li><span>${pagetitle }</span></li>
                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
                </div>
            </header>
