<%--SUPER ADMIN MENU--%>
<div id="superAdminNavigation">
    <nav id="menu" class="nav-main" role="navigation">
        <ul class="nav nav-main">
            <%--        DASHBOARD--%>

            <%--            ADMIN SETTINGS--%>
                <%--                    USER--%>
                <li><a id="userCreatePageBtn"><i
                        class="fa fa-users" aria-hidden="true"></i> <span>User Settings</span></a></li>
                <%--                    OFFICE--%>
                <li><a id="officeCreatePageBtn"><i
                        class="fa fa-briefcase" aria-hidden="true"></i> <span>Office Settings</span></a></li>
                <%--                    DRIVER--%>
                <li><a id="driverCreatePageBtn"><i
                        class="fa fa-user" aria-hidden="true"></i> <span>Driver Settings</span></a></li>
                <%--                    STAFFS--%>
                <li><a id="staffsCreatePageBtn"><i
                        class="fa fa-users" aria-hidden="true"></i> <span>Staff Settings</span></a></li>
                <%--                    VEHICLE--%>
                <li><a id="vehicleCreatePageBtn"><i
                        class="fa fa-automobile" aria-hidden="true"></i> <span>Vehicle Settings</span></a></li>
                <%--                            LOGO --%>
                <li>
                    <a id="logoUploadPageBtn"><i class="fa fa-photo" aria-hidden="true"></i>
                        <span>Logo Settings</span></a>
                </li>
        </ul>
    </nav>
</div>