<!-- Full Height Modal Right -->
<div class="modal fade right" id="loadingModal" tabindex="-1"
     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-full-height modal-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Loading</h4>
      </div>
      <div class="modal-body">
        <p>
          Message: <span id="msg">Loading Request. Please wait.</span>
        </p>
      </div>
    </div>
  </div>
</div>
<!-- Full Height Modal Right -->
